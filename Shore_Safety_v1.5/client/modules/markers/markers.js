/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App')
        .controller('MarkersController', MarkersController);

    // For global map page
    function MarkersController($http, $rootScope, $state, $uibModal) {
        var vm = this;
        $rootScope.search = '';
        $rootScope.isSearchShowing = true;

        vm.statuses = [
            '',
            'good',
            'bad',
            'ugly',
            'planned',
            'route'
        ];

        vm.filters = {
            'good': true,
            'bad': true,
            'ugly': true,
            'planned': true,
            'route': true
        };

        if (localStorage.getItem('filters')) {
            try {
                vm.filters = JSON.parse(localStorage.getItem('filters'));
            } catch (e) { }
        }

        $rootScope.$on('filter markers', function (evt, filters) {
            vm.filters = filters;
            localStorage.setItem('filters', JSON.stringify(vm.filters));
        });

        vm.filterMarker = function () {
            $rootScope.$broadcast('filter markers', vm.filters);
        };

        getData();

        function uuid() {
            return window.btoa((new Date()) + Math.random() * 1000000);
        }

        function getData() {
            $http.get($rootScope.clientApiUrl('marker')).then(function (result) {
                vm.list = result.data;
                $rootScope.$broadcast('map refresh');
            }, function () {
                localStorage.removeItem('token');
                $state.go('login');
            });
        }

        function findIndex(id, historyId) {
            var index = -1;
            for (var j = 0; j < vm.list.length; j++) {
                if (vm.list[j]._id === id) {
                    index = j;
                }
            }
            if (!historyId) {
                return index;
            } else {
                if (index > -1) {
                    var history = vm.list[index].history;
                    var historyIndex = -1;
                    for (var j = 0; j < history.length; j++) {
                        if (history[j]._id === historyId) {
                            historyIndex = j;
                        }
                    }
                    return historyIndex;
                } else {
                    return -1;
                }
            }
        }

        vm.delete = function (id, historyId) {
            if (confirm('do you want to delete?')) {
                const index = findIndex(id);

                if (index > -1) {
                    if (!historyId) {
                        $http.delete($rootScope.clientApiUrl('marker/' + id)).then(function (result) {
                            vm.list.splice(index, 1);
                            $rootScope.$broadcast('map refresh');

                            var refreshOnce;
                            function refreshHack() {
                                if (!refreshOnce) {
                                    refreshOnce = true;
                                    location.reload();
                                }
                            }
                            refreshHack();
                        });
                    } else {
                        const historyIndex = findIndex(id, historyId);
                        if (historyIndex > -1) {
                            vm.list[index].history.splice(historyIndex, 1);
                            vm.edit(id);
                            vm.save(true);
                        }
                    }
                }
            }
        };

        vm.addHistory = function (id) {
            const index = findIndex(id);

            if (index > -1) {
                const item = vm.list[index];
                if (!item.history.length) {
                    vm.list[index].history = [{
                        'markerId': item._id,
                        'description': item.description,
                        'status': item.status,
                        'route': item.route,
                        'routeName': item.routeName,
                        'image': item.image,
                        'create': item.create,
                        '_adding': true,
                        '_id': uuid(),
                    }];
                }

                const historyId = uuid();

                vm.list[index].history.unshift({
                    'markerId': vm.list[index]._id,
                    'description': '',
                    'status': 1,
                    'route': 1,
                    'routeName': '',
                    'image': '',
                    '_adding': true,
                    '_id': historyId,
                });

                vm.edit(id, historyId);
            }
        };

        vm.edit = function (id, historyId) {
            const index = findIndex(id);
            vm.editingId = id;
            vm.historyId = historyId;

            if (index > -1) {
                const item = vm.list[index];
                vm.editingItem = {
                    'markerId': item.markerId,
                    'description': item.description,
                    'status': String(item.status),
                    'route': 1,
                    'routeName': item.routeName,
                    'lat': item.lat,
                    'lng': item.lng,
                    'history': item.history,
                    'image': item.image,
                };

                if (vm.historyId) {
                    const historyIndex = findIndex(id, vm.historyId);
                    const historyItem = vm.editingItem.history[historyIndex];

                    vm.editingHistoryItem = {
                        'description': historyItem.description,
                        'status': String(historyItem.status),
                        'route': String(historyItem.route),
                        'routeName': historyItem.routeName,
                        'image': historyItem.image,
                        'create': historyItem.create,
                        'edit': historyItem.edit
                    };
                }
            }
        };

        vm.save = function (noAlert) {
            if (noAlert || confirm('do you want to save changes?')) {
                const index = findIndex(vm.editingId);
                if (index > -1) {
                    if (!vm.historyId) {
                        vm.list[index].markerId = vm.editingItem.markerId;
                        vm.list[index].description = vm.editingItem.description;
                        vm.list[index].status = Number(vm.editingItem.status);
                        vm.list[index].route = Number(vm.editingItem.route);
                        vm.list[index].routeName = vm.editingItem.routeName;
                        vm.list[index].lat = vm.editingItem.lat;
                        vm.list[index].lng = vm.editingItem.lng;
                        if (vm.editingItem.uploadImage) {
                            vm.editingItem.image = vm.editingItem.uploadImage;
                            vm.list[index].uploadImage = vm.editingItem.uploadImage;
                            vm.list[index].image = vm.editingItem.uploadImage;
                        }
                    } else {
                        const historyIndex = findIndex(vm.editingId, vm.historyId);
                        if (historyIndex > -1) {
                            vm.list[index].history[historyIndex].description = vm.editingHistoryItem.description;
                            vm.list[index].history[historyIndex].status = Number(vm.editingHistoryItem.status);
                            vm.list[index].history[historyIndex].route = Number(vm.editingHistoryItem.route);
                            vm.list[index].history[historyIndex].routeName = vm.editingHistoryItem.routeName;
                            if (vm.editingHistoryItem.uploadImage) {
                                vm.editingHistoryItem.image = vm.editingHistoryItem.uploadImage;
                                vm.list[index].history[historyIndex].uploadImage = vm.editingHistoryItem.uploadImage;
                                vm.list[index].history[historyIndex].image = vm.editingHistoryItem.uploadImage;
                            }
                        }
                    }

                    for (var i in vm.list) {
                        for (var j in vm.list[i].history) {
                            if (vm.list[i].history[j]._adding) {
                                delete vm.list[i].history[j]._id;
                                delete vm.list[i].history[j]._adding;
                            }
                        }
                    }

                    let def;
                    if (vm.list[index]._id) {
                        def = $http.put($rootScope.clientApiUrl('marker/' + vm.list[index]._id), vm.list[index]);
                    } else {
                        def = $http.post($rootScope.clientApiUrl('marker'), vm.list[index]);
                    }
                    def.then(function (result) {
                        vm.cancel();
                        getData();
                        $rootScope.$broadcast('map refresh');
                    });
                }
            }
        };

        vm.cancel = function () {
            const index = findIndex(vm.editingId);
            if (index > -1) {
                if (!vm.list[index] || !vm.list[index]._id) {
                    vm.list.splice(index, 1);
                } else {
                    delete vm.list[index].uploadImage;
                    if (vm.list[index].history) {
                        for (var historyIndex = vm.list[index].history.length - 1; historyIndex >= 0; historyIndex--) {
                            vm.list[index].history[historyIndex].uploadImage;
                            if (vm.list[index].history[historyIndex]._adding || !vm.list[index].history[historyIndex].create) {
                                vm.list[index].history.splice(historyIndex, 1);
                            }
                        }
                    }
                }
                delete vm.editingId;
                delete vm.historyId;
                delete vm.editingItem;
                delete vm.editingHistoryItem;
            }
        };

        vm.upload = function (id, historyId) {
            const index = findIndex(id);
            let historyIndex = -1;
            if (index > -1) {
                if (historyId) {
                    historyIndex = findIndex(id, historyId);
                }

                var modalInstance = $uibModal.open({
                    templateUrl: './modules/upload/upload.html',
                    controller: 'UploadController',
                    controllerAs: 'vm'
                });

                modalInstance.result.then(function (data) {
                    if (data) {
                        if (historyId) {
                            if (historyIndex > -1) {
                                vm.list[index].history[historyIndex].uploadImage = data;
                            }
                        } else {
                            vm.list[index].uploadImage = data;
                            vm.editingItem.uploadImage = data;
                        }
                    }
                });
            }
        }
    }
})();

