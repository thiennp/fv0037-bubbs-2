/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App')
        .controller('ProfileController', ProfileController);

    // For global map page
    function ProfileController($http, $rootScope, $scope, $timeout) {
        var vm = this;
        $rootScope.isSearchShowing = false;

        $http.get($rootScope.clientApiUrl('profile')).then(function (result) {
            vm.user = result.data;
        });

        vm.submit = function () {
            vm.loading = true;
            var canvas = document.getElementById('profile-image');
            var dataURL = canvas.toDataURL();
            vm.user.uploadImage = dataURL.length > 100 ? dataURL : '';

            $http.put($rootScope.clientApiUrl('profile'), vm.user).then(function () {
                $http.get($rootScope.clientApiUrl('profile')).then(function (result) {
                    vm.loading = false;
                    vm.user = result.data;
                    if (!!result.data.name) {
                        localStorage.setItem('user_name', result.data.name);
                    }
                    if (!!result.data.username) {
                        localStorage.setItem('user_username', result.data.username);
                    }
                    if (!!result.data.email) {
                        localStorage.setItem('user_email', result.data.email);
                    }
                    if (!!result.data.image) {
                        localStorage.setItem('user_image', result.data.image);
                    }
                });
            });
        };

        vm.canvasShown = function () {
            var canvas = document.getElementById('profile-image');
            return canvas && canvas.offsetWidth;
        };

        var init = function () {
            var fileUpload = document.getElementById('fileUpload');
            if (!fileUpload) {
                setTimeout(init, 100);
            } else {
                var canvas = document.getElementById('profile-image');
                var ctx = canvas.getContext("2d");
                var canvasSize = 400;

                fileUpload.onchange = readImage;

                canvas.onclick = function (e) {
                    var x = e.offsetX;
                    var y = e.offsetY;
                    ctx.beginPath();
                    ctx.fillStyle = 'black';
                    ctx.arc(x, y, 5, 0, Math.PI * 2);
                    ctx.fill();
                };
            }

            function readImage() {
                if (this.files && this.files[0]) {
                    var FR = new FileReader();
                    FR.onload = function (e) {
                        var img = new Image();
                        img.src = e.target.result;
                        img.onload = function () {
                            var w = img.naturalWidth;
                            var h = img.naturalHeight;
                            if (w > h) {
                                h = canvasSize * h / w;
                                w = canvasSize;
                            } else {
                                w = canvasSize * w / h;
                                h = canvasSize;
                            }
                            $("#profile-image").attr('width', w);
                            $("#profile-image").attr('height', h);
                            ctx.drawImage(img, 0, 0, w, h);
                            vm.imageUpdated = true;
                            $scope.$digest();
                        };
                    };
                    FR.readAsDataURL(this.files[0]);
                }
            }
        };

        $(init);
    }
})();
