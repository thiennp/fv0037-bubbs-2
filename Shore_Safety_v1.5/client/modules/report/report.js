/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App')
        .controller('ReportController', ReportController);

    // For global map page
    function ReportController($http, $rootScope, $state, $timeout) {
        var vm = this;
        $rootScope.search = '';
        $rootScope.isSearchShowing = true;

        vm.labels = ['', 'Good', 'Bad', 'Ugly', 'Planned'];
        vm.selected = {};
        vm.sort = 'create';
        vm.reverse = true;

        vm.statuses = [
            '',
            'good',
            'bad',
            'ugly',
            'planned',
            'route'
        ];

        vm.filters = {
            'good': true,
            'bad': true,
            'ugly': true,
            'planned': true,
            'route': true
        };

        if (localStorage.getItem('filters')) {
            try {
                vm.filters = JSON.parse(localStorage.getItem('filters'));
            } catch (e) { }
        }

        $rootScope.$on('filter markers', function (evt, filters) {
            console.log(filters);
            vm.filters = filters;
            localStorage.setItem('filters', JSON.stringify(vm.filters));
        });

        vm.filterMarker = function () {
            $rootScope.$broadcast('filter markers', vm.filters);
        };

        getData();

        function getData() {
            if ($state.params.id) {
                if ($state.params.id.split(',').length < 2) {
                    $http.get($rootScope.clientApiUrl('marker/' + $state.params.id)).then(function (result) {
                        vm.data = [result.data];
                    }, function () {
                        localStorage.removeItem('token');
                        $state.go('login');
                    });
                } else {
                    vm.data = [];
                    vm.allIds = $state.params.id.split(',');
                    for (var i = 0; i < vm.allIds.length; i++) {
                        const index = i;
                        $http.get($rootScope.clientApiUrl('marker/' + vm.allIds[i])).then(function (result) {
                            vm.data[index] = result.data;
                        }, function () {
                            localStorage.removeItem('token');
                            $state.go('login');
                        });
                    }
                }
            } else {
                $http.get($rootScope.clientApiUrl('all')).then(function (result) {
                    vm.list = result.data;
                    vm.shownList = vm.list;
                }, function () {
                    localStorage.removeItem('token');
                    $state.go('login');
                });
            }
        }

        vm.export = function () {
            vm.printing = true;
            $timeout(function () {
                window.print();
                vm.printing = false;
            }, 10);
        };

        vm.toggleSelection = function (id) {
            $timeout(function () {
                if (id === undefined) {
                    if (!vm.preventAll) {
                        if (!vm.allSelected) {
                            _.each(vm.list, function (item) {
                                vm.selected[item._id] = false;
                            });
                        } else {
                            _.each(vm.list, function (item) {
                                vm.selected[item._id] = true;
                            });
                        }
                    } else {
                        vm.preventAll = false;
                    }
                } else {
                    vm.preventAll = true;

                    if (!vm.selected[id] && vm.allSelected) {
                        vm.allSelected = false;
                    }

                    $timeout(function () {
                        vm.preventAll = false;
                    }, 100);
                }
            }, 10);
        };

        vm.selectAll = function (status) {
            var ids = [];

            _.each(vm.list, function (item) {
                if (item.status === status) {
                    ids.push(item._id);
                }
            });

            $state.go('app.report', {
                id: ids.join(',')
            });
        };

        vm.selections = function () {
            var ids = [];
            _.each(vm.list, function (item) {
                if (vm.selected[item._id]) {
                    ids.push(item._id);
                }
            });
            return ids.join(',');
        };
    }
})();
