/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App')
        .controller('MainController', MainController);

    // For global map page
    function MainController($rootScope) {
        var vm = this;
        $rootScope.isSearchShowing = false;

        $rootScope.$on('auth', init);

        vm.filterMarker = function () {
            $rootScope.$broadcast('filter markers', vm.filters);
        };

        vm.filterRoute = function () {
            if (!vm.initialized) {
                return;
            }
            _.each(vm.routeNames, function (item, index) {
                $rootScope.routeFiltersByName[item] = vm.routeFilters[index];
            });
            localStorage.setItem('routeFiltersByName', JSON.stringify($rootScope.routeFiltersByName));
            $rootScope.$broadcast('filter routes', vm.routeFilters);
        };

        vm.search = function () {
            $rootScope.$broadcast('filter change', vm.searchText);
        };

        function _updateRouteFilterDefault() {
            _.each(vm.routeNames, function (item, index) {
                vm.routeFilters[index] = true;
                $rootScope.routeFiltersByName[item] = true;
            });
        }

        function updateRouteFilter() {
            const routeFiltersByName = localStorage.getItem('routeFiltersByName');
            if (!routeFiltersByName) {
                _updateRouteFilterDefault();
            } else {
                try {
                    $rootScope.routeFiltersByName = JSON.parse(routeFiltersByName);
                    _.each(vm.routeNames, function (item, index) {
                        vm.routeFilters[index] = $rootScope.routeFiltersByName[item] !== undefined ? $rootScope.routeFiltersByName[item] : true;
                    });
                } catch (e) {
                    _updateRouteFilterDefault();
                }
            }
            vm.initialized = true;
            vm.filterRoute();
        }

        function init() {

            vm.filters = {
                'good': true,
                'bad': true,
                'ugly': true,
                'planned': true,
                'route': true
            };

            if (localStorage.getItem('filters')) {
                try {
                    vm.filters = JSON.parse(localStorage.getItem('filters'));
                } catch (e) {}
            }

            vm.routeFilters = [];
            $rootScope.routeFiltersByName = {};
            vm.filterRoute();

            vm.routeNames = $rootScope.routeNames;
            updateRouteFilter();

            $rootScope.$on('update route names', function (evt, routeNames) {
                vm.routeNames = routeNames;
                updateRouteFilter();
            });

            $rootScope.$on('add route name', function (evt, data) {
                if (vm.routeNames.indexOf(data.routeName) === -1) {
                    vm.routeNames.push(data.routeName);
                }
                updateRouteFilter();
            });
        }

        init();
    }
})();