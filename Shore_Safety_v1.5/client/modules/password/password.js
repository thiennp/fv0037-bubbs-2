/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App')
        .controller('PasswordController', PasswordController);

    function PasswordController($http, $state, $rootScope) {
        var vm = this;
        $rootScope.isSearchShowing = false;

        vm.submit = function () {
            $http.post(window.authUrl + 'password', {
                passwordCode: $state.params.passwordCode,
                password: vm.password,
            }).then(function (result) {
                try {
                    alert('update password successfully');
                    $state.go('login');
                } catch (e) {
                    console.log(e);
                }
            }, function (err) {
                alert('can\'t update password');
                vm.err = err;
            });
        };
    }
})();
