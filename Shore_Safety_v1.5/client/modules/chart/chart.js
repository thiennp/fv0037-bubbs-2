/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App')
        .controller('ChartController', ChartController);

    function ChartController($http, $rootScope, $state) {
        var vm = this;
        $rootScope.isSearchShowing = false;

        vm.labels = ['Good', 'Bad', 'Ugly', 'Planned'];
        vm.data = [0, 0, 0, 0];
        vm.routeDataFiltered = [0, 0, 0, 0];

        vm.routeLabels = ['Route 1', 'Route 2', 'Route 3'];
        vm.routeData = [0, 0, 0];

        vm.routeByLabel = {};
        vm.routeLabelAll = [];
        vm.routeDataAll = [];

        vm.total = 0;
        vm.chartOptions = {
            scales: {
                yAxes: [{
                    ticks: {
                        display: true,
                        beginAtZero: true,
                        min: 0
                    }
                }]
            }
        };
        console.log($rootScope.routeFiltersByName);

        $http.get($rootScope.clientApiUrl('all')).then(function (result) {
            vm.list = result.data;
            console.log(vm.list);
            // vm.chartOptions.scales.yAxes[0].ticks.max = Math.max(...result.data);
            angular.forEach(result.data, function (item) {
                if (item.status) {
                    vm.data[item.status - 1]++;
                    vm.total++;
                    if (item.status === 5) {
                        vm.routeData[item.route - 1]++;
                    }

                    if (!vm.routeByLabel[item.routeName]) {
                        const len = vm.routeLabelAll.length;
                        vm.routeByLabel[item.routeName] = len + 1;
                        vm.routeLabelAll.push('Route ' + item.routeName);
                        vm.routeDataAll[len] = 0;
                    }

                    vm.routeDataAll[vm.routeByLabel[item.routeName] - 1]++;
                    if ($rootScope.routeFiltersByName[item.routeName]) {
                        vm.routeDataFiltered[item.status - 1]++;
                    }
                }
            });
        }, function () {
            localStorage.removeItem('token');
            $state.go('login');
        });
    }
})();