/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App')
        .controller('GlobalController', GlobalController);

    // For global map page
    function GlobalController($rootScope, $state) {
        var vm = this;
        vm.state = $state;
        vm.filters = {
            'good': true,
            'bad': true,
            'ugly': true,
            'planned': true,
            'route': true
        };
        if (localStorage.getItem('filters')) {
            try {
                vm.filters = JSON.parse(localStorage.getItem('filters'));
            } catch (e) {}
        }
        $rootScope.$broadcast('filter markers', vm.filters);

        vm.noMapStates = ['app.chart', 'app.markers', 'app.route-markers', 'app.reports', 'app.report', 'app.profile'];

        $rootScope.$on('filter markers', function (evt, filters) {
            vm.filters = filters;
            localStorage.setItem('filters', JSON.stringify(vm.filters));
        });

        $rootScope.$on('filter routes', function (evt, routeFilters) {
            vm.routeFilters = routeFilters;
            vm.allRoutes = [];
            angular.forEach(vm.routeFilters, function (route, index) {
                if (route && $rootScope.routeNames[index].trim()) {
                    vm.allRoutes.push($rootScope.routeNames[index]);
                }
            });
            vm.subtitle = vm.allRoutes.join(' | ');
            takeRouteFilter();
        });

        function takeRouteFilter() {
            const routeFiltersByName = localStorage.getItem('routeFiltersByName');
            if (routeFiltersByName) {
                try {
                    $rootScope.routeFiltersByName = JSON.parse(routeFiltersByName);
                } catch (e) {}
            }
        }

        takeRouteFilter();

        vm.closeCapture = function () {
            document.getElementById('capture-bar').classList.remove('show');
        };

        vm.user = function () {
            return {
                name: localStorage.getItem('user_name'),
                username: localStorage.getItem('user_username'),
                image: localStorage.getItem('user_image'),
                email: localStorage.getItem('user_email')
            };
        };

        vm.logout = function () {
            localStorage.removeItem('token');
            localStorage.removeItem('id');
            localStorage.removeItem('user_name');
            localStorage.removeItem('user_username');
            localStorage.removeItem('user_image');
            localStorage.removeItem('user_email');
            $state.go('login');
        };

        vm.isLoggedIn = function () {
            return localStorage.getItem('token');
        };
    }
})();