/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App')
        .controller('MarkerConfirmController', MarkerConfirmController);

    // For global map page
    function MarkerConfirmController($uibModalInstance, data) {
        var vm = this;
        vm.data = data.marker;
        vm.list = data.list;
        vm.allRouteNames = [];
        vm.allMarkerIds = [];

        _.each(vm.list, function (item) {
            if (item.markerId && vm.allMarkerIds.indexOf(item.markerId) === -1){
                vm.allMarkerIds.push(item.markerId);
            }

            if (item.routeName && vm.allRouteNames.indexOf(item.routeName) === -1){
                vm.allRouteNames.push(item.routeName);
            }
        });

        if (!vm.data.route) {
            vm.data.route = '1';
        }

        vm.user = {
            'name': localStorage.getItem('user_name'),
            'username': localStorage.getItem('user_username'),
            'email': localStorage.getItem('user_email'),
            'image': localStorage.getItem('user_image')
        }

        vm.submit = function () {
            vm.submited = true;
            if (!vm.data.markerId || !vm.data.status || !vm.data.routeName) {
                return;
            }
            var canvas = document.getElementById('canvas');
            var dataURL = canvas.toDataURL();
            vm.data.image = dataURL;
            $uibModalInstance.close(vm.data);
            location.reload();
        };

        vm.cancel = function () {
            $uibModalInstance.dismiss();
        };

        vm.suggestMarkerIds = function () {
            return _.filter(vm.allMarkerIds, function(item) {
                return item.toLowerCase().indexOf(vm.data.markerId.toLowerCase()) > -1;
            });
        };

        vm.suggestRouteNames = function () {
            return _.filter(vm.allRouteNames, function(item) {
                return item.toLowerCase().indexOf(vm.data.routeName.toLowerCase()) > -1;
            });
        };

        $(function () {
            var fileUpload = document.getElementById('fileUpload');
            var canvas = document.getElementById('canvas');
            var ctx = canvas.getContext("2d");
            var canvasSize = 400;

            function readImage() {
                if (this.files && this.files[0]) {
                    var FR = new FileReader();
                    FR.onload = function (e) {
                        var img = new Image();
                        img.src = e.target.result;
                        img.onload = function () {
                            var w = img.naturalWidth;
                            var h = img.naturalHeight;
                            if (w > h) {
                                h = canvasSize * h / w;
                                w = canvasSize;
                            } else {
                                w = canvasSize * w / h;
                                h = canvasSize;
                            }
                            $("#canvas").attr('width', w);
                            $("#canvas").attr('height', h);
                            ctx.drawImage(img, 0, 0, w, h);
                        };
                    };
                    FR.readAsDataURL(this.files[0]);
                }
            }

            fileUpload.onchange = readImage;

            canvas.onclick = function (e) {
                var x = e.offsetX;
                var y = e.offsetY;
                ctx.beginPath();
                ctx.fillStyle = 'black';
                ctx.arc(x, y, 5, 0, Math.PI * 2);
                ctx.fill();
            };
        });
    }
})();
