App
    .config(function ($stateProvider) {
        $stateProvider
            .state('main', {
                url: '/',
                templateUrl: './main/main.html',
                controller: 'MainController'
            })
    })

    .controller('MainController', [
        '$http',
        '$interval',
        '$q',
        '$rootScope',
        '$scope',
        '$state',
        function ($http, $interval, $q, $rootScope, $scope, $state) {

            // Init list of data servers
            $rootScope.servers = [];

            // Static files
            var urls = [
                $http.get('./data/device_0.json'),
                $http.get('./data/device_1.json'),
                $http.get('./data/device_2.json')
            ];

            // Promise
            $q.all(urls).then(function (response) {
                _.each(response, function (item) {
                    $rootScope.servers.push(item.data);
                });

                getAllData(updateLMapMainMarker);
            });

            // Get data from a specific url
            function getData(url) {
                var defer = $q.defer();
                $.ajax({
                    url: url,
                    data: { page: 1 },
                    dataType: 'jsonp',
                }).done(function (results) {
                    defer.resolve(results);
                }).error(function () {
                    defer.reject();
                });
                return defer.promise;
            }

            // Get/refresh all data
            function getAllData(callback) {
                // Config data request
                var urls = [
                    $http.get('./data/lnglat.json'),
                    $http.get('./data/location_0.json'),
                    $http.get('./data/leftside.json')
                ];

                // Add data request to the list
                _.each($rootScope.servers, function (item) {
                    urls.push(getData(item.outputUrl + '#' + Math.floor(Math.random() * 1000000)));
                });

                // Prevent duplicated call at the same time
                if (!$scope.loading) {
                    $scope.loading = true;

                    // Get data
                    $q.all(urls).then(function (response) {
                        // COnfig
                        zoom = response[0].data.zoom;
                        locationData = response[1].data;
                        leftSidebarData = response[2].data;

                        res = [];
                        for (var i = 3; i < response.length; i++) {
                            res.push(response[i][0] ? response[i][0] : {lat: lat, lng: lng});
                            lat = response[i][0] ? response[i][0].lat : lat;
                            lng = response[i][0] ? response[i][0].lng : lng;
                            bounds.push([lng, lat])
                        }

                        // For google map
                        initLMap($state, true);
                        callback(res);
                    });
                }
            }
        }]);