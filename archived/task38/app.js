/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 * Global Modules
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    L.mapbox.accessToken = 'pk.eyJ1IjoiYnVibGluZzMzMyIsImEiOiIzZDBjMmVjY2IzYzIyM2MxODg4Nzg1N2FiYjI0MWMzOCJ9.0VASIZ01lYSOwW6-y7OILg';
    mapboxgl.accessToken = 'pk.eyJ1IjoiYnVibGluZzMzMyIsImEiOiIzZDBjMmVjY2IzYzIyM2MxODg4Nzg1N2FiYjI0MWMzOCJ9.0VASIZ01lYSOwW6-y7OILg';
    var limit = 100;

    angular
        .module('App', [
            'ngSanitize',
            'ngRoute',
            'ui.router',
            'chart.js'
        ])
        .config(routerConfig)
        .config(chartConfig)
        .controller('DatabaseController', DatabaseController)
        .controller('DatatableController', DatatableController)
        .controller('GlobalMapController', GlobalMapController)
        .controller('GlobalMap3DController', GlobalMap3DController)
        .controller('MapHistoryController', MapHistoryController)
        .controller('MapDetailController', MapDetailController)
        .controller('DatabaseDetailController', DatabaseDetailController); // New -- If you want to add new controller, you must define it here, then create the function to it

    /** @ngInject */
    function routerConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('slave', {
                url: '/slave',
                templateUrl: 'templates/slave.html',
                controller: 'DatabaseController'
            })
            // New -- add For master page
            .state('master', {
                url: '/master',
                templateUrl: 'templates/master.html', // New -- copy master.html from slave.html and do nothing else (same template)
                controller: 'DatabaseController' // New -- User same controller with slave
            })
            .state('global_map', {
                url: '/global_map',
                templateUrl: 'templates/global_map.html',
                controller: 'GlobalMapController'
            })
            .state('global_map_3d', {
                url: '/global_map_3d',
                templateUrl: 'templates/global_map_3d.html',
                controller: 'GlobalMap3DController'
            })
            .state('map_history', {
                url: '/map_history',
                templateUrl: 'templates/global_map.html',
                controller: 'MapHistoryController'
            })
            .state('map_detail', {
                url: '/map_detail/:lon/:lat/:rotate/:speed',
                templateUrl: 'templates/global_map.html',
                controller: 'MapDetailController'
            })
            .state('master_detail', {
                url: '/master_detail/:index',
                templateUrl: 'templates/master_detail.html',
                controller: 'DatabaseDetailController'
            })
            .state('slave_detail', {
                url: '/slave_detail/:index',
                templateUrl: 'templates/slave_detail.html',
                controller: 'DatabaseDetailController'
            })
            .state('datatable', {
                url: '/datatable.html',
                templateUrl: 'templates/datatable.html',
                controller: 'DatatableController'
            });

        $urlRouterProvider.otherwise('/slave');
    }

    function chartConfig(ChartJsProvider) {
        ChartJsProvider.setOptions({ colors: ['#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'] });
    }

    // For database page
    function DatabaseController($http, $scope, $state, $timeout) {
        $scope.sort = 'timestamp';
        $scope.dataIndex = [];

        function getData() {
            var dataUrl;
            if ($state.current.name === 'slave' || $state.current.name === 'database') {
                dataUrl = './DataStreams/hs_33.json';
            } else { // New -- add this condition: if not slave or database then state can only be master, as there're only 3 states that using this controller
                dataUrl = './DataStreams/hs_1.json';
            }

            // New -- Then get the data from hs_1.json or hs_33.json base on state
            $http.get(dataUrl).then(function (response1) {
                // New -- After having the output url, get data from output url
                $http.get(response1.data.outputUrl).then(function (response2) {
                    // New -- Assign data to $scope ($scope.data is the thing that can be display in template -html as {{ data }})
                    // New -- For example if data is an array [1, 2], if want to show 2 then write this {{ data[1] }} in the template , {{ data[0] }} is 1
                    $scope.data = response2.data;
                });
            });
        }

        getData();

        $scope.sortBy = function (index) {
            if ($scope.sort === index) {
                $scope.reverse = !$scope.reverse;
            } else {
                $scope.reverse = false;
                $scope.sort = index;
            }
        };

        $scope.labels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
        $scope.series = ['Series A', 'Series B'];

        $scope.chartData = [
            [65, 59, 80, 81, 56, 55, 40],
            [28, 48, 40, 19, 86, 27, 90]
        ];

        $scope.chartOptions = {
            'title': {
                'display': false
            },
            'legend': {
                'display': false
            },
            'tooltips': {
                'enabled': false
            },
            'showLines': false
        };
    }

    function DatabaseDetailController($http, $scope, $state, $timeout) {
        var zoom = 13;
        var map, color;

        function getData() {
            var dataUrl;
            if ($state.current.name === 'slave_detail') {
                dataUrl = './DataStreams/hs_33.json';
                color = 'blue';
            } else { // New -- add this condition: if not slave or database then state can only be master, as there're only 3 states that using this controller
                dataUrl = './DataStreams/hs_1.json';
                color = 'green';
            }

            // New -- Then get the data from hs_1.json or hs_33.json base on state
            $http.get(dataUrl).then(function (response1) {
                // New -- After having the output url, get data from output url
                $http.get(response1.data.outputUrl).then(function (response2) {
                    // New -- Assign data to $scope ($scope.data is the thing that can be display in template -html as {{ data }})
                    // New -- For example if data is an array [1, 2], if want to show 2 then write this {{ data[1] }} in the template , {{ data[0] }} is 1
                    $scope.data = response2.data[$state.params.index];
                    showMap($scope.data);
                });
            });
        }

        // Create main maker html
        function mainMarkerHTML() {
            return '<div class="load ' + color + '" style="transform: rotate(' + $scope.data.rotate + 'deg)" onclick="window.location.href=\'#!/map_history\'"><div class="load-pulse"><div class="load-pulse-container ' + color + '"></div></div><i class="fa fa-arrow-up"></i></div>';
        }


        // Home page
        function showMap() {
            $(function () {
                // This adds the map
                map = new mapboxgl.Map({
                    // container id specified in the HTML
                    container: 'map-detail',
                    // style URL
                    style: 'mapbox://styles/mapbox/streets-v9',
                    // initial position in [long, lat] format
                    center: [$scope.data.lat, $scope.data.lon],
                    // initial zoom
                    zoom: zoom
                });

                map.on('load', function (e) {
                    map.addLayer({
                        'id': 'slave-line',
                        'type': 'line',
                        'source': {
                            'type': 'geojson',
                            'data': {
                                'type': 'Feature',
                                'properties': {},
                                'geometry': {
                                    'type': 'LineString',
                                    'coordinates': [
                                        [$scope.data.lat, $scope.data.lon]
                                    ]
                                }
                            }
                        },
                        'layout': {
                            'line-join': 'round',
                            'line-cap': 'round'
                        },
                        'paint': {
                            'line-color': '#ff0000',
                            'line-width': 2
                        }
                    });

                    // add master marker to map
                    var el = document.createElement('div');
                    el.id = 'detail-marker';

                    if ($scope.data.opt_read >= limit) {
                        if ($state.current.name === 'slave_detail') {
                            color = 'orange';
                        } else { // New -- add this condition: if not slave or database then state can only be master, as there're only 3 states that using this controller
                            color = 'red';
                        }
                    }

                    el.innerHTML = mainMarkerHTML();
                    new mapboxgl.Marker(el)
                        .setLngLat([$scope.data.lat, $scope.data.lon])
                        .addTo(map);
                });
            });
        }

        getData();

        $scope.labels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
        $scope.series = ['Series A', 'Series B'];

        $scope.chartData = [
            [65, 59, 80, 81, 56, 55, 40],
            [28, 48, 40, 19, 86, 27, 90]
        ];
    }

    // For global map page
    function GlobalMapController($http, $interval, $scope, $timeout) {

        var firstLoad = true;
        var mainMarkerShown = false;

        var zoom = 13;

        var map, masterLat, masterLon, slaveLat, slaveLon;

        var masterResponse, slaveResponse;

        // Create main maker html
        function mainMarkerHTML(data, color) {
            return '<div class="load ' + color + '" style="transform: rotate(' + data.rotate + 'deg)" onclick="window.location.href=\'#!/map_history\'"><div class="load-pulse"><div class="load-pulse-container ' + color + '"></div></div><i class="fa fa-arrow-up"></i></div>';
        }

        function getMasterData() {
            // Get data from phant (the url get from json files)
            $http.get(masterResponse.data.outputUrl).then(function (masterJSON) {
                var list = masterJSON.data;

                if (firstLoad) {
                    // Initialize the map for the first time data get
                    showMap(list[0].lat, list[0].lon, masterResponse);
                    firstLoad = false;

                    // If lat or lon are updated
                } else if (masterLat != list[0].lat || masterLon != list[0].lon) {
                    masterLat = list[0].lat;
                    masterLon = list[0].lon;

                    // Remove old master marker from map
                    var element = document.getElementById('master-marker');
                    if (element) {
                        element.parentNode.removeChild(element);
                    }

                    // add master marker to map
                    var el = document.createElement('div');
                    el.id = 'master-marker';

                    var color = 'blue';
                    if (list[0].opt_read >= limit) {
                        color = 'orange';
                    }

                    el.innerHTML = mainMarkerHTML(list[0], color);
                    new mapboxgl.Marker(el)
                        .setLngLat([masterLat, masterLon])
                        .addTo(map);

                    if (!slaveResponse) {
                        $http.get('./DataStreams/hs_33.json').then(function (res) {
                            slaveResponse = res;
                            getSlaveData(list[0]);
                        });
                    } else {
                        getSlaveData(list[0]);
                    }
                }
            });
        }

        function getSlaveData(masterData) {
            // Get data from phant (the url get from json files)
            $http.get(slaveResponse.data.outputUrl).then(function (res) {
                var list = res.data;
                if (slaveLat != list[0].lat || slaveLon != list[0].lon) {
                    slaveLat = list[0].lat;
                    slaveLon = list[0].lon;

                    // Remove old slave marker from map
                    var element = document.getElementById('slave-marker');
                    if (element) {
                        element.parentNode.removeChild(element);
                    }

                    // add slave markers to map
                    var el = document.createElement('div');
                    el.id = 'slave-marker';

                    var color = 'green';
                    if (masterData.opt_read >= limit) {
                        color = 'red';
                    }
                    el.innerHTML = mainMarkerHTML(list[0], color);
                    // add marker to map
                    new mapboxgl.Marker(el)
                        .setLngLat([slaveLat, slaveLon])
                        .addTo(map);

                    var coordinates = [[masterLat, masterLon]];
                    if (masterData.opt_read >= limit) {
                        coordinates.push([slaveLat, slaveLon]);
                    }

                    map.getSource('slave-line').setData({
                        'type': 'Feature',
                        'properties': {},
                        'geometry': {
                            'type': 'LineString',
                            'coordinates': coordinates
                        }
                    });
                }
            });
        }


        // Home page
        function showMap(masterLat, masterLon, lineColor) {
            $(function () {
                // This adds the map
                map = new mapboxgl.Map({
                    // container id specified in the HTML
                    container: 'map',
                    // style URL
                    style: 'mapbox://styles/mapbox/streets-v9',
                    // initial position in [long, lat] format
                    center: [masterLat, masterLon],
                    // initial zoom
                    zoom: zoom
                });

                map.on('load', function (e) {
                    map.addLayer({
                        'id': 'slave-line',
                        'type': 'line',
                        'source': {
                            'type': 'geojson',
                            'data': {
                                'type': 'Feature',
                                'properties': {},
                                'geometry': {
                                    'type': 'LineString',
                                    'coordinates': [
                                        [masterLat, masterLon]
                                    ]
                                }
                            }
                        },
                        'layout': {
                            'line-join': 'round',
                            'line-cap': 'round'
                        },
                        'paint': {
                            'line-color': '#ff0000',
                            'line-width': 2
                        }
                    });

                    $timeout(function () {
                        getMasterData();
                    }, 1000);
                });
            });
        }

        $scope.boot = function () {
            $http.get('./DataStreams/hs_1.json').then(function (res) {
                if (res.data) {
                    masterResponse = res;
                    getMasterData();
                }
            });
        };
    }

    // For global map page
    function GlobalMap3DController($http, $scope, $state, $timeout) {

        var firstLoad = true;
        var mainMarkerShown = false;

        var zoom = 13;

        var map, masterLat, masterLon, slaveLat, slaveLon, masterObj, slaveObj, stores;

        var masterResponse, slaveResponse;

        // Create main maker html
        function mainMarkerHTML(data, color) {
            return '<div class="load ' + color + '" style="transform: rotate(' + data.rotate + 'deg)" onclick="window.location.href=\'#!/map_history\'"><div class="load-pulse"><div class="load-pulse-container ' + color + '"></div></div><i class="fa fa-arrow-up"></i></div>';
        }

        function getMasterData() {
            // Get data from phant (the url get from json files)
            $http.get(masterResponse.data.outputUrl).then(function (masterJSON) {
                var list = masterJSON.data;

                if (firstLoad) {
                    // Initialize the map for the first time data get
                    showMap(list[0].lat, list[0].lon, masterResponse);
                    firstLoad = false;

                    // If lat or lon are updated
                } else if (masterLat != list[0].lat || masterLon != list[0].lon) {
                    masterLat = list[0].lat;
                    masterLon = list[0].lon;
                    masterObj = list[0];

                    // Remove old master marker from map
                    var element = document.getElementById('master-marker');
                    if (element) {
                        element.parentNode.removeChild(element);
                    }

                    // add master marker to map
                    // Create an img element for the marker
                    var el = document.createElement('div');
                    el.id = 'marker-master';
                    el.className = 'marker';
                    // Add markers to the map at all points
                    new mapboxgl.Marker(el, { offset: [-28, -46] })
                        .setLngLat([masterLat, masterLon])
                        .addTo(map);

                    el.addEventListener('click', function (e) {
                        // 1. Fly to the point
                        flyToStore([masterLat, masterLon]);

                        // 2. Close all other popups and display popup for clicked store
                        createPopUp({
                            'type': 'Feature',
                            'geometry': {
                                'type': 'Point',
                                'coordinates': [masterLat, masterLon]
                            },
                            'properties': masterObj
                        });

                        // 3. Highlight listing in sidebar (and remove highlight for all other listings)
                        var activeItem = document.getElementsByClassName('active');

                        e.stopPropagation();
                        if (activeItem[0]) {
                            activeItem[0].classList.remove('active');
                        }

                        var listing = document.getElementById('listing-' + i);
                        listing.classList.add('active');

                    });

                    if (!slaveResponse) {
                        $http.get('./DataStreams/hs_33.json').then(function (res) {
                            slaveResponse = res;
                            getSlaveData(list[0]);
                        });
                    } else {
                        getSlaveData(list[0]);
                    }
                }
            });
        }

        function getSlaveData(masterData) {
            // Get data from phant (the url get from json files)
            $http.get(slaveResponse.data.outputUrl).then(function (res) {
                var list = res.data;
                if (slaveLat != list[0].lat || slaveLon != list[0].lon) {
                    slaveLat = list[0].lat;
                    slaveLon = list[0].lon;
                    slaveObj = list[0];

                    // Remove old slave marker from map
                    var element = document.getElementById('slave-marker');
                    if (element) {
                        element.parentNode.removeChild(element);
                    }

                    // add slave marker to map
                    // Create an img element for the marker
                    var el = document.createElement('div');
                    el.id = 'marker-slave';
                    el.className = 'marker';
                    // Add markers to the map at all points
                    new mapboxgl.Marker(el, { offset: [-28, -46] })
                        .setLngLat([slaveLat, slaveLon])
                        .addTo(map);

                    el.addEventListener('click', function (e) {
                        // 1. Fly to the point
                        flyToStore([slaveLat, slaveLon]);

                        // 2. Close all other popups and display popup for clicked store
                        createPopUp({
                            'type': 'Feature',
                            'geometry': {
                                'type': 'Point',
                                'coordinates': [slaveLat, slaveLon]
                            },
                            'properties': slaveObj
                        });

                        // 3. Highlight listing in sidebar (and remove highlight for all other listings)
                        var activeItem = document.getElementsByClassName('active');

                        e.stopPropagation();
                        if (activeItem[0]) {
                            activeItem[0].classList.remove('active');
                        }

                        var listing = document.getElementById('listing-' + i);
                        listing.classList.add('active');

                    });

                    var coordinates = [[masterLat, masterLon]];
                    if (masterData.opt_read >= limit) {
                        coordinates.push([slaveLat, slaveLon]);
                    }

                    map.getSource('slave-line').setData({
                        'type': 'Feature',
                        'properties': {},
                        'geometry': {
                            'type': 'LineString',
                            'coordinates': coordinates
                        }
                    });
                    console.log(list[0]);
                    stores = {
                        'type': 'FeatureCollection',
                        'features': [{
                            'type': 'Feature',
                            'geometry': {
                                'type': 'Point',
                                'coordinates': [masterLat, masterLon]
                            },
                            'properties': masterObj
                        }, {
                            'type': 'Feature',
                            'geometry': {
                                'type': 'Point',
                                'coordinates': [slaveLat, slaveLon]
                            },
                            'properties': slaveObj
                        }]
                    };
                    buildLocationList(stores);
                }
            });
        }


        // Home page
        function showMap(masterLat, masterLon, lineColor) {
            $(function () {
                // This adds the map
                map = new mapboxgl.Map({
                    // container id specified in the HTML
                    container: 'map-3d',
                    // style URL
                    style: 'mapbox://styles/bubling333/cj64257x950kn2spanw0usbys',
                    // initial position in [long, lat] format
                    center: [masterLat, masterLon],
                    // initial zoom
                    zoom: zoom
                });

                map.on('load', function (e) {
                    map.addLayer({
                        'id': 'slave-line',
                        'type': 'line',
                        'source': {
                            'type': 'geojson',
                            'data': {
                                'type': 'Feature',
                                'properties': {},
                                'geometry': {
                                    'type': 'LineString',
                                    'coordinates': [
                                        [masterLat, masterLon]
                                    ]
                                }
                            }
                        },
                        'layout': {
                            'line-join': 'round',
                            'line-cap': 'round'
                        },
                        'paint': {
                            'line-color': '#ff0000',
                            'line-width': 2
                        }
                    });

                    $timeout(function () {
                        getMasterData();
                    }, 1000);
                });
            });
        }

        $scope.boot = function () {
            $http.get('./DataStreams/hs_1.json').then(function (res) {
                if (res.data) {
                    masterResponse = res;
                    getMasterData();
                }
            });
        };


        function flyToStore(latlon) {
            map.flyTo({
                center: latlon,
                zoom: zoom
            });
        }

        function createPopUp(currentFeature) {
            var popUps = document.getElementsByClassName('mapboxgl-popup');
            if (popUps[0]) popUps[0].remove();

            //  Marker Information - currentFeature.properties.id or currentFeature.properties.address
            var popup = new mapboxgl.Popup({ closeOnClick: false })
                .setLngLat(currentFeature.geometry.coordinates)
                .setHTML('<h3>' + currentFeature.properties.licence + '</h3>' +

                //  Marker Information - currentFeature.properties.id or currentFeature.properties.address
                //  currentFeature.properties.name seemed to cause issue so explicity set name instead
                /*
                '<h4>' + currentFeature.properties.id + '</h4>')
                 .addTo(map);
                */

                '<h4>' + currentFeature.properties.name + '</h4>')
                .addTo(map);
        }


        function buildLocationList(data) {
            for (var i = 0; i < data.features.length; i++) {
                var currentFeature = data.features[i];
                var prop = currentFeature.properties;

                var listings = document.getElementById('listings');
                var listing = listings.appendChild(document.createElement('div'));
                listing.className = 'item';
                listing.id = "listing-" + i;

                var link = listing.appendChild(document.createElement('a'));
                link.href = 'javascript:;';
                link.className = 'title';
                link.dataPosition = i;

                //  Here is where you set the First Listing i.e prop.address or prop.licence
                link.innerHTML = prop.licence;


                /*
                for (var index in prop) {
                    if (index !== 'address') {
                        var details = listing.appendChild(document.createElement('div'));
                        details.innerHTML += '<strong>' + index + '</strong>: ' + prop[index];
                    }
                }
                */


                //  Here is where you structure the listings
                var attr = ['timestamp', 'name', 'address', 'lon', 'lat', 'speed', 'rotate', 'id'];
                for (var index in attr) {
                    var details = listing.appendChild(document.createElement('div'));

                    //  Attr is the listing headers i.e. 0 = timestamp, 1 = name etc // just put index for entire object array
                    details.innerHTML += '<strong>' + attr[index] + '</strong>: ' + '<br>' + prop[attr[index]] + '<br><br>';


                }




                link.addEventListener('click', function (e) {
                    // Update the currentFeature to the store associated with the clicked link
                    var clickedListing = data.features[this.dataPosition];

                    // 1. Fly to the point
                    flyToStore(clickedListing.geometry.coordinates);

                    // 2. Close all other popups and display popup for clicked store
                    createPopUp(clickedListing);

                    // 3. Highlight listing in sidebar (and remove highlight for all other listings)
                    var activeItem = document.getElementsByClassName('active');

                    if (activeItem[0]) {
                        activeItem[0].classList.remove('active');
                    }
                    this.parentNode.classList.add('active');

                });
            }
        }
    }

    // For global map page
    function MapHistoryController($http, $scope, $timeout) {

        var map, list, color;

        var zoom = 13;

        // Create main maker html
        function mainMarkerHTML(data, i) {
            var speedClass = generateSpeedClass(data[i].speed);
            if (!i) {
                return '<div class="load' + speedClass + '" style="transform: rotate(' + data[i].rotate + 'deg)" onclick="window.location.href=\'#!/map_detail/' + data[i].lon + '/' + data[i].lat + '/' + data[i].rotate + '/' + data[i].speed + '\'"><div class="load-pulse"><div class="load-pulse-container' + speedClass + '"></div></div><i class="fa fa-arrow-up"></i></div>';
            } else {
                return '<div class="load' + speedClass + '" style="transform: rotate(' + data[i].rotate + 'deg)" onclick="window.location.href=\'#!/map_detail/' + data[i].lon + '/' + data[i].lat + '/' + data[i].rotate + '/' + data[i].speed + '\'"><i class="fa fa-arrow-up"></i></div>';
            }
        }

        function getData() {
            // Get data from json files
            $http.get('./DataStreams/hs_1.json').then(function (response1) {
                // Get data from phant (the url get from json files)
                $http.get(response1.data.outputUrl).then(function (response2) {
                    list = response2.data;
                    showMap();
                });
            });
        }


        // DATA TABLE
        window.setInterval(function () {
            phantCall();
        }, 1000);

        function phantCall() {
            $.ajax({
                //url: './assets/articles.json',
                url: 'http://138.68.160.161:8080/output/JdQ20eWO0BSX40K8oljbIm2glj0.json',
                dataType: 'json',
                type: 'get',
                cache: false,
                success: function (data) {
                    $(data).each(function (index, value) {
                        document.getElementById("lat").innerHTML = data[0].lat;
                        document.getElementById("lon").innerHTML = data[0].lon;
                        document.getElementById("elapsed_time").innerHTML = data[0].elapsed_time;
                        document.getElementById("opt_read").innerHTML = data[0].opt_read;
                        document.getElementById("sensor_1").innerHTML = data[0].sensor_1;
                        document.getElementById("timestamp").innerHTML = data[0].timestamp;
                    });
                }
            });
        }

        // Home page
        function showMap() {
            $(function () {
                // This adds the map
                map = new mapboxgl.Map({
                    // container id specified in the HTML
                    container: 'map',
                    // style URL
                    style: 'mapbox://styles/bubling333/cj64257x950kn2spanw0usbys',
                    // initial position in [long, lat] format
                    center: [list[0].lat, list[0].lon],
                    // initial zoom
                    zoom: zoom
                });

                map.on('load', function (e) {
                    for (var i in list) {
                        // add master marker to map
                        var el = document.createElement('div');
                        el.id = 'marker-' + i;

                        el.innerHTML = mainMarkerHTML(list, i);
                        new mapboxgl.Marker(el)
                            .setLngLat([list[i].lat, list[i].lon])
                            .addTo(map);
                    }
                });
            });
        }

        $scope.boot = function () {
            $timeout(getData, 100);
        };
    }

    // For map detail page
    function MapDetailController($http, $scope, $state, $timeout) {
        var lat = $state.params.lat;
        var lon = $state.params.lon;
        var rotate = $state.params.rotate;
        var speed = $state.params.speed;

        // Init the map
        var mapTwo = L.mapbox.map('map', 'mapbox.light')
            .setView([lon, lat], 4);

        var myLayer = L.mapbox.featureLayer().addTo(mapTwo);

        var geojson = [];

        myLayer.on('layeradd', function (e) {
            var marker = e.layer,
                feature = marker.feature;
            marker.setIcon(L.divIcon(feature.properties.icon));
        });

        myLayer.setGeoJSON(geojson);

        mapTwo.scrollWheelZoom.disable();

        function markerHTML() {
            var speedClass = generateSpeedClass(speed);
            return '<div class="load' + speedClass + '" style="transform: rotate(' + rotate + 'deg)" onclick="window.location.href=\'#!/global_map\'"><div class="load-pulse"><div class="load-pulse-container' + speedClass + '"></div></div><i class="fa fa-arrow-up"></i></div>';
        }

        $scope.boot = function () {
            $timeout(function () {
                // Get data
                $(function () {
                    geojson = [{
                        type: 'Feature',
                        geometry: {
                            type: 'Point',
                            coordinates: [lat, lon]
                        },
                        properties: {
                            icon: {
                                html: markerHTML() // add content inside the marker
                            }
                        }
                    }];
                    myLayer.setGeoJSON(geojson);
                });
            }, 100);
        };
    }

    // New -- Add new controller as a function here
    // Define $http and $scope, they're both Angular native variable so you don't have to try to understand where are they come from
    // $scope is using for assign data to template
    // $http is using to get data from url
    function DatatableController($http, $scope) {
        function getData() {
            // New -- Then get the data from hs_1.json
            $http.get('./DataStreams/hs_1.json').then(function (response1) {
                // New -- After having the output url, get data from output url
                $http.get(response1.data.outputUrl).then(function (response2) {
                    // New -- Assign data to $scope ($scope.data is the thing that can be display in template -html as {{ data }})
                    // New -- For example if data is an array [1, 2], if want to show 2 then write this {{ data[1] }} in the template , {{ data[0] }} is 1
                    $scope.data = response2.data;
                });
            });
        }

        getData();
    }

    function generateSpeedClass(speed) {
        if (speed < 1) {
            return ' white';
        } else if (speed < 16) {
            return ' green';
        } else if (speed < 31) {
            return ' dark-green';
        } else if (speed < 46) {
            return ' blue';
        } else if (speed < 61) {
            return ' dark-blue';
        } else if (speed < 71) {
            return ' yellow';
        } else if (speed < 81) {
            return ' dark-yellow';
        } else if (speed < 86) {
            return ' purple';
        } else if (speed < 91) {
            return ' dark-purple';
        } else if (speed < 100) {
            return ' red';
        } else {
            return ' dark-red';
        }
    }
})();