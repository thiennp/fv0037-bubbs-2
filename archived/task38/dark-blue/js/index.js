$(function() {
  $('<img/>').attr('src', 'https://i.pinimg.com/originals/dc/c7/a3/dcc7a3ca8fbb46d71eac350d92ad3748.jpg').load(function() {
    $('.bg-img').append($(this));
    // simulate loading
    setTimeout(function() { 
     $('.container').addClass('loaded'); 
    }, 1500)
   //$(this).remove(); // prevent memory leaks as @benweet suggested
  });
  $('.form-toggle').on('click', function() {
    $('.container').toggleClass('show-register')
  })
})