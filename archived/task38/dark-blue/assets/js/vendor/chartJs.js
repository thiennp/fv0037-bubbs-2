$(function () {




    var json = (function () {
        var json = null;
        $.ajax({
            'async': false,
            'global': false,
            'url': 'http://138.68.160.161:8080/output/Yl6e6EvQvGT4MrkO3OvYhAP2wGJ.json',
            'dataType': "json",
            'success': function (data) {
                json = data;
            }
        });
        return json;
    })();



    var data_1 = json[0].data_1;
    var data_2 = json[0].data_2;
    var data_3 = json[0].data_3;
    var data_4 = json[0].data_4;
    var data_5 = json[0].data_5;
    var data_6 = json[0].data_6;
    var data_7 = json[0].data_7;
    var timestamp = json.timestamp;


    console.log(json[0].data_1)
    console.log(json[0].data_2)
    console.log(json[0].data_3)
    console.log(json[0].data_4)
    console.log(json[0].data_5)
    console.log(json[0].data_6)
    console.log(json[0].data_7)
    console.log(json[0].timestamp)


    var lineData = {
        labels: ["January_1", "February", "March", "April", "May", "June", "July"],
        datasets: [
            {
                label: "Example dataset",
                fillColor: "rgba(41,171,164,0.5)",
                strokeColor: "rgba(41,171,164,0.7)",
                pointColor: "rgba(41,171,164,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(41,171,164,1)",
                data: [data_1, data_2, data_3, data_4, data_5, data_6, data_7]
            }
        ]
    };


    /*
    // DOUBLE LINE IN ONE CHARTS
        var lineData = {
            labels: ["January_1", "February", "March", "April", "May", "June", "July"],
            datasets: [
                {
                    label: "Example dataset",
                    fillColor: "rgba(20,20,220,0.5)",
                    strokeColor: "rgba(20,20,20,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#D4D4D4",
                    pointHighlightFill: "#D4D4D4",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: [data_1, data_2, data_3, data_4, data_5, data_6, data_7]
                },
                {
                    label: "Example dataset",
                    fillColor: "rgba(41,171,164,0.5)",
                    strokeColor: "rgba(41,171,164,0.7)",
                    pointColor: "rgba(41,171,164,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(41,171,164,1)",
                    data: [28, 48, 40, 19, 86, 27, 90]
                }
            ]
        };
    */
    var lineOptions = {
        scaleShowGridLines: true,
        scaleGridLineColor: "rgba(0,0,0,.05)",
        scaleGridLineWidth: 1,
        bezierCurve: true,
        bezierCurveTension: 0.4,
        pointDot: true,
        pointDotRadius: 4,
        pointDotStrokeWidth: 1,
        pointHitDetectionRadius: 20,
        datasetStroke: true,
        datasetStrokeWidth: 2,
        datasetFill: true,
        responsive: true,
    };


    var ctx = document.getElementById("lineChart").getContext("2d");
    var myNewChart = new Chart(ctx).Line(lineData, lineOptions);

    var barData = {
        labels: ["January_1", "February", "March", "April", "May", "June", "July"],
        datasets: [/*
            {
                label: "My First dataset",
                fillColor: "rgba(220,220,220,0.5)",
                strokeColor: "rgba(220,220,220,0.8)",
                highlightFill: "rgba(220,220,220,0.75)",
                highlightStroke: "rgba(220,220,220,1)",
                data: [65, 59, 80, 81, 56, 55, 40]
            },*/
            {
                label: "My Second dataset",
                fillColor: "rgba(41,171,164,0.5)",
                strokeColor: "rgba(41,171,164,0.8)",
                highlightFill: "rgba(41,171,164,0.75)",
                highlightStroke: "rgba(41,171,164,1)",
                data: [data_1, data_2, data_3, data_4, data_5, data_6, data_7]
            }
        ]
    };

    var barOptions = {
        scaleBeginAtZero: true,
        scaleShowGridLines: true,
        scaleGridLineColor: "rgba(0,0,0,.05)",
        scaleGridLineWidth: 1,
        barShowStroke: true,
        barStrokeWidth: 2,
        barValueSpacing: 5,
        barDatasetSpacing: 1,
        responsive: true
    }


    var ctx = document.getElementById("barChart").getContext("2d");
    var myNewChart = new Chart(ctx).Bar(barData, barOptions);

    var polarData = [
        {
            value: data_1,
            color: "#29aba4",
            highlight: "#1ab394",
            label: "Data_1"
        },
        {
            value: data_2,
            color: "#dedede",
            highlight: "#1ab394",
            label: "data_2"
        },
        {
            value: data_3,
            color: "#b5b8cf",
            highlight: "#1ab394",
            label: "data_3"
        }
    ];

    var polarOptions = {
        scaleShowLabelBackdrop: true,
        scaleBackdropColor: "rgba(255,255,255,0.75)",
        scaleBeginAtZero: true,
        scaleBackdropPaddingY: 1,
        scaleBackdropPaddingX: 1,
        scaleShowLine: true,
        segmentShowStroke: true,
        segmentStrokeColor: "#fff",
        segmentStrokeWidth: 2,
        animationSteps: 100,
        animationEasing: "easeOutBounce",
        animateRotate: true,
        animateScale: false,
        responsive: true,

    };

    var ctx = document.getElementById("polarChart").getContext("2d");
    var myNewChart = new Chart(ctx).PolarArea(polarData, polarOptions);

    var doughnutData = [
        {
            value: 300,
            color: "#29aba4",
            highlight: "#1ab394",
            label: "App"
        },
        {
            value: 50,
            color: "#dedede",
            highlight: "#1ab394",
            label: "Software"
        },
        {
            value: 100,
            color: "#b5b8cf",
            highlight: "#1ab394",
            label: "Laptop"
        }
    ];

    var doughnutOptions = {
        segmentShowStroke: true,
        segmentStrokeColor: "#fff",
        segmentStrokeWidth: 2,
        percentageInnerCutout: 45, // This is 0 for Pie charts
        animationSteps: 100,
        animationEasing: "easeOutBounce",
        animateRotate: true,
        animateScale: false,
        responsive: true,
    };


    var ctx = document.getElementById("doughnutChart").getContext("2d");
    var myNewChart = new Chart(ctx).Doughnut(doughnutData, doughnutOptions);


    var radarData = {
        labels: ["data_1", "data_2", "data_3", "data_4", "data_5", "data_6", "data_7"],
        datasets: [/*
            {
                label: "My First dataset",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [data_1, data_2, data_3, data_4, data_5, data_6, data_7]
            },*/
            {
                label: "My Second dataset",
                fillColor: "rgba(41,171,164,0.2)",
                strokeColor: "rgba(41,171,164,1)",
                pointColor: "rgba(41,171,164,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: [data_1, data_2, data_3, data_4, data_5, data_6, data_7]
            }
        ]
    };

    var radarOptions = {
        scaleShowLine: true,
        angleShowLineOut: true,
        scaleShowLabels: false,
        scaleBeginAtZero: true,
        angleLineColor: "rgba(0,0,0,.1)",
        angleLineWidth: 1,
        pointLabelFontFamily: "'Arial'",
        pointLabelFontStyle: "normal",
        pointLabelFontSize: 10,
        pointLabelFontColor: "#666",
        pointDot: true,
        pointDotRadius: 3,
        pointDotStrokeWidth: 1,
        pointHitDetectionRadius: 20,
        datasetStroke: true,
        datasetStrokeWidth: 2,
        datasetFill: true,
        responsive: true,
    }

    var ctx = document.getElementById("radarChart").getContext("2d");
    var myNewChart = new Chart(ctx).Radar(radarData, radarOptions);

});
