$(function() {
    $("span.pie").peity("pie", {
        fill: ['#29aba4', '#d7d7d7', '#ffffff']
    })

    $(".line").peity("line",{
        fill: '#z',
        stroke:'#169c81',
    })

    $(".bar").peity("bar", {
        fill: ["#29aba4", "#d7d7d7"]
    })

    $(".bar_dashboard").peity("bar", {
        fill: ["#29aba4", "#d7d7d7"],
        width:100
    })

    $('.donut').peity('donut', {
        fill: ['#29aba4', '#d7d7d7', '#ffffff']
    })

    var updatingChart_0 = $(".updating-chart_0").peity("line", { fill: null,stroke:'#04637f', width: 64 })
    var updatingChart_1 = $(".updating-chart_1").peity("line", { fill: null,stroke:'#04637f', width: 64 })
    var updatingChart_2 = $(".updating-chart_2").peity("line", { fill: null,stroke:'#04637f', width: 64 })

    setInterval(function() {
        var random = Math.round(Math.random() * 10)
        var values = updatingChart_0.text().split(",")
        values.shift()
        values.push(random)

        updatingChart_0
            .text(values.join(","))
            .change()
    }, 1000);

    setInterval(function() {
        var random = Math.round(Math.random() * 10)
        var values = updatingChart_1.text().split(",")
        values.shift()
        values.push(random)

        updatingChart_1
            .text(values.join(","))
            .change()
    }, 1000);

    setInterval(function() {
        var random = Math.round(Math.random() * 10)
        var values = updatingChart_2.text().split(",")
        values.shift()
        values.push(random)

        updatingChart_2
            .text(values.join(","))
            .change()
    }, 1000);
});