
var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";

var targetLighthouse = "M63.625,95.425l-3.888-66.099h2.513v-2.592h-3.888V15.501h3.888L50.586,3.837L38.921,15.501h3.888v11.232  h-3.888v2.592h2.514l-3.888,66.099H63.625z M55.771,15.501v9.937H45.402v-9.937H55.771z M52.541,92.833v-4.985h-3.91v4.985h-8.335  l0.435-7.385l19.148-9.574l0.997,16.959H52.541z M42.969,47.391l14.799-7.399l0.885,15.058l-16.638,8.549L42.969,47.391z";

var planeSVG = "M19.671,8.11l-2.777,2.777l-3. 837-0.861c0.362-0.505,0.916-1. 683,0.464-2.135c-0.518-0.517-1 .979,0.278-2.305, 0.604l-0.913, 0.913L7.614.8.804l-2.021.2. 021l2.232,1,061l -0.082.0.082l1 .701.1.701l0.688-0.687l3.164, 1.504L9.571,18.21H6.413l -1. 137,1.138l3.6,0.948l1.83,1.83l 0.947,3.598l1.137-1.137V21.43l 3.725-3.725l1.504,3.164l-0687 , 0.687l1.702,1.701l0.081- 0.081 l1.062.2.231l2.02-2.02l-0.604-2689l0.912-0.912c0.326-0.326 , 1,121-1,789,0,604-2,306c- 0,452-0,452-1,63,0,101-2,135.0. 464l-0.861-3.838l2.777-2.777c0 .947-0.947,3.599-4.862,2.62-5. 839C24.533,4.512,20.618,7.163, 19,671, 8.11z";



var londonLocation = [0.1278, 51.5074];

var dataProvider = {
    "map": "worldLow",
    "linkToObject": "london",
    "zoomLatitude": londonLocation[0],
    "zoomLongitude": londonLocation[1],
    "images": [{
        "id": "london",
        "color": "#000000",
        "svgPath": targetSVG,
        "title": "Whitby Lighthouse, North Yorkshire, England",
        "scale": 1.5,
        "zoomLevel": 2.74,
        "latitude": 46.9480,
        "longitude": 7.4481,

        "images": [{
            "label": "Flights from London",
            "svgPath": planeSVG,
            "left": 100,
            "top": 45,
            "labelShiftY": 5,
            "color": "#CC0000",
            "labelColor": "#CC0000",
            "labelRollOverColor": "#CC0000",
            "labelFontSize": 20
        }, {
            "label": "show flights from Vilnius",
            "left": 106,
            "top": 70,
            "labelColor": "#000000",
            "labelRollOverColor": "#CC0000",
            "labelFontSize": 11,
            "linkToObject": "vilnius"
        }]
    }, {
    }],
    "areasSettings": {
        "autoZoom": true
    }
};
/**
 * Create the map
 */
var chart;

setDataSet();

setInterval(setDataSet, 3000)

function setDataSet() {
    $.ajax({
        url: 'http://138.68.160.161:8080/output/JdQ20eWO0BSX40K8oljbIm2glj0.json',
        dataType: 'json',
        type: 'get',
        cache: false,
        success: function (data) {
// console.log(data, data[0], data[0].lat, data[0].lon);

            if (data[0].opt_read == 0) {

                dataProvider.images[1] = {
                    "svgPath": targetSVG,
                    "title": "Bern",
                    "latitude": data[0].lat,
                    "longitude": data[0].lon
                };
                dataProvider.lines = [{
                    latitudes: [47, data[0].lat],
                    longitudes: [7.5, data[0].lon]
                }];
            } else {
                dataProvider.images[1] = {};
                dataProvider.lines = [];
            }
            if (!chart) {

                chart = AmCharts.makeChart("mapdiv", {
                    "type": "map",
                    "mouseWheelZoomEnabled": true,
                    "allowClickOnSelectedObject": true,
                    "theme": "light",
                    "dataProvider": dataProvider,
                    "areasSettings": {
                        "autoZoom": true
                    },
                    "areasSettings": {
                        "unlistedAreasColor": "#FFCC00"
                    },
                    "imagesSettings": {
                        "color": "#CC0000",
                        "rollOverColor": "#CC0000",
                        "selectedColor": "#000000"
                    },
                    "linesSettings": {
                        "color": "#CC0000",
                        "alpha": 0.4
                    },
                    "balloon": {
                        "drop": true
                    },
                    "backgroundZoomsToTop": true,
                    "linesAboveImages": true,
                    "export": {
                        "enabled": true
                    }
                });
            } else {
                chart.dataProvider = dataProvider;
                chart.dataProvider.zoomLevel = chart.zoomLevel();
                chart.dataProvider.zoomLatitude = chart.zoomLatitude();
                // THIS TO STOP THE CHART BOUNCING BUT UNSURE WHY THE MARKER NOT DRAWN
                // delete chart.images.zoomLevel;
                chart.dataProvider.zoomLongitude = chart.zoomLongitude();
                delete chart.dataProvider.images[0].zoomLevel;

// update map
                chart.validateData();
            }
        }
    });
}
