// svg path for target icon
var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";

var targetLighthouse = "M63.625,95.425l-3.888-66.099h2.513v-2.592h-3.888V15.501h3.888L50.586,3.837L38.921,15.501h3.888v11.232  h-3.888v2.592h2.514l-3.888,66.099H63.625z M55.771,15.501v9.937H45.402v-9.937H55.771z M52.541,92.833v-4.985h-3.91v4.985h-8.335  l0.435-7.385l19.148-9.574l0.997,16.959H52.541z M42.969,47.391l14.799-7.399l0.885,15.058l-16.638,8.549L42.969,47.391z";

// svg path for plane icon
var planeSVG = "M63.625,95.425l-3.888-66.099h2.513v-2.592h-3.888V15.501h3.888L50.586,3.837L38.921,15.501h3.888v11.232  h-3.888v2.592h2.514l-3.888,66.099H63.625z M55.771,15.501v9.937H45.402v-9.937H55.771z M52.541,92.833v-4.985h-3.91v4.985h-8.335  l0.435-7.385l19.148-9.574l0.997,16.959H52.541z M42.969,47.391l14.799-7.399l0.885,15.058l-16.638,8.549L42.969,47.391z";

window.setInterval(function() {
    phantCall();
}, 3000);


function phantCall() {
    $.ajax({
//url: './assets/articles.json',
        url: 'http://138.68.160.161:8080/output/JdQ20eWO0BSX40K8oljbIm2glj0.json',
        dataType: 'json',
        type: 'get',
        cache: false,
        success: function (data) {

            $(data).each(function (index, value) {

                // document.getElementById("lat").innerHTML = data[0].lat;
                // document.getElementById("lon").innerHTML = data[0].lon;
                document.getElementById("elapsed_time").innerHTML = data[0].elapsed_time;
                // document.getElementById("opt_read").innerHTML = data[0].opt_read;
                document.getElementById("sensor_1").innerHTML = data[0].sensor_1;
                // document.getElementById("timestamp").innerHTML = data[0].timestamp;

                console.log(data[0].elapsed_time)
                console.log(data[0].sensor_1)
                console.log(data[0].lat)
                console.log(data[0].lon)



            });
        }
    });
}

// latitudes: [54.28250494415543, 54.47774597362849], longitudes: [-0.39036737393757903, -0.567533984893771]


AmCharts.makeChart("mapdiv", {
    type: "map",
    addClassNames: true,

    dataProvider: {
        map: "worldLow",
        allowClickOnSelectedObject: true,
        allowMultipleDescriptionWindows: true,
        getAreasFromMap: true,
        linkToObject: "london",
        images: [{
            id: "london",
            color: "#000000",

            title: "Whitby Lighthouse, North Yorkshire, England",
            latitude: 54.49283740924501,
            longitude: -0.612874381357642,
            scale: 1.5,
            zoomLevel: 5,
            zoomLatitude: 54.49283740924501,
            zoomLongitude: -0.612874381357642,



            lines: [ {
                latitudes: [56.89398954749854, 54.49283740924501],
                longitudes: [2.9543556495366374, -0.612874381357642]
            }],

            images: [{
                label: "Whitby Lighthouse | Distress Signal Detected",
                svgPath: planeSVG,
                scale: 0.7,
                left: 100,
                top: 55,
                labelShiftY: 4,
                color: "#CC0000",
                labelColor: "#CC0000",
                labelRollOverColor: "#CC0000",
                labelFontSize: 20
            }, {
                label: "",
                left: 106,
                top: 70,
                labelColor: "#000000",
                labelRollOverColor: "#CC0000",
                labelFontSize: 11,
                linkToObject: "vilnius"
            }]
        },

            {
                id: "vilnius",
                color: "#000000",
                svgPath: targetLighthouse,
                title: "Whitby Lighthouse, North Yorkshire, England",
                latitude: 54.49283740924501,
                longitude: -0.612874381357642,
                scale: 0.4,
                zoomLevel: 1.92,
                zoomLongitude: 15.4492,
                zoomLatitude: 50.2631,

                /* lines: [{
                   latitudes: [54.6896, 50.8371],
                   longitudes: [25.2799, 4.3676],
                   color: "#00cc00"
                 }, {
                   latitudes: [54.6896, 59.9138],
                   longitudes: [25.2799, 10.7387],
                   color: "#00cc00"
                 }, {
                   latitudes: [54.6896, 40.4167],
                   longitudes: [25.2799, -3.7033],
                   color: "#00cc00"
                 }, {
                   latitudes: [54.6896, 50.0878],
                   longitudes: [25.2799, 14.4205],
                   color: "#00cc00"
                 }, {
                   latitudes: [54.6896, 48.2116],
                   longitudes: [25.2799, 17.1547],
                   color: "#00cc00"
                 }, {
                   latitudes: [54.6896, 44.8048],
                   longitudes: [25.2799, 20.4781],
                   color: "#00cc00"
                 }, {
                   latitudes: [54.6896, 55.7558],
                   longitudes: [25.2799, 37.6176],
                   color: "#00cc00"
                 }, {
                   latitudes: [54.6896, 37.9792],
                   longitudes: [25.2799, 23.7166],
                   color: "#00cc00"
                 }, {
                   latitudes: [54.6896, 54.6896],
                   longitudes: [25.2799, 25.2799],
                   color: "#00cc00"
                 }, {
                   latitudes: [54.6896, 51.5002],
                   longitudes: [25.2799, -0.1262],
                   color: "#00cc00"
                 }, {
                   latitudes: [54.6896, 53.3441],
                   longitudes: [25.2799, -6.2675],
                   color: "#00cc00"
                 }],

                 images: [{
                   label: "Flights from Vilnius",
                   svgPath: planeSVG,
                   left: 100,
                   top: 45,
                   labelShiftY: 5,
                   color: "#CC0000",
                   labelColor: "#CC0000",
                   labelRollOverColor: "#CC0000",
                   labelFontSize: 20
                 }, {
                   label: "show flights from London",
                   left: 106,
                   top: 70,
                   labelColor: "#000000",
                   labelRollOverColor: "#CC0000",
                   labelFontSize: 11,
                   linkToObject: "london"
                 }]*/
            },




            {
                type: "circle",
                svgPath: targetSVG,
                title: "Sea Test Node 1",
                rollOverColor: "#089282",
                rollOverScale: 3,
                zoomLevel: 5,
                latitude: 56.89398954749854,
                longitude: 2.9543556495366374
            }, {
                svgPath: targetSVG,
                rollOverColor: "#089282",
                rollOverScale: 3,
                zoomLevel: 5,
                scale: 0.5,
                allowClickOnSelectedObject: true,
                title: "Sea Test Node 1",
                latitude: 56.89398954749854,
                longitude: 2.9543556495366374
            },





            /*, {
              svgPath: targetSVG,
              title: "Brussels",
              latitude: 50.8371,
              longitude: 4.3676
            }, {
              svgPath: targetSVG,
              title: "Prague",
              latitude: 50.0878,
              longitude: 14.4205
            }, {
              svgPath: targetSVG,
              title: "Athens",
              latitude: 37.9792,
              longitude: 23.7166
            }, {
              svgPath: targetSVG,
              title: "Reykjavik",
              latitude: 64.1353,
              longitude: -21.8952
            }, {
              svgPath: targetSVG,
              title: "Dublin",
              latitude: 53.3441,
              longitude: -6.2675
            }, {
              svgPath: targetSVG,
              title: "Oslo",
              latitude: 59.9138,
              longitude: 10.7387
            }, {
              svgPath: targetSVG,
              title: "Lisbon",
              latitude: 38.7072,
              longitude: -9.1355
            }, {
              svgPath: targetSVG,
              title: "Moscow",
              latitude: 55.7558,
              longitude: 37.6176
            }, {
              svgPath: targetSVG,
              title: "Belgrade",
              latitude: 44.8048,
              longitude: 20.4781
            }, {
              svgPath: targetSVG,
              title: "Bratislava",
              latitude: 48.2116,
              longitude: 17.1547
            }, {
              svgPath: targetSVG,
              title: "Ljubljana",
              latitude: 46.0514,
              longitude: 14.5060
            }, {
              svgPath: targetSVG,
              title: "Madrid",
              latitude: 40.4167,
              longitude: -3.7033
            }, {
              svgPath: targetSVG,
              title: "Stockholm",
              latitude: 59.3328,
              longitude: 18.0645
            }, {
              svgPath: targetSVG,
              title: "Bern",
              latitude: 46.9480,
              longitude: 7.4481
            }, {
              svgPath: targetSVG,
              title: "Kiev",
              latitude: 50.4422,
              longitude: 30.5367
            }, {
              svgPath: targetSVG,
              title: "Paris",
              latitude: 48.8567,
              longitude: 2.3510
            }, {
              svgPath: targetSVG,
              title: "New York",
              latitude: 40.43,
              longitude: -74
            }*/
        ]
    },

    areasSettings: {
        unlistedAreasColor: "#FFCC00"
    },

    imagesSettings: {
        color: "#CC0000",
        rollOverColor: "#CC0000",
        selectedColor: "#CC0000"
    },

    linesSettings: {
        rollOverColor: "#089282",
        rollOverScale: 3,
        color: "#CC0000",
        alpha: 0.4,
        thickness: 5,
        disableMouseEvents: false,
        balloonText: "TRANSMISSION <br><br>  Phant Value: <div id=\"sensor_1\" style=\"width: 100%;\"></div> <div id=\"elapsed_time\" style=\"width: 100%;\"></div><br><br>"


    },
    "listeners": [{
        "event": "clickMapObject",
        "method": function(event) {
            if (event.mapObject.svgPath !== undefined) {

            }
        }
    }],

    backgroundZoomsToTop: true,
    linesAboveImages: true
});




