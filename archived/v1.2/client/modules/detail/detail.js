/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App')
        .controller('DetailController', DetailController);

    // For global map page
    function DetailController($http, $rootScope, $state) {
        var vm = this;

        if (!$state.params || !$state.params.id) {
            $sta
            te.go('main');
        } else {
            console.log('detail');
            $http.get($rootScope.clientApiUrl('marker/' + $state.params.id)).then(function (result) {
                vm.marker = result.data;
            }, function (err) {
                console.log(err);
                localStorage.removeItem('token');
                $state.go('login');
            });
        }
    }
})();