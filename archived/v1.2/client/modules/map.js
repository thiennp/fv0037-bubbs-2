/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    var colors = ['rgba(139, 195, 74, 0.5)', 'rgba(220, 20, 20, 0.5)', 'rgba(255, 235, 59, 0.5)', '#46bfbd', '#fdb45c', '#949fb1', '#4d5360'];
    var zoom = 6;
    var uluru = { lng: 12.362724, lat: 60.670670 };
    var bounds = new google.maps.LatLngBounds();

    angular
        .module('App')
        .config(chartConfig)
        .controller('MapController', MapController);

    function chartConfig(ChartJsProvider) {
        ChartJsProvider.setOptions({ colors: colors });
    }

    // For global map page
    function MapController($http, $rootScope, $state, $uibModal) {
        var map, vm = this;

        function createRouteNames(list) {
            var routeNames = [];
            angular.forEach(list, function (item) {
                var routeName = item.routeName;
                if (routeName && routeNames.indexOf(routeName) === -1) {
                    routeNames.push(routeName);
                }
            });
            $rootScope.routeNames = routeNames;
            $rootScope.$broadcast('update route names', routeNames);
        }

        function markerClick(evt) {
            var lat = evt.latLng.lat();
            var lng = evt.latLng.lng();
            var foundMarker = _.find(vm.result, function (item) {
                return Math.floor(item.lat * 1000000) === Math.floor(lat * 1000000) && Math.floor(item.lng * 1000000) === Math.floor(lng * 1000000);
            });
            if (foundMarker) {
                $state.go('detail', {
                    'id': foundMarker._id
                });
            }
        }

        function isMatched(item) {
            var statuses = [
                '',
                'good',
                'bad',
                'ugly',
                'planned',
                'route'
            ];
            if (item.routeName) {
                var routeIndex = $rootScope.routeNames.indexOf(item.routeName);
                if (!vm.routeFilters[routeIndex]) {
                    return false;
                }
            } else {
                return true;
            }
            if (!item.markerId) {
                return false;
            }
            if (item.markerId.toLowerCase().indexOf(vm.searchText.toLowerCase()) === -1) {
                return false;
            }
            if (!vm.filters[statuses[item.status]]) {
                return false;
            }
            return true;
        }

        function onFilter() {
            var searchBounds = new google.maps.LatLngBounds();
            var count = 0;
            var markerCenter;
            if (!vm.result.message) {
                angular.forEach(vm.result, function (item, index) {
                    if (item.status === undefined) {
                        item.status = 1;
                    }

                    if (!vm.searchText) vm.searchText = '';
                    if (isMatched(item)) {
                        count++;
                        markerCenter = {
                            lat: vm.result[index].lat,
                            lng: vm.result[index].lng
                        };
                        searchBounds.extend(markerCenter);
                        vm.showing[index] = true;
                        if (vm.markers[index]) {
                            vm.markers[index].setMap(map);
                        }
                    } else {
                        vm.showing[index] = false;
                        if (vm.markers[index]) {
                            vm.markers[index].setMap(null);
                        }
                    }

                    if (count > 1) {
                        map.fitBounds(searchBounds);
                    } else if (count === 1) {
                        map.setCenter(markerCenter);
                        map.setZoom(zoom);
                    }
                });

                updateCluster();
                updateRoute();
            }
        }

        function updateCluster() {
            if (map) {
                // var markerCluster = new MarkerClusterer(map, vm.markers, { imagePath: './image/planned' });
            }
        }

        function updateRoute() {
            angular.forEach(vm.polies, function (item) {
                if (item) {
                    item.setMap(null);
                }
            });

            vm.routeLatLngArr = [];
            vm.polies = [];

            angular.forEach(vm.result, function (item, index) {
                if (vm.showing[index] && item.routeName) {
                    var index = _.findIndex($rootScope.routeNames, function (routeItem) {
                        return routeItem === item.routeName;
                    });
                    if (index > -1) {
                        if (!vm.routeLatLngArr[index]) {
                            vm.routeLatLngArr[index] = [];
                        }
                        vm.routeLatLngArr[index].push(new google.maps.LatLng(item.lat, item.lng));
                    }
                }
            });


            //***********ROUTING****************//

            //Initialize the Direction Service

            //Loop and Draw Path Route between the Points on MAP
            for (var j in vm.routeLatLngArr) {
                if (vm.routeLatLngArr[j] && vm.routeLatLngArr[j].length > 1) {
                    var service = new google.maps.DirectionsService();
                    var arr = vm.routeLatLngArr[j];

                    for (var i = 0; i < arr.length; i++) {
                        if ((i + 1) < arr.length) {
                            var src = arr[i];
                            var des = arr[i + 1];
                            service.route({
                                origin: src,
                                destination: des,
                                travelMode: google.maps.DirectionsTravelMode.DRIVING
                            }, function (result, status) {
                                if (status == google.maps.DirectionsStatus.OK) {
                                    //Initialize the Path Array
                                    var path = new google.maps.MVCArray();

                                    //Set the Path Stroke Color
                                    var poly = new google.maps.Polyline({
                                        map: map,
                                        strokeColor: '#29292900'
                                    });
                                    poly.setPath(path);
                                    vm.polies.push(poly);

                                    for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                                        path.push(result.routes[0].overview_path[i]);
                                    }
                                }
                            });
                        }
                    }
                }
            }
        }

        vm.removeMarkers = function () {
            if (vm.markers) {
                for (var index = 0; index < vm.markers.length; index++) {
                    vm.markers[index].setMap(null);
                    vm.showing[index] = false;
                }
            }
        }

        vm.showMap = function () {
            map = new google.maps.Map(document.getElementById('map'), {
                scaleControl: true,
                zoom: zoom,
                center: uluru,
            });

            vm.resetMapData();

            map.addListener('click', function (evt) {
                var lat = evt.latLng.lat();
                var lng = evt.latLng.lng();
                vm.datalist.unshift({
                    lat: lat,
                    lng: lng
                });

                var modalInstance = $uibModal.open({
                    templateUrl: './modules/marker-confirm/marker-confirm.html',
                    controller: 'MarkerConfirmController',
                    controllerAs: 'vm',
                    resolve: {
                        data: {
                            marker: vm.datalist[0],
                            list: vm.result,
                        }
                    }
                });

                modalInstance.result.then(function (data) {
                    $http.post($rootScope.clientApiUrl('marker'), data).then(function (result) {

                        var img = './image/';
                        switch (Number(data.status)) {
                            case 1:
                                img += 'good.png';
                                break;
                            case 2:
                                img += 'bad.png';
                                break;
                            case 3:
                                img += 'ugly.png';
                                break;
                            case 4:
                                img += 'planned.png';
                                break;
                            case 5:
                                img += 'route_' + data.route + '.png';
                                break;
                        }

                        // add marker to map
                        var marker = new google.maps.Marker({
                            position: {
                                lat: evt.latLng.lat(),
                                lng: evt.latLng.lng()
                            },
                            map: map,
                            icon: img
                        });

                        var infowindow = new google.maps.InfoWindow({
                            content: `${data.markerId || 'No marker id'}<br>${localStorage.getItem('user_username')}`
                        });

                        marker.addListener('mouseover', function () {
                            infowindow.open(map, marker);
                        });

                        marker.addListener('mouseout', function () {
                            infowindow.close(map, marker);
                        });

                        marker.addListener('click', markerClick);
                        vm.markers.push(marker);
                        map.setCenter(marker.position);
                        $state.go('detail', {
                            'id': result.data._id
                        });

                        vm.result.push(result.data);
                        $rootScope.$broadcast('add route name', result.data);
                        onFilter();
                    });
                }, function (err) {
                    console.log(err);
                });
            });

            $rootScope.$on('map refresh', function (evt, value) {
                vm.resetMapData();
            });

            $rootScope.$on('filter change', function (evt, value) {
                vm.searchText = value;
                onFilter();
            });


            $rootScope.$on('filter markers', function (evt, filters) {
                console.log(filters);
                vm.filters = filters;
                onFilter();
            });


            $rootScope.$on('filter routes', function (evt, routeFilters) {
                vm.routeFilters = routeFilters;
                onFilter();
            });

            vm.chart = {};
            vm.chart.labels = ['Good', 'Bad', 'Ugly', 'Planned'];
            vm.chart.data = [0, 0, 0, 0];
            vm.chart.Options = {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            };

            vm.chart.routeLabels = ['Route 1', 'Route 2', 'Route 3'];
            vm.chart.routeData = [0, 0, 0];
            vm.chart.routeOptions = {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            };

            vm.chart.total = 0;
            vm.chart.colors = colors;

            console.log('map');
            $http.get($rootScope.clientApiUrl('all')).then(function (result) {
                vm.chart.list = result.data;
                angular.forEach(result.data, function (item) {
                    if (item.status) {
                        vm.chart.data[item.status - 1]++;
                        vm.chart.total++;

                        if (item.status === 5) {
                            vm.chart.routeData[item.route - 1]++;
                        }
                    }
                });
                createRouteNames(result.data);
            }, function () {
                localStorage.removeItem('token');
                $state.go('login');
            });
        };

        vm.resetMapData = function () {
            vm.result = [];
            vm.removeMarkers();
            console.log('map');
            $http.get($rootScope.clientApiUrl('all')).then(function (result) {
                if (result.data) {
                    vm.result = result.data;
                    createRouteNames(result.data);
                    angular.forEach(result.data, function (item, index) {
                        if (item.lat && item.lng) {
                            var newLatLng = {
                                lat: item.lat,
                                lng: item.lng
                            };
                            vm.datalist.unshift(newLatLng);
                            bounds.extend(newLatLng);

                            var img = './image/';
                            switch (Number(item.status)) {
                                case 1:
                                    img += 'good.png';
                                    break;
                                case 2:
                                    img += 'bad.png';
                                    break;
                                case 3:
                                    img += 'ugly.png';
                                    break;
                                case 4:
                                    img += 'planned.png';
                                    break;
                                case 5:
                                    img += 'route_' + item.route + '.png';
                                    break;
                            }

                            var marker = new google.maps.Marker({
                                position: item,
                                map: map,
                                icon: img
                            });

                            var infowindow = new google.maps.InfoWindow({


                                content: '<span style="color:#157ad6; "><h3>Marker ID: ' + item.markerId + '</h3><br></span>' +  '<span style="color:#157ad6; "><pre id="infobox">Creation Date: ' + item.create.slice(0, -14) + '</span><br>' + '<span>Creation Time: ' + item.create.slice(11, -5) + '</pre></span><br>'  + '<span style="color:#157ad6; "><pre id="infobox">Username: ' + item.user.username + '</span><br><span style="color:#157ad6; "><pre id="infobox">Route: ' + item.routeName + '</span><br>' || 'No Marker ID'

                                // content: `${item.markerId || 'No marker id'}<br>${item.route ? 'ROUTE ' + $rootScope.routeNames[item.route] + '<br>' : ''}${item.user.username || 'Username is not set for the user who created this marker'}`
                            });

                            marker.addListener('mouseover', function () {
                                infowindow.open(map, marker);
                            });

                            marker.addListener('mouseout', function () {
                                infowindow.close(map, marker);
                            });
 
                            marker.addListener('click', markerClick);

                            vm.markers[index] = marker;
                        }
                    });

                    if (result.data.length > 1) {
                        map.fitBounds(bounds);
                    } else if (result.data.length === 1) {
                        map.setCenter({
                            lat: result.data[0].lat,
                            lng: result.data[0].lng
                        });
                        map.setZoom(zoom);
                    }

                    onFilter();
                }
            }, function () {
                localStorage.removeItem('token');
                $state.go('login');
            });
        };

        function init() {
            var src = 'https://developers.google.com/maps/documentation/javascript/examples/kml/westcampus.kml';

            vm.datalist = [];
            vm.showing = [];
            vm.result = [];
            vm.markers = [];
            vm.routeFilters = ['', true, true, true];
            vm.filters = {
                good: true,
                bad: true,
                ugly: true,
                planned: true,
                route: true
            };

            $rootScope.$on('auth', function () {
                $rootScope.$broadcast('map refresh');
            });

            vm.routeNames = $rootScope.routeNames;

            $rootScope.$on('update route names', function (evt, routeNames) {
                vm.routeNames = routeNames;
                onFilter();
            });
        }

        init();
    }
})();
