const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const md5 = require('md5');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const nodemailer = require('nodemailer');

app.use(bodyParser.json({
    limit: '5mb'
}));
app.use(bodyParser.urlencoded({
    limit: '5mb'
}));


app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/client'));
app.use(express.static(__dirname + '/upload'));

mongoose.connect('mongodb://localhost/fv0037');

var Schema = mongoose.Schema;

var AppModel = new Schema({
    name: {
        unique: true,
        type: String
    },
    create: Date,
    userId: String,
});

var MarkerModel = new Schema({
    lat: Number,
    lng: Number,
    description: String,
    markerId: String,
    status: Number,
    routeName: String,
    route: Number,
    image: String,
    create: Date,
    edit: Date,
    history: [{
        description: String,
        status: Number,
        routeName: String,
        route: Number,
        image: String,
        create: Date,
        edit: Date
    }],
    userId: String,
    appId: String,
});

var UserModel = new Schema({
    email: {
        unique: true,
        type: String
    },
    username: {
        unique: true,
        type: String
    },
    password: String,
    company: String,
    create: Date,
    edit: Date,
    image: String,
    name: String,
    bio: String,
    role: {
        type: Number,
        default: 1, // 0: Admin, 1: User
    },
    appId: String,
    verificationCode: String,
    passwordCode: String,
});


var App = mongoose.model('AppModel', AppModel);
var Marker = mongoose.model('MarkerModel', MarkerModel);
var User = mongoose.model('UserModel', UserModel);

function applyUser(marker, user) {
    log('applyUser', marker, user);
    return {
        _id: marker._id,
        lat: marker.lat,
        lng: marker.lng,
        description: marker.description,
        markerId: marker.markerId,
        status: marker.status,
        route: marker.route,
        routeName: marker.routeName,
        image: marker.image,
        create: marker.create,
        edit: marker.edit,
        history: marker.history,
        userId: marker.userId,
        appId: marker.appId,
        user: user ? {
            name: user.name,
            username: user.username,
            company: user.company,
            image: user.image,
            email: user.email
        } : {}
    };
}

function applyAllUsers(res, markers, users) {
    var sendMarkers = [];
    var usersById = {};
    var i;
    for (i = 0; i < users.length; i++) {
        usersById[users[i]._id] = users[i];
    }
    for (i = 0; i < markers.length; i++) {
        sendMarkers[i] = applyUser(markers[i], usersById[markers[i].userId]);
    }
    log('sendMarkers', sendMarkers);
    res.send(sendMarkers);
}

function getUser(id, res, callback) {
    User.findOne({
        _id: id
    }, function (err, user) {
        if (!err) {
            callback(user);
        } else {
            res.send(err);
        }
    });
}

function log(title, text) {
    console.log('');
    console.log('');
    console.log('');
    console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
    console.log(title);
    if (text) {
        console.log(text);
    }
    console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
    console.log('');
    console.log('');
}

function sendMail(email, text) {
    // TODO: Update email account here
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'shoresafety111@gmail.com',
            pass: 'shoresafety111ForTesting', // Update your email account here
        }
    });

    var mailOptions = {
        from: 'test@gmail.com',
        to: email,
        subject: 'Registration',
        text: text,
    };

    log(`To ${email}:`, text);

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}

function verifyFirstUser(res, callback, errMsg) {
    User.find({})
        .exec((err, users) => {
            if (users.length === 1) {
                users[0].role = 0;
                users[0].save((err, result) => {
                    if (err) {
                        res.send(401, {
                            success: false,
                            message: 'error saving first user',
                        });
                    } else {
                        callback();
                    }
                });
            } else {
                res.send(401, {
                    success: false,
                    message: errMsg,
                });
                return;
            }
        });
}

function findMarkers(condition, res) {
    Marker.find(condition)
        .sort('_id')
        .exec(function (err, markers) {
            log('markers', markers, err);
            if (!err) {
                User.find({}, function (err2, users) {
                    if (!err2) {
                        log('users', users, err);
                        applyAllUsers(res, markers, users);
                    } else {
                        res.send(err2);
                    }
                });
            } else {
                res.send(err);
            }
        });
}

function findOneMarker(condition) {
    Marker.findOne(condition, function (err, marker) {
        if (!err) {
            if (marker) {
                User.findOne({
                    _id: marker.userId
                }, function (err, user) {
                    if (!err) {
                        const returnMarker = applyUser(marker, user);
                        res.send(returnMarker);
                    } else {
                        renpms.send(err);
                    }
                });
            }
        } else {
            res.send(err);
        }
    });
}

var apiRoutes = express.Router();

// route to authenticate a user (POST http://localhost:8080/api/authenticate)

// route middleware to verify a token
apiRoutes.use(function (req, res, next) {

    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    // decode token
    if (token) {

        // verifies secret and checks exp
        jwt.verify(token, 'secret', function (err, decoded) {
            if (err) {
                return res.send(401, {
                    success: false,
                    message: 'Failed to authenticate token.'
                });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });

    } else {

        // if there is no token
        // return an error
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });

    }
});

app.use('/api', apiRoutes);

app.get('/', function (req, res) {
    res.sendFile('client/index.html', {
        root: __dirname
    });
});

app.get('/api/:appId/all', function (req, res) {
    const userId = req.headers['x-access-id'];
    let condition;
    getUser(userId, res, function (user) {
        log('appId', req.params.appId);
        if (req.params.appId !== 'master') {
            condition = {
                appId: {
                    $eq: req.params.appId,
                },
            };
            if (user.role !== 0) {
                condition.userId = {
                    $eq: userId
                };
            }
            findMarkers(condition, res);
        } else if (user.role === 0) {
            findMarkers({}, res);
        } else {
            verifyFirstUser(res, () => findMarkers({}, res), 'No markers found');
        }
    });
});

app.get('/api/:appId/marker', function (req, res) {
    const userId = req.headers['x-access-id'];
    let condition;
    getUser(userId, res, function (user) {
        if (req.params.appId !== 'master') {
            condition = {
                status: {
                    $lte: 4
                },
                appId: req.params.appId,
            };
            if (user.role !== 0) {
                condition.userId = {
                    $eq: userId
                };
            }
            findMarkers(condition, res);
        } else if (user.role === 0) {
            condition = {
                status: {
                    $lte: 4
                },
            };
            findMarkers(condition, res);
        } else {
            verifyFirstUser(res, () => findMarkers({}, res), 'No markers found');
        }
    });
});

app.get('/api/:appId/route', function (req, res) {
    const userId = req.headers['x-access-id'];
    let condition;

    getUser(userId, res, function (user) {
        if (req.params.appId !== 'master') {
            condition = {
                status: {
                    $gt: 4
                },
                appId: req.params.appId,
            };
            if (user.role !== 0) {
                condition.userId = {
                    $eq: userId
                };
            }
            findMarkers(condition, res);
        } else if (user.role === 0) {
            condition = {
                status: {
                    $gt: 4
                },
            };
            findMarkers(condition, res);
        } else {
            verifyFirstUser(res, () => findMarkers({}, res), 'No routes found');
        }
    });
});

app.get('/api/:appId/marker/:id', function (req, res) {
    const userId = req.headers['x-access-id'];
    let condition;
    getUser(userId, res, function (user) {
        if (req.params.appId !== 'master') {
            condition = {
                _id: req.params.id,
                appId: req.params.appId,
            };
            if (user.role !== 0) {
                condition.userId = {
                    $eq: userId
                };
            }
            findOneMarker(condition);
        } else if (user.role === 0) {
            condition = {
                _id: req.params.id,
            };
            findOneMarker(condition);
        } else {
            verifyFirstUser(res, () => findOneMarker(condition), 'No marker found');
        }
    });
});

app.post('/api/:appId/marker', function (req, res) {
    const userId = req.headers['x-access-id'];
    if (req.body.image) {
        var base64Data = req.body.image.replace(/^data:image\/png;base64,/, '');
        var filename = 'upload_' + Math.floor(Math.random() * 1000000000) + '.png';
        require('fs').writeFile('upload/' + filename, base64Data, 'base64', function (err) {
            console.log(err);
        });
        req.body.image = filename;
    }
    req.body.create = new Date();
    req.body.userId = userId;
    req.body.appId = req.params.appId;
    if (req.body.history) {
        req.body.history.forEach(function (item) {
            if (item.uploadImage) {
                var base64Data = item.uploadImage.replace(/^data:image\/png;base64,/, '');
                var filename = 'upload_' + Math.floor(Math.random() * 1000000000) + '.png';
                require('fs').writeFile('upload/' + filename, base64Data, 'base64', function (err) {
                    console.log(err);
                });
                item.image = filename;
            }
            if (!item._id) {
                item.create = new Date();
            } else {
                item.edit = new Date();
            }
        });
    }

    var marker = new Marker(req.body);
    marker.save(function (err, result) {
        if (!err) {
            res.send(result);
        } else {
            res.send(err);
        }
    });
});

app.put('/api/:appId/marker/:id', function (req, res) {
    const userId = req.headers['x-access-id'];
    if (req.body.uploadImage) {
        var base64Data = req.body.uploadImage.replace(/^data:image\/png;base64,/, '');
        var filename = 'upload_' + Math.floor(Math.random() * 1000000000) + '.png';
        require('fs').writeFile('upload/' + filename, base64Data, 'base64', function (err) {
            console.log(err);
        });
        req.body.image = filename;
    }
    req.body.update = new Date();
    req.body.userId = userId;
    req.body.history.forEach(function (item) {
        if (item.uploadImage) {
            var base64Data = item.uploadImage.replace(/^data:image\/png;base64,/, '');
            var filename = 'upload_' + Math.floor(Math.random() * 1000000000) + '.png';
            require('fs').writeFile('upload/' + filename, base64Data, 'base64', function (err) {
                console.log(err);
            });
            item.image = filename;
        }
        if (!item._id) {
            item.create = new Date();
        } else {
            item.edit = new Date();
        }
    });

    let condition;
    getUser(userId, res, function (user) {
        if (req.params.appId !== 'master') {
            condition = {
                _id: req.params.id,
                appId: req.params.appId,
            };
        } else if (user.role === 0) {
            condition = {
                _id: req.params.id,
            };
        } else {
            res.send(401, {
                success: false,
                message: 'No marker found'
            });
        }
        Marker
            .update(condition, {
                $set: req.body
            }, function (err, result) {
                if (!err) {
                    res.send(result);
                } else {
                    res.send(err);
                }
            });
    });
});

app.delete('/api/:appId/marker/:id', function (req, res) {
    const userId = req.headers['x-access-id'];
    let condition;
    getUser(userId, res, function (user) {
        if (req.params.appId !== 'master') {
            condition = {
                _id: req.query.id,
                appId: req.params.appId,
            };
        } else if (user.role === 0) {
            condition = {
                _id: req.params.id,
            };
        } else {
            res.send(401, {
                success: false,
                message: 'No marker found'
            });
        }

        var Marker = mongoose.model('MarkerModel', MarkerModel);
        Marker.remove(condition,
            function (err) {
                if (!err) {
                    res.send(req.body);
                } else {
                    res.send(err);
                }
            });
    });
});

app.post('/auth/:appId/register', function (req, res) {
    var User = mongoose.model('UserModel', UserModel);
    User.findOne({
        email: req.body.email,
        appId: req.params.appId,
    }, function (err, user) {
        if (err) throw err;
        if (!user) {
            User.findOne({
                username: req.body.username,
            }, function (err, user) {
                if (!user) {
                    req.body.create = new Date();
                    req.body.password = md5(req.body.password);
                    req.body.appId = req.params.appId;
                    req.body.company = req.body.company;
                    req.body.verificationCode = md5(Math.random());
                    var user = new User(req.body);
                    log('Registering', user);
                    user.save(function (err, result) {
                        if (!err) {
                            log('Registered', result);
                            const mailContent = 'Click link to Activate account\n';
                            const mailContent2 = req.headers.host + '/#!/verify/' + req.body.verificationCode;
                            sendMail(user.email, mailContent + mailContent2)
                            res.send(result);
                        } else {
                            res.send(err);
                        }
                    });
                } else {
                    res.send(401, {
                        success: false,
                        message: 'Username is already exist'
                    });
                }
            });
        } else {
            res.send(401, {
                success: false,
                message: 'Email is already exist'
            });
        }
    });
});

app.post('/auth/password', function (req, res) {
    var User = mongoose.model('UserModel', UserModel);
    log('Password code', req.body.passwordCode);
    User.findOne({
        passwordCode: req.body.passwordCode,
    }, function (err, user) {
        if (err) throw err;
        if (user) {
            log('user', user);
            user.password = md5(req.body.password);
            user.save(function (err, result) {
                if (!err) {
                    log(`Password updated to: ${user.email}`);
                    res.send(result);
                } else {
                    res.send(err);
                }
            });
        } else {
            res.send(401, {
                success: false,
                message: 'passwordCode us not correct'
            });
        }
    });
});

app.post('/auth/forgot', function (req, res) {
    var User = mongoose.model('UserModel', UserModel);
    User.findOne({
        email: req.body.email,
    }, function (err, user) {
        if (err) throw err;
        if (user) {
            user.passwordCode = md5(Math.random());
            user.save(function (err, result) {
                if (!err) {
                    const mailContent = req.headers.host + '/#!/password/' + user.passwordCode;
                    sendMail(user.email, mailContent)
                    res.send(result);
                } else {
                    res.send(err);
                }
            });
        } else {
            res.send(401, {
                success: false,
                message: 'Email doens\'t exist'
            });
        }
    });
});

app.get('/auth/verification/:verificationCode', function (req, res) {
    var User = mongoose.model('UserModel', UserModel);
    User.findOne({
        verificationCode: req.params.verificationCode,
    }, function (err, user) {
        if (err) throw err;
        if (user) {
            user.verificationCode = null;
            user.save(function (err, result) {
                if (!err) {
                    res.send(result);
                } else {
                    res.send(err);
                }
            });
        } else {
            res.send(401, {
                success: false,
                message: 'Can not find user'
            });
        }
    });
});

app.post('/auth/login', function (req, res) {
    User.findOne({
        username: req.body.username,
        password: md5(req.body.password),
    }, function (err, user) {
        if (err) throw err;

        if (!user) {
            res.send(401, {
                success: false,
                message: 'Authentication failed. Please check your email or password.'
            });
        } else if (user) {
            const payload = {};

            var token = jwt.sign({}, 'secret');

            // return the information including token as JSON
            res.send({
                success: true,
                token: token,
                id: user._id,
                email: user.email,
                name: user.name,
                image: user.image,
                username: user.username,
                appId: user.appId,
                role: user.role,
            });
        }
    });
});

app.get('/api/:appId/profile', function (req, res) {
    const userId = req.headers['x-access-id'];
    User.findOne({
        _id: userId,
        appId: req.params.appId,
    }, {
        'email': true,
        'image': true,
        'username': true,
        'name': true,
        'bio': true
    }, function (err, user) {
        if (!err) {
            res.send(user);
        } else {
            res.send(err);
        }
    });
});

app.put('/api/:appId/profile', function (req, res) {
    const userId = req.headers['x-access-id'];
    if (req.body.uploadImage) {
        var base64Data = req.body.uploadImage.replace(/^data:image\/png;base64,/, '');
        var filename = 'upload_' + Math.floor(Math.random() * 1000000000) + '.png';
        require('fs').writeFile('upload/' + filename, base64Data, 'base64', function (err) {
            console.log(err);
        });
        req.body.image = filename;
        delete req.body.uploadImage;
    }
    req.body.edit = new Date();

    User
        .update({
            _id: userId
        }, {
            $set: req.body
        }, function (err, result) {
            if (!err) {
                User.findOne({
                    _id: userId
                }, {
                    'email': true,
                    'image': true,
                    'name': true,
                    'bio': true
                }, function (err, user) {
                    if (!err) {
                        res.send(user);
                    } else {
                        res.send(err);
                    }
                });
            } else {
                res.send(err);
            }
        });
});

app.get('/auth/app', function (req, res) {
    App.find({}, function (err, list) {
        if (!err) {
            res.send(list);
        } else {
            res.send(err);
        }
    });
});

app.get('/api/app/:id', function (req, res) {
    App.findOne({
        _id: req.params.id
    }, function (err, item) {
        if (!err) {
            res.send(item);
        } else {
            res.send(err);
        }
    });
});

app.delete('/api/app/:id', function (req, res) {
    const condition = {
        appId: req.params.id,
    };
    Marker.remove(condition, function (err1) {
        if (!err1) {
            User.remove(condition, function (err2) {
                if (!err2) {
                    App.remove({
                        _id: req.params.id
                    }, function (err3, item) {
                        if (!err3) {
                            res.send(item);
                        } else {
                            res.send(err3);
                        }
                    });
                } else {
                    res.send(err2);
                }
            });
        } else {
            res.send(err1);
        }
    });
});

app.post('/api/app', function (req, res) {
    const userId = req.headers['x-access-id'];
    User.findOne({
        _id: userId,
    }, function (err, user) {
        if (user && user.role === 0) {
            App.findOne({
                name: req.params.name,
            }, function (err, found) {
                if (err) {
                    res.send(err);
                    return;
                }
                if (found) {
                    res.send(409, {
                        success: false,
                        message: 'App name is taken'
                    });
                    return;
                }
                req.body.create = new Date();
                req.body.userId = userId;

                var item = new App(req.body);
                item.save(function (err, result) {
                    if (!err) {
                        res.send(result);
                    } else {
                        res.send(err);
                    }
                });
            });
        }
    });
});

app.put('/api/app/:id', function (req, res) {
    const userId = req.headers['x-access-id'];
    User.findOne({
        _id: userId,
    }, function (err, user) {
        if (user && user.role === 0) {
            App.findOne({
                name: req.params.name,
            }, function (err, found) {
                if (err) {
                    res.send(err);
                    return;
                }
                if (found && found._id !== req.params.id) {
                    res.send(409, {
                        success: false,
                        message: 'App name is taken'
                    });
                    return;
                }
                req.body.update = new Date();
                req.body.userId = userId;

                App.update({
                    _id: req.params.id
                }, {
                    $set: req.body
                }, function (err, result) {
                    if (!err) {
                        res.send(result);
                    } else {
                        res.send(err);
                    }
                });
            });
        }
    });
});

app.listen(3001, function () {
    console.log('Example app listening on port 3001!')
});