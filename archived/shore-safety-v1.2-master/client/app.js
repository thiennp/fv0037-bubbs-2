/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 * Global Modules
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App', [
            'ngSanitize',
            'ngRoute',
            'ui.router',
            'ui.bootstrap',
            'chart.js',
            'angularMoment'
        ])
        .config(routerConfig)
        .run(run);

    /** @ngInject */
    function routerConfig($httpProvider, $stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app', {
                url: '/:appId',
                templateUrl: 'modules/app/app.html'
            })
            .state('login', {
                url: '/login',
                templateUrl: 'modules/auth/auth.html',
                controller: 'AuthController',
                controllerAs: 'vm',
            })
            .state('app.register', {
                url: '/register',
                templateUrl: 'modules/auth/auth.html',
                controller: 'AuthController',
                controllerAs: 'vm',
            })
            .state('select', {
                url: '/select/:next',
                templateUrl: 'modules/select/select.html',
                controller: 'SelectController',
                controllerAs: '$ctrl',
            })
            .state('admin', {
                url: '/admin',
                templateUrl: 'modules/admin/admin.html',
                controller: 'AdminController',
                controllerAs: '$ctrl',
            })
            .state('app.main', {
                url: '/main',
                templateUrl: 'modules/main/main.html',
                controller: 'MainController',
                controllerAs: 'mainCtrl',
            })
            .state('app.detail', {
                url: '/detail/:id',
                templateUrl: 'modules/detail/detail.html',
                controller: 'DetailController',
                controllerAs: 'detailCtrl',
            })
            .state('app.chart', {
                url: '/chart',
                templateUrl: 'modules/chart/chart.html',
                controller: 'ChartController',
                controllerAs: 'chartCtrl',
            })
            .state('app.markers', {
                url: '/markers',
                templateUrl: 'modules/markers/markers.html',
                controller: 'MarkersController',
                controllerAs: 'vm',
            })
            .state('app.reports', {
                url: '/report',
                templateUrl: 'modules/report/report.html',
                controller: 'ReportController',
                controllerAs: 'vm',
            })
            .state('app.report', {
                url: '/report/:id',
                templateUrl: 'modules/report/report.html',
                controller: 'ReportController',
                controllerAs: 'vm',
            })
            .state('app.route-markers', {
                url: '/route-markers',
                templateUrl: 'modules/route-markers/route-markers.html',
                controller: 'RouteMarkersController',
                controllerAs: 'vm',
            })
            .state('app.profile', {
                url: '/profile',
                templateUrl: 'modules/profile/profile.html',
                controller: 'ProfileController',
                controllerAs: 'vm',
            })
            .state('verify', {
                url: '/verify/:verificationCode',
                template: 'Verifying...',
                controller: 'VerificationController',
                controllerAs: 'vm',
            })
            .state('password', {
                url: '/password/:passwordCode',
                templateUrl: 'modules/password/password.html',
                controller: 'PasswordController',
                controllerAs: 'vm',
            })
            .state('new', {
                url: '/new',
                template: '<div ui-view></div>',
            })
            .state('new.main', {
                url: '/main',
                templateUrl: 'modules/new/main/main.html',
                controller: 'NewMainController',
                controllerAs: 'vm',
            });

        $urlRouterProvider.otherwise('/login');
        $httpProvider.interceptors.push(function ($q) {
            return {
                'request': function (config) {
                    config.headers['x-access-token'] = localStorage.getItem('token');
                    config.headers['x-access-id'] = localStorage.getItem('id');
                    return config;
                }
            };
        });
    }

    function run($state, $rootScope) {
        $rootScope.pageName = function (stateName) {
            return stateName.split('app.').join('');
        };

        $rootScope.clientApiUrl = function (path) {
            return window.apiUrl + $state.params.appId + '/' + path;
        };

        $rootScope.appId = function () {
            return $state.params.appId;
        };
    }
})();
