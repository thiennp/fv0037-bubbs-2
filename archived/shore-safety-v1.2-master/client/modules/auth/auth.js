/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App')
        .controller('AuthController', AuthController);

    // For global map page
    function AuthController($http, $state, $rootScope) {
        var vm = this;
        vm.isRegisterPage = false;

        localStorage.removeItem('token');
        localStorage.removeItem('id');
        if ($state.params.appId) {
            vm.isRegisterPage = true;
        }

        function isValidPassword() {
            var valid = false;
            var str = ['ABCDEFGHIJKLMNOPQRSTUVWXYZ', '0123456789'];
            for (var i = 0; i < str[0].length; i++) {
                if (vm.password.indexOf(str[0][i]) > -1) {
                    for (var j = 0; j < str[1].length; j++) {
                        if (vm.password.indexOf(str[1][j]) > -1) {
                            valid = true;
                            break;
                        }
                    }
                    break;
                }
            }
            return valid;
        }

        vm.gotoRegister = function () {
            if ($state.params.appId) {
                vm.forgotPass = false;
                vm.isRegisterPage = true;
                vm.err = undefined;
            } else {
                $state.go('select', { next: 'register' });
            }
        }

        vm.login = function () {
            delete vm.err;
            if (!vm.password) {
                vm.err = {
                    data: {
                        message: 'Password is required!'
                    }
                };
            } else if (!vm.username) {
                vm.err = {
                    data: {
                        message: 'Username is require!'
                    }
                };
            } else {
                $http.post(window.authUrl + 'login', {
                    username: vm.username,
                    password: vm.password
                }).then(function (result) {
                    if (result && result.data && result.data.success) {
                        localStorage.setItem('token', result.data.token);
                        if (!!result.data.name) {
                            localStorage.setItem('user_name', result.data.name);
                        }
                        if (!!result.data.username) {
                            localStorage.setItem('user_username', result.data.username);
                        }
                        if (!!result.data.email) {
                            localStorage.setItem('user_email', result.data.email);
                        }
                        if (!!result.data.image) {
                            localStorage.setItem('user_image', result.data.image);
                        }
                        localStorage.setItem('id', result.data.id);
                        $rootScope.$broadcast('auth');
                        if (result.data.appId) {
                            $state.go('app.main', { appId: result.data.appId });
                        } else if (result.data.role < 1) {
                            $state.go('admin');
                        } else {
                            $state.go('select', { next: 'main' });
                        }
                    }
                }, function (err) {
                    vm.err = err;
                });
            }
        };

        vm.register = function () {
            delete vm.err;
            if (!vm.password) {
                vm.err = {
                    data: {
                        message: 'Password is required!'
                    }
                };
            } else if (!vm.retypePassword) {
                vm.err = {
                    data: {
                        message: 'Please re-type password!'
                    }
                };
            } else if (!vm.email) {
                vm.err = {
                    data: {
                        message: 'Email is require!'
                    }
                };
            } else if (!vm.username) {
                vm.err = {
                    data: {
                        message: 'Username is require!'
                    }
                };
            } else if (!isValidPassword()) {
                vm.err = {
                    data: {
                        message: 'Password must contain Number and Capital letter!'
                    }
                };
            } else if (vm.password !== vm.retypePassword) {
                vm.err = {
                    data: {
                        message: 'Passwords do not matched!'
                    }
                };
            } else {
                $http.post(window.authUrl + $state.params.appId + '/register', {
                    email: vm.email,
                    username: vm.username,
                    password: vm.password,
                    company: vm.company,
                }).then(function (result) {

                    swal("Registered!", "Please check your mail!", "success");
                   // alert('activation email has been sent to' + data.email +  'please check you mail!');
                    try {
                        if (result.data._id) {
                            vm.isRegisterPage = false;
                        }
                    } catch (e) {
                        console.log(e);
                    }
                }, function (err) {
                    vm.err = err;
                });
            }
        };

        vm.sendEmail = function () {
            $http.post(window.authUrl + 'forgot', {
                email: vm.email,
            }).then(function (result) {
                swal("Email is sent!", "Please check your mail", "success");
                //alert('Email is sent, please check');
            }, function (err) {
                swal("Email doesn\'t exist!", "Please try to re-register!");
                //alert('Email doesn\'t exist');
                vm.err = err;
            });
        };
    }
})();

