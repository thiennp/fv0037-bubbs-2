/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App')
        .controller('ChartController', ChartController);

    function ChartController($http, $rootScope, $state) {
        var vm = this;

        vm.labels = ['Good', 'Bad', 'Ugly', 'Planned'];
        vm.data = [0, 0, 0, 0];

        vm.routeLabels = ['Route 1', 'Route 2', 'Route 3'];
        vm.routeData = [0, 0, 0];
        vm.total = 0;

        $http.get($rootScope.clientApiUrl('all')).then(function (result) {
            vm.list = result.data;
            angular.forEach(result.data, function (item) {
                if (item.status) {
                    vm.data[item.status - 1]++;
                    vm.total++;
                    if (item.status === 5) {
                        vm.routeData[item.route - 1]++;
                    }
                }
            });
        }, function () {
            localStorage.removeItem('token');
            $state.go('login');
        });
    }
})();
