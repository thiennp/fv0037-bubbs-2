function addChart(id, color, ec, limitless) {
    var lines = ec.init(document.getElementById(id), limitless);
    var lines_options = {
        'color': [
            color,
            color,
            color,
            color
        ],
        'series': []
    }

    $.when(
        $.getJSON('./data/charts.json', function (response) {
            for (var i in response) {
                lines_options[i] = response[i];
            }
        }),
        $.getJSON('./data/chart_data_1.json', function (response) {
            lines_options.series[0] = {
                'name': response.name,
                'type': 'line',
                'data': response.data
            }
        })
    ).then(function () {
        lines.setOption(lines_options);

        window.onresize = function () {
            setTimeout(function () {
                lines.resize();
            }, 200);
        }
    });
}
$(function () {
    // Set paths
    // ------------------------------
    require.config({
        paths: {
            echarts: 'assets/js/plugins/visualization/echarts'
        }
    });

    // Configuration
    // ------------------------------
    require(
        [
            'echarts',
            'echarts/theme/limitless',
            'echarts/chart/line'
        ],

        // Charts setup
        function (ec, limitless) {
            for (var i = 0; i < 9; i++) {
                addChart('basic_lines_' + i, '#66bb6a', ec, limitless);
                addChart('high_lines_' + i, '#ef5350', ec, limitless);
                addChart('low_lines_' + i, '#ffe155', ec, limitless);
            }
        }
    )
});
