<?php ${(require_once('SimpleAuth.php'))}->protectme(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Demo</title>
    <!--Mapbox-->
    <script src='./lib/mapbox-gl.js'></script>
    <link href='./lib/mapbox-gl.css' rel='stylesheet' />
    <script src='./lib/turf.min.js'></script>
    <script src='./lib/lodash.min.js'></script>
    <!--/Mapbox-->

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v1.3.1/mapbox-gl-geocoder.css' rel='stylesheet'
        type='text/css' />

    <link href="./assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="./assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="./assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="./assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="./assets/css/colors.css" rel="stylesheet" type="text/css">
    <link href="./assets/css/custom.css" rel="stylesheet" type="text/css">
    <link href="./app.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Modules stylesheets -->
    <link href="./header/header.css" rel="stylesheet" type="text/css">
    <!-- /modules stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="./assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="./assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="./assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="./assets/js/plugins/loaders/blockui.min.js"></script>
    <script type="text/javascript" src="./assets/js/plugins/visualization/d3/d3.min.js"></script>
    <!-- /core JS files-->

    <!-- /echarts -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>

    <!-- AngularJS files-->
    <script type="text/javascript" src="./lib/angular.min.js"></script>
    <script type="text/javascript" src="./lib/angular-route.min.js"></script>
    <script type="text/javascript" src="./lib/angular-ui-router.min.js"></script>
    <!-- /angularJS files-->

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDL-o-AZKiILyzzXST_kbxrzLP4dVbjU1M"></script>
    <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v1.3.1/mapbox-gl-geocoder.js'></script>

    <script type="text/javascript" src="./assets/js/core/app.js"></script>

    <!--app-->
    <script type="text/javascript" src="./app.js"></script>

    <!--map-->
    <script type="text/javascript" src="./map/map.js"></script>

    <!--components-->
    <script type="text/javascript" src="./home/home.js"></script>
    <script type="text/javascript" src="./main/main.js"></script>

    <!--header-->
    <script type="text/javascript" src="./header/header.js"></script>


    </head>

    <body ng-app="Map">
        <div class="fakeloader">
            <div class="fl spinner1">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
        <div ng-include="'./header/header.html'" class="main-nav"></div>
        <div class="page-container body" ng-class="{'hide-history': hideHistory}" ui-view></div>
    </body>

</html>
