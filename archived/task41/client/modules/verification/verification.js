/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App')
        .controller('VerificationController', VerificationController);

    function VerificationController($http, $rootScope, $state) {
        $http.get(window.authUrl + 'verification/' + $state.params.verificationCode)
            .then(function (result) {
                alert('Verified successfully');
                $state.go('login');
            }, function () {
                alert('Verification code is wrong');
                $state.go('login');
            });
    }
})();
