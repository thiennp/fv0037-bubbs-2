/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App')
        .controller('UploadController', UploadController);

    // For global map page
    function UploadController($uibModalInstance) {
        var vm = this;

        vm.submit = function () {
            var canvas = document.getElementById('canvas-upload');
            var dataURL = canvas.toDataURL();
            $uibModalInstance.close(dataURL);
        };

        vm.cancel = function () {
            $uibModalInstance.dismiss();
        };

        $(function () {
            var fileUpload = document.getElementById('fileUpload');
            var canvas = document.getElementById('canvas-upload');
            var ctx = canvas.getContext("2d");
            var canvasSize = 400;

            function readImage() {
                if (this.files && this.files[0]) {
                    var FR = new FileReader();
                    FR.onload = function (e) {
                        var img = new Image();
                        img.src = e.target.result;
                        img.onload = function () {
                            var w = img.naturalWidth;
                            var h = img.naturalHeight;
                            if (w > h) {
                                h = canvasSize * h / w;
                                w = canvasSize;
                            } else {
                                w = canvasSize * w / h;
                                h = canvasSize;
                            }
                            $("#canvas-upload").attr('width', w);
                            $("#canvas-upload").attr('height', h);
                            ctx.drawImage(img, 0, 0, w, h);
                        };
                    };
                    FR.readAsDataURL(this.files[0]);
                }
            }

            fileUpload.onchange = readImage;
        });
    }
})();