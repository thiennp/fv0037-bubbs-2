/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App')
        .controller('MainController', MainController);

    // For global map page
    function MainController($rootScope) {
        var vm = this;

        $rootScope.$on('auth', init);

        vm.filterMarker = function () {
            $rootScope.$broadcast('filter markers', vm.filters);
        };

        vm.filterRoute = function () {
            $rootScope.$broadcast('filter routes', vm.routeFilters);
        };

        vm.search = function () {
            $rootScope.$broadcast('filter change', vm.searchText);
        };

        function updateRouteFilter() {
            _.each(vm.routeNames, function (item, index) {
                vm.routeFilters[index] = true;
            });
            vm.filterRoute();
        }

        function init() {

            vm.filters = {
                'good': true,
                'bad': true,
                'ugly': true,
                'planned': true,
                'route': true
            };
            vm.routeFilters = [];
            vm.filterRoute();

            vm.routeNames = $rootScope.routeNames;
            updateRouteFilter();

            $rootScope.$on('update route names', function (evt, routeNames) {
                vm.routeNames = routeNames;
                updateRouteFilter();
            });

            $rootScope.$on('add route name', function (evt, data) {
                if (vm.routeNames.indexOf(data.routeName) === -1) {
                    vm.routeNames.push(data.routeName);
                }
                updateRouteFilter();
            });
        }

        init();
    }
})();