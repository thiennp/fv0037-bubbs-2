mapboxgl.accessToken = 'pk.eyJ1IjoidGhpZW5ucCIsImEiOiJjajJoODVkMDkwMDBlMzNwZDl0dm82anpjIn0.-XO6yaXgc-t8VrASeHL_vA';
var styles = [
    'mapbox://styles/mapbox/traffic-day-v2',
    'mapbox://styles/mapbox/streets-v10',
    'mapbox://styles/mapbox/light-v9'
]

var currentStyles = 0;

var lMap, locationData, leftSidebarData, chartsData, lat, lng, zoom;
var bounds = [];

var sidebar = [];
var mapMarkers = [];

var markers = [];
var markerEls = [];
var hidden = [];
var geoHistory = [];
var activeDistance;
var lastLat, lastLng;
var newAddedMarker = {};


var activeMarker;

var features, geojson, linestring, geocoder;

lat = 44.433486;
lng = 10.409613;

function redraw() {
    linestring.geometry.coordinates = [[lng, lat]];

    var marker = _.find(markers, function (item) {
        return item.id == activeMarker;
    });

    if (marker) {
        linestring.geometry.coordinates.push([marker.lngLat.lng, marker.lngLat.lat]);
    }

    geojson.features.push(linestring);

    if (!lMap.getSource('geojson')) {
        resetLayer();
    }
    lMap.getSource('geojson').setData(geojson);
}

function updateMarker() {
    geojson.features = [];
    for (i = 0; i < markers.length; i++) {
        if (!hidden[i]) {
            geojson.features.push(markers[i].point);

            if (markers[i].distance < activeDistance) {
                activeDistance = markers[i].distance;
                activeMarker = markers[i].id;
            }

            if (!markerEls[i]) {
                var html = '';
                html += '<div class="menu-item-marker">';
                html += '  <div class="menu-item-marker-node"></div>';
                html += '  <div class="menu-item-marker-title"><strong>M' + (i + 1) + '</strong>(<i>' + markers[i].distance + 'km</i>)</div>';
                html += '  <div class="menu-item-marker-tooltip">bla bla bla m' + (i + 1) + '</div>';
                html += '</div>';
                var el = document.createElement('div');
                el.innerHTML = html;

                markerEls[i] = new mapboxgl.Marker(el)
                    .setLngLat([markers[i].lngLat.lng, markers[i].lngLat.lat])
                    .addTo(lMap);
            }
        } else {
            if (activeMarker == markers[i].id) {
                activeMarker = null;
            }

            if (markerEls[i]) {
                markerEls[i].remove();
                markerEls[i] = null;
            }
        }
    }

    $('#sidebar-2').html(markerMenuHtml());
    redraw();
}

function autoActive() {
    if (!activeMarker) {
        activeMarker = null;
        activeDistance = 999999999;
        _.each(markers, function (item, index) {
            if (Number(item.distance) < activeDistance && !hidden[index]) {
                activeDistance = Number(item.distance);
                activeMarker = item.id;
            }
        });
    } else {
        activeMarker = null;
    }

    updateMarker();
}

function markerMenuHtml() {
    var html = '';
    html += '<div class="box-title">';
    html += '  <div class="box-closeButton" onclick="hideSidebar(1)">';
    html += '    <svg viewBox="0 0 24 24">';
    html += '      <path d="M22.2,4c0,0,0.5,0.6,0,1.1l-6.8,6.8l6.9,6.9c0.5,0.5,0,1.1,0,1.1L20,22.3c0,0-0.6,0.5-1.1,0L12,15.4l-6.9,6.9c-0.5,0.5-1.1,0-1.1,0L1.7,20c0,0-0.5-0.6,0-1.1L8.6,12L1.7,5.1C1.2,4.6,1.7,4,1.7,4L4,1.7c0,0,0.6-0.5,1.1,0L12,8.5l6.8-6.8c0.5-0.5,1.1,0,1.1,0L22.2,4z">';
    html += '      </path>';
    html += '    </svg>';
    html += '  </div>';
    html += '</div>';
    for (var i = 0; i < markers.length; i++) {
        html += '<div class="description-title col-xs-6">M' + (i + 1) + '</div>';
        html += '<div class="col-xs-6"><div class="switch"><input onchange="toggleMarker(' + i + ')" id="cmn-toggle-' + i + '" class="cmn-toggle cmn-toggle-round" type="checkbox" ';
        if (!hidden[i]) {
            html += 'checked="checked"';
        }
        html += '><label for="cmn-toggle-' + i + '"></label></div></div>';
        html += '<div class="clearfix"></div>';
    }
    html += '<button class="btn btn-primary auto-active" onclick="autoActive()">Auto</button>';

    return html;
}

function leftMenuHtml() {
    var html = '';
    html += '<div class="box-title">';
    html += '  <div>' + leftSidebarData.title + '</div>';
    html += '  <div class="box-closeButton" onclick="hideSidebar(2)">';
    html += '    <svg viewBox="0 0 24 24">';
    html += '      <path d="M22.2,4c0,0,0.5,0.6,0,1.1l-6.8,6.8l6.9,6.9c0.5,0.5,0,1.1,0,1.1L20,22.3c0,0-0.6,0.5-1.1,0L12,15.4l-6.9,6.9c-0.5,0.5-1.1,0-1.1,0L1.7,20c0,0-0.5-0.6,0-1.1L8.6,12L1.7,5.1C1.2,4.6,1.7,4,1.7,4L4,1.7c0,0,0.6-0.5,1.1,0L12,8.5l6.8-6.8c0.5-0.5,1.1,0,1.1,0L22.2,4z">';
    html += '      </path>';
    html += '    </svg>';
    html += '  </div>';
    html += '</div>';
    for (var i = 0; i < leftSidebarData.description.length; i++) {
        html += '<h3>' + leftSidebarData.description[i].title + '</h3>';
        for (var j = 0; j < leftSidebarData.description[i].params.length; j++) {
            html += '<div class="description-title col-xs-6">' + leftSidebarData.description[i].params[j].title + '</div>';
            html += '<div class="description-value col-xs-6">' + leftSidebarData.description[i].params[j].value + '</div>';
            html += '<div class="clearfix"></div>';
        }
    }

    return html;
}

function bottomMenuHtml() {
    var html = '';
    html += '<div class="box-title">';
    html += '  <div class="box-closeButton" onclick="hideSidebar(2)">';
    html += '    <svg viewBox="0 0 24 24">';
    html += '      <path d="M22.2,4c0,0,0.5,0.6,0,1.1l-6.8,6.8l6.9,6.9c0.5,0.5,0,1.1,0,1.1L20,22.3c0,0-0.6,0.5-1.1,0L12,15.4l-6.9,6.9c-0.5,0.5-1.1,0-1.1,0L1.7,20c0,0-0.5-0.6,0-1.1L8.6,12L1.7,5.1C1.2,4.6,1.7,4,1.7,4L4,1.7c0,0,0.6-0.5,1.1,0L12,8.5l6.8-6.8c0.5-0.5,1.1,0,1.1,0L22.2,4z">';
    html += '      </path>';
    html += '    </svg>';
    html += '  </div>';
    html += '</div>';
    for (var i = 0; i < leftSidebarData.description.length; i++) {
        html += '<h3>' + leftSidebarData.description[i].title + '</h3>';
        for (var j = 0; j < leftSidebarData.description[i].params.length; j++) {
            html += '<div class="description-title col-xs-6">' + leftSidebarData.description[i].params[j].title + '</div>';
            html += '<div class="description-value col-xs-6">' + leftSidebarData.description[i].params[j].value + '</div>';
            html += '<div class="clearfix"></div>';
        }
    }

    return html;
}

function toggleMarker(i) {
    hidden[i] = !hidden[i];

    updateMarker();
}

function newPoint(lng, lat) {
    return point = {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [
                lng,
                lat
            ]
        },
        "properties": {
            "id": String(new Date().getTime()) + Math.random()
        }
    };
}

function resetLayer() {
    lMap.addSource('geojson', {
        "type": "geojson",
        "data": geojson
    });

    // Add styles to the map
    lMap.addLayer({
        id: 'measure-points',
        type: 'circle',
        source: 'geojson',
        paint: {
            'circle-radius': 20,
            'circle-color': '#505963'
        },
        filter: ['in', '$type', 'Point']
    });

    lMap.addLayer({
        id: 'measure-lines',
        type: 'line',
        source: 'geojson',
        layout: {
            'line-cap': 'round',
            'line-join': 'round'
        },
        paint: {
            'line-color': '#505963',
            'line-width': 2.5
        },
        filter: ['in', '$type', 'LineString']
    });
}

function mainMarkerHTML(url) {
    var html = '';
    html += '<div class="load closed load-0">';
    html += '  <div class="load-pulse">';
    html += '    <div class="load-pulse-container"></div>';
    html += '  </div>';
    html += '  <nav class="menu">';
    html += '    <input type="checkbox" class="menu-open-' + i + ' menu-open" name="menu-open-' + i + '" id="menu-open-' + i + '" />';
    html += '    <label class="menu-open-button" for="menu-open-' + i + '" onclick="window.location.href=\'' + url + '\'">';
    html += '    </label>';
    html += '  </nav>';
    html += '</div>';
    return html;
}

function markerHTML(i) {
    var html = '';
    html += '<div class="load closed load-0">';
    html += '  <div class="load-pulse">';
    html += '    <div class="load-pulse-container"></div>';
    html += '  </div>';
    html += '  <div class="text-group text-0">';
    html += '    <div class="text-group-container">';
    html += '      <div class="line1"></div>';
    html += '      <div class="line2"></div>';
    html += '      <div class="text"></div>';
    html += '    </div>';
    html += '  </div>';
    html += '  <div class="text-group text-1">';
    html += '    <div class="text-group-container">';
    html += '      <div class="line1"></div>';
    html += '      <div class="text"></div>';
    html += '    </div>';
    html += '  </div>';
    html += '  <div class="text-group text-2">';
    html += '    <div class="text-group-container">';
    html += '      <div class="line1"></div>';
    html += '      <div class="line2"></div>';
    html += '      <div class="text"></div>';
    html += '    </div>';
    html += '  </div>';
    html += '  <nav class="menu">';
    html += '    <input type="checkbox" class="menu-open-' + i + ' menu-open" name="menu-open-' + i + '" id="menu-open-' + i + '" />';
    html += '    <label class="menu-open-button" for="menu-open-' + i + '">';
    html += '      <span class="hamburger hamburger-1"></span>';
    html += '      <span class="hamburger hamburger-2"></span>';
    html += '      <span class="hamburger hamburger-3"></span>';
    html += '    </label>';

    html += '    <a class="menu-item"><i class="fa fa-google"></i></a>';
    html += '    <a class="menu-item"><i class="fa fa-facebook"></i></a>';
    html += '    <a class="menu-item"><i class="fa fa-twitter"></i></a>';
    html += '    <a class="menu-item"><i class="fa fa-vine"></i></a>';

    html += '    <a class="menu-item"><i class="fa fa-trello"></i></a>';
    html += '    <a class="menu-item"><i class="fa fa-pinterest"></i></a>';

    html += '    <svg xmlns="http://www.w3.org/2000/svg" version="1.1">';
    html += '      <defs>';
    html += '        <filter id="shadowed-goo">';
    html += '            <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="10" />';
    html += '            <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo" />';
    html += '            <feGaussianBlur in="goo" stdDeviation="3" result="shadow" />';
    html += '            <feColorMatrix in="shadow" mode="matrix" values="0 0 0 0 0  0 0 0 0 0  0 0 0 0 0  0 0 0 1 -0.2" result="shadow" />';
    html += '            <feOffset in="shadow" dx="1" dy="1" result="shadow" />';
    html += '            <feComposite in2="shadow" in="goo" result="goo" />';
    html += '            <feComposite in2="goo" in="SourceGraphic" result="mix" />';
    html += '        </filter>';
    html += '        <filter id="goo">';
    html += '            <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="10" />';
    html += '            <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo" />';
    html += '            <feComposite in2="goo" in="SourceGraphic" result="mix" />';
    html += '        </filter>';
    html += '      </defs>';
    html += '    </svg>';
    html += '  </nav>';
    html += '</div>';
    return html;
}

function historyMarkerHtml(geo) {
    var html = '';
    html += '<div class="history-marker">';
    html += '    <div class="history-marker__tooltip">';
    html += '        <div class="detail"><strong>Lat:</strong> ' + geo.lat + '</div>';
    html += '        <div class="detail"><strong>Lng:</strong> ' + geo.lng + '</div>';
    html += '        <div class="detail"><strong>Timestamp:</strong> ' + new Date(geo.timestamp).toDateString() + '</div>';
    html += '    <div>';
    html += '<div>';
    return html;
}

function showSidebar(id) {
    $('#sidebar-' + (id + 1)).addClass('active');
    switch (id) {
        case 0:
            $('#sidebar-1 .context').html(contextMenuHtml());
            break;
        case 1:
            updateMarker();
            break;
        case 2:
            $('#sidebar-3').html(leftMenuHtml());
            break;
    }

    if (id == 0) {
        if (sidebar[1]) {
            $('#sidebar-1').removeClass('active');
            $('#sidebar-1').addClass('active-both');
        }
    }

    if (id == 1) {
        if (sidebar[0]) {
            $('#sidebar-1').removeClass('active');
            $('#sidebar-1').addClass('active-both');
        }
    }

    sidebar[id] = true;
}

function hideSidebar(id) {
    $('#sidebar-' + (id + 1)).removeClass('active').removeClass('active-both');
    $('.menu-item').removeClass('active');

    switch (id) {
        case 0:
            if (!sidebar[1] && !sidebar[2] && !sidebar[3]) {
                $('.load-pulse-container').removeClass('active');
            }
            break;
        case 1:
            if (sidebar[0]) {
                $('#sidebar-1').removeClass('active-both').addClass('active');
            }
            if (!sidebar[0] && !sidebar[2] && !sidebar[3]) {
                $('.load-pulse-container').removeClass('active');
            }
            break;
        case 2:
            if (!sidebar[0] && !sidebar[1] && !sidebar[3]) {
                $('.load-pulse-container').removeClass('active');
            }
            break;
        case 3:
            if (!sidebar[0] && !sidebar[1] && !sidebar[2]) {
                $('.load-pulse-container').removeClass('active');
            }
            break;
    }

    sidebar[id] = false;
}

function toggleSidebar(id) {
    if (sidebar[id]) {
        hideSidebar(id);
    } else {
        showSidebar(id);
    }

    if (sidebar[0]) {
        $('.menu-item:nth-child(3)').addClass('active');
    }

    if (sidebar[1]) {
        $('.menu-item:nth-child(6)').addClass('active');
    }

    if (sidebar[2]) {
        $('.menu-item:nth-child(7)').addClass('active');
    }

    if (sidebar[3]) {
        $('.menu-item:nth-child(8)').addClass('active');
    }
}

function contextMenuHtml() {
    var html = '';
    html += '<div class="box-title">';
    html += '  <div>' + locationData.title + '</div>';
    html += '  <div class="box-closeButton" onclick="hideSidebar(0)">';
    html += '    <svg viewBox="0 0 24 24">';
    html += '      <path d="M22.2,4c0,0,0.5,0.6,0,1.1l-6.8,6.8l6.9,6.9c0.5,0.5,0,1.1,0,1.1L20,22.3c0,0-0.6,0.5-1.1,0L12,15.4l-6.9,6.9c-0.5,0.5-1.1,0-1.1,0L1.7,20c0,0-0.5-0.6,0-1.1L8.6,12L1.7,5.1C1.2,4.6,1.7,4,1.7,4L4,1.7c0,0,0.6-0.5,1.1,0L12,8.5l6.8-6.8c0.5-0.5,1.1,0,1.1,0L22.2,4z">';
    html += '      </path>';
    html += '    </svg>';
    html += '  </div>';
    html += '</div>';
    for (var i = 0; i < locationData.description.length; i++) {
        html += '<h3>' + locationData.description[i].title + '</h3>';
        for (var j = 0; j < locationData.description[i].params.length; j++) {
            html += '<div class="description-title col-xs-6">' + locationData.description[i].params[j].title + '</div>';
            html += '<div class="description-value col-xs-6">' + locationData.description[i].params[j].value + '</div>';
            html += '<div class="clearfix"></div>';
        }
    }

    return html;
}

function getData() {
    for (var i = 0; i < 3; i++) {
        applyData(i);
    }
}

function applyData(id) {
    $.getJSON('./data/device_' + id + '_spd.json', function (data) {
        $.getJSON(data.outputUrl + '#' + Math.floor(Math.random() * 1000000), function (res) {
            $('.load-0 .text-' + id + ' .text').html('LAT: ' + res[0].lat + ', LON: ' + res[0].lng + ', SPD: ' + res[0].spd);
        });
    });
}

function createMarkers() {
    var el = document.createElement('div');
    el.innerHTML = markerHTML(geoHistory.length - 1);

    mapMarkers[geoHistory.length - 1] = new mapboxgl.Marker(el)
        .setLngLat([lng, lat])
        .addTo(lMap);

    lMap.flyTo({
        center: [lng, lat],
        speed: 0.2
    });

    for (var i = 0; i < geoHistory.length - 1; i++) {
        var el = document.createElement('div');
        el.innerHTML = historyMarkerHtml(geoHistory[i]);
        mapMarkers[i] = new mapboxgl.Marker(el)
            .setLngLat([geoHistory[i].lng, geoHistory[i].lat])
            .addTo(lMap);
    }

    hideFakeMarker();
}

function hideFakeMarker() {
    setTimeout(function () {
        $('.fakeloader').addClass('hide-fakeloader');
        setTimeout(function () {
            $('.fakeloader').hide();
        }, 300);
    }, 300);
}

function createMainMarkers(locs) {
    for (i = 0; i < locs.length; i++) {
        var el = document.createElement('div');
        el.innerHTML = mainMarkerHTML('#/home/' + i);

        mapMarkers[i] = new mapboxgl.Marker(el)
            .setLngLat([locs[i].lng, locs[i].lat])
            .addTo(lMap);
        hideFakeMarker();
    }
}

function removeMarkers() {
    for (var i = 0; i < mapMarkers.length; i++) {
        mapMarkers[i].remove();
    }
}

window.updateLMapMarker = function () {
    removeMarkers();
    createMarkers();
}

window.updateLMapMainMarker = function (locs) {
    removeMarkers();
    createMainMarkers(locs);
}

window.initLMap = function ($state, status) {
    var params = {
        container: 'map',
        style: styles[currentStyles],
        center: [lng, lat],
        zoom: zoom
    };
    lMap = new mapboxgl.Map(params);

    geocoder = new mapboxgl.Geocoder();

    lMap.addControl(geocoder);

    // GeoJSON object to hold our measurement features
    geojson = {
        "type": "FeatureCollection",
        "features": []
    };

    // Used to draw a line between points
    linestring = {
        "type": "Feature",
        "geometry": {
            "type": "LineString",
            "coordinates": []
        }
    };

    lMap.on('load', function () {
        resetLayer();

        geojson.features.push(newPoint(lng, lat));

        lMap.on('click', function (e) {
            if (!window.onAddingNewMarker) {
                setTimeout(function () {
                    if (!window.onMenuOpen) {
                        features = lMap.queryRenderedFeatures(e.point, { layers: ['measure-points'] });

                        if (features.length) {
                            var id = features[0].properties.id;
                            geojson.features = geojson.features.filter(function (point) {
                                return point.properties.id !== id;
                            });

                            _.remove(markers, function (item) {
                                return item.id === id;
                            });
                        } else {
                            var point = newPoint(e.lngLat.lng, e.lngLat.lat);

                            linestring.geometry.coordinates = [[lng, lat], [e.lngLat.lng, e.lngLat.lat]];

                            markers.push({
                                id: point.properties.id,
                                point: point,
                                lngLat: e.lngLat,
                                distance: turf.lineDistance(linestring).toLocaleString()
                            });
                        }

                        if (activeMarker) {
                            autoActive();
                        }

                        updateMarker();
                    } else {
                        setTimeout(function () {
                            window.onMenuOpen = false;
                        }, 100);
                    }
                }, 50);
            } else {
                lastLat = e.lngLat.lat;
                lastLng = e.lngLat.lng;
                lastHum = 20;
                lastTemp = 30;
                lat = lastLat;
                lng = lastLng;
                newAddedMarker = {
                    lat: lat,
                    lng: lng,
                    temp: lastTemp,
                    hum: lastHum,
                    timestamp: new Date()
                };
                geoHistory.push(newAddedMarker);
                updateLMapMarker();
                $.get(localStorage.getItem('server') + '?private_key=' + localStorage.getItem('privateKey') + '&lat=' + lastLat + '&lng=' + lastLng + '&hum=' + lastHum + '&temp=' + lastTemp, function () {
                    // Do nothing now
                });
            }
        });

        geocoder.on('result', function (ev) {
            var newlnglat = ev.result.geometry.coordinates;
            mainMarker.setLngLat(newlnglat);
            lng = newlnglat[0];
            lat = newlnglat[1];
            redraw();
        });
    });

    // End distance

    getData();

    setTimeout(function () {
        $('.menu-item').click(function (e) {
            window.onMenuOpen = true;
            $('.menu-item').removeClass('active');
            $(this).addClass('active');
            $(this).parent().parent().find('.load-pulse-container').addClass('active');
            var index = $(this).index() - 2;
            switch (index) {
                case 0:
                    if (sidebar[0]) {
                        $('.menu-item:nth-child(3)').removeClass('active');
                    }
                    if (sidebar[1]) {
                        $('.menu-item:nth-child(6)').addClass('active');
                    }
                    toggleSidebar(0);
                    break;
                case 1:
                    currentStyles++;
                    if (currentStyles >= styles.length) {
                        currentStyles = 0;
                    }
                    lMap.setStyle(styles[currentStyles]);
                    if (activeMarker) {
                        setTimeout(function () {
                            redraw();
                        }, 1000);
                    }
                    break;
                case 2:
                    $('.load-0 .text-group').toggleClass('active');
                    break;
                case 3:
                    if (sidebar[0]) {
                        $('.menu-item:nth-child(3)').addClass('active');
                    }
                    if (sidebar[1]) {
                        $('.menu-item:nth-child(6)').removeClass('active');
                    }
                    toggleSidebar(1);
                    break;
                case 4:
                    toggleSidebar(2);
                    break;
                case 5:
                    // $state.go('charts');
                    break;
            }
        });

        $('.menu-open').click(function () {
            window.onMenuOpen = true;
            $(this).parent().parent().toggleClass('closed');
            $(this).parent().parent().find('.load-pulse-container').toggleClass('active');
        });
    }, 200);
}