var App = angular
    .module('Map', [
        'ui.router'
    ])
    .run(function ($rootScope) {
        $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams, options) {
                if (toState.name.indexOf('home') === -1) {
                    window.homeState = false;
                } else {
                    window.homeState = true;
                }
                $rootScope.server = null;
            });
    })
    .config(function ($urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
    });