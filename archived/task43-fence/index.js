'use strict';

const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const fileUpload = require('express-fileupload');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const md5 = require('md5');
const mongoose = require('mongoose');
const nodemailer = require('nodemailer');

const app = express();
app.use(bodyParser.json({
    limit: '5mb'
}));
app.use(bodyParser.urlencoded({
    limit: '5mb'
}));
app.use(fileUpload());

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/client'));
app.use(express.static(__dirname + '/upload'));

mongoose.connect('mongodb://localhost/fv0037-43');

const Schema = mongoose.Schema;

const FenceModel = new Schema({
    create: Date,
    update: Date,
    data: Object,
    name: String,
    area: String,
    edit: Date,
    userId: String,
    color: String,
});

const UserModel = new Schema({
    email: {
        unique: true,
        type: String
    },
    username: {
        unique: true,
        type: String
    },
    password: String,
    company: String,
    create: Date,
    edit: Date,
    image: String,
    name: String,
    bio: String,
    verificationCode: String,
    passwordCode: String,
    isAdmin: {
        default: false,
        type: Boolean,
    },
});

const Fence = mongoose.model('FenceModel', FenceModel);
const User = mongoose.model('UserModel', UserModel);

function log(title, text) {
    console.log('');
    console.log('');
    console.log('');
    console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
    console.log(title);
    if (text) {
        console.log(text);
    }
    console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
    console.log('');
    console.log('');
}

function checkUser(req, res, callback, isAdmin = false) {
    const userId = req.headers['x-access-id'];
    User.findOne({
        _id: userId,
    }, {}, (err, user) => {
        if (!err) {
            if (isAdmin) {
                if (user.isAdmin) {
                    callback();
                } else {
                    User.find({
                            isAdmin: true,
                        })
                        .exec((err, data) => {
                            if (!err) {
                                if (!data.length) {
                                    callback();
                                } else {
                                    res.send(new Error('Not an admin'));
                                }
                            } else {
                                res.send(err);
                            }
                        });
                }
            } else {
                callback();
            }
        } else {
            res.send(err);
        }
    });
}

function sendMail(email, text) {
    // TODO: Update email account here
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'shoresafety111@gmail.com',
            pass: 'shoresafety111ForTesting', // Update your email account here
        }
    });

    const mailOptions = {
        from: 'shoresafety111@gmail.com',
        to: email,
        subject: 'Registration',
        text: text,
    };

    log(`To ${email}:`, text);

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}

const apiRoutes = express.Router();
// route to authenticate a user (POST http://localhost:8080/api/authenticate)

// route middleware to verify a token
apiRoutes.use((req, res, next) => {
    // check header or url parameters or post parameters for token
    const token = req.body.token || req.query.token || req.headers['x-access-token'];

    // decode token
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, 'secret', (err, decoded) => {
            if (err) {
                return res.send(401, {
                    success: false,
                    message: 'Failed to authenticate token.'
                });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });
    } else {
        // if there is no token
        // return an error
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    }
});

app.use('/api', apiRoutes);
app.use(express.static('/upload'));

app.get('/', (_req, res) => {
    res.sendFile('client/index.html', {
        root: __dirname
    });
});

app.get('/api/fence', (_req, res) => {
    // const userId = req.headers['x-access-id'];
    Fence.find({})
        .exec((err, data) => {
            if (!err) {
                res.send(data);
            } else {
                res.send(err);
            }
        });
});

app.post('/api/fence', (req, res) => {
    // const userId = req.headers['x-access-id'];
    req.body.create = new Date();
    const data = new Fence(req.body);
    data.save((err, result) => {
        if (!err) {
            res.send(result);
        } else {
            res.send(err);
        }
    });
});

app.put('/api/fence/:id', (req, res) => {
    // const userId = req.headers['x-access-id'];
    req.body.update = new Date();
    Fence.update({
        _id: req.params.id
    }, {
        $set: req.body
    }, (err, result) => {
        if (!err) {
            res.send(result);
        } else {
            res.send(err);
        }
    });
});

app.delete('/api/fence/:id', (req, res) => {
    log(`DELETE api/fence/${req.params.id}`);
    // const userId = req.headers['x-access-id'];
    let condition;
    condition = {
        _id: req.params.id,
    };

    const Fence = mongoose.model('FenceModel', FenceModel);
    Fence.remove(condition,
        (err) => {
            if (!err) {
                res.send(req.body);
            } else {
                res.send(err);
            }
        });
});

app.post('/auth/register', (req, res) => {
    const User = mongoose.model('UserModel', UserModel);
    User.findOne({
        email: req.body.email,
    }, (err, user) => {
        if (err) {
            throw err;
        }
        if (!user) {
            User.findOne({
                username: req.body.username,
            }, (_err, user) => {
                if (!user) {
                    req.body.create = new Date();
                    req.body.password = md5(req.body.password);
                    req.body.company = req.body.company;
                    req.body.verificationCode = md5(Math.random());
                    user = new User(req.body);
                    log('Registering', user);
                    user.save((err, result) => {
                        if (!err) {
                            log('Registered', result);
                            // const mailContent = req.headers.host + '/#!/verify/' + req.body.verificationCode;
                            // sendMail('bubling333@gmail.com', mailContent)
                            res.send(result);
                        } else {
                            res.send(err);
                        }
                    });
                } else {
                    res.send(401, {
                        success: false,
                        message: 'Username is already exist'
                    });
                }
            });
        } else {
            res.send(401, {
                success: false,
                message: 'Email is already exist'
            });
        }
    });
});

app.post('/auth/password', (req, res) => {
    const User = mongoose.model('UserModel', UserModel);
    log('Password code', req.body.passwordCode);
    User.findOne({
        passwordCode: req.body.passwordCode,
    }, (err, user) => {
        if (err) {
            throw err;
        }
        if (user) {
            log('user', user);
            user.password = md5(req.body.password);
            user.save((err, result) => {
                if (!err) {
                    log(`Password updated to: ${user.email}`);
                    res.send(result);
                } else {
                    res.send(err);
                }
            });
        } else {
            res.send(401, {
                success: false,
                message: 'passwordCode us not correct'
            });
        }
    });
});

app.post('/auth/forgot', (req, res) => {
    const User = mongoose.model('UserModel', UserModel);
    User.findOne({
        email: req.body.email,
    }, (err, user) => {
        if (err) {
            throw err;
        }
        if (user) {
            user.passwordCode = md5(Math.random());
            user.save((err, result) => {
                if (!err) {
                    const mailContent = req.headers.host + '/#!/password/' + user.passwordCode;
                    sendMail(user.email, mailContent);
                    res.send(result);
                } else {
                    res.send(err);
                }
            });
        } else {
            res.send(401, {
                success: false,
                message: 'Email doens\'t exist'
            });
        }
    });
});

app.get('/auth/verification/:verificationCode', (req, res) => {
    const User = mongoose.model('UserModel', UserModel);
    User.findOne({
        verificationCode: req.params.verificationCode,
    }, (err, user) => {
        if (err) {
            throw err;
        }
        if (user) {
            user.verificationCode = null;
            user.save((err, result) => {
                if (!err) {
                    res.send(result);
                } else {
                    res.send(err);
                }
            });
        } else {
            res.send(401, {
                success: false,
                message: 'Can not find user'
            });
        }
    });
});

app.post('/auth/login', (req, res) => {
    User.findOne({
        username: req.body.username,
        password: md5(req.body.password),
    }, (err, user) => {
        if (err) {
            throw err;
        }

        if (user.verificationCode) {
            res.send(401, {
                success: false,
                message: 'User is not verified, please verify user by clicking the link in your email.'
            });
            log(`/auth/verification/${user.verificationCode}`);
            return;
        }

        if (!user) {
            res.send(401, {
                success: false,
                message: 'Authentication failed. Please check your email or password.'
            });
        } else if (user) {
            const token = jwt.sign({}, 'secret');

            // return the information including token as JSON
            res.send({
                success: true,
                token: token,
                id: user._id,
                email: user.email,
                bio: user.bio,
                name: user.name,
                image: user.image,
                username: user.username,
            });
        }
    });
});

app.get('/api/profile', (req, res) => {
    const userId = req.headers['x-access-id'];
    User.findOne({
        _id: userId,
    }, {}, (err, user) => {
        if (!err) {
            res.send(user);
        } else {
            res.send(err);
        }
    });
});

app.put('/api/profile', (req, res) => {
    const userId = req.headers['x-access-id'];
    if (req.body.uploadImage) {
        const base64Data = req.body.uploadImage.replace(/^data:image\/png;base64,/, '');
        const filename = 'upload_' + Math.floor(Math.random() * 1000000000) + '.png';
        require('fs').writeFile('upload/' + filename, base64Data, 'base64', (err) => {
            console.log(err);
        });
        req.body.image = filename;
        delete req.body.uploadImage;
    }
    req.body.edit = new Date();

    User
        .update({
            _id: userId
        }, {
            $set: req.body
        }, (err) => {
            if (!err) {
                User.findOne({
                    _id: userId
                }, {
                    'email': true,
                    'image': true,
                    'name': true,
                    'bio': true
                }, (err, user) => {
                    if (!err) {
                        res.send(user);
                    } else {
                        res.send(err);
                    }
                });
            } else {
                res.send(err);
            }
        });
});

app.post('/upload', (req, res) => {
    if (!req.files) {
        return res.status(400).send('No files were uploaded.');
    }
    // The name of the input field (i.e. "file") is used to retrieve the uploaded file
    const file = req.files.file;
    const filename = file.name;

    // Use the mv() method to place the file somewhere on your server
    file.mv('upload/' + filename, (err) => {
        if (err) {
            return res.status(500).send(err);
        }
        console.log('file upload to /upload/' + filename);

        res.send({
            filename: filename,
        });
    });
});

app.post('/api/image/pulse/:filename', (req, res) => {
    const filename = req.originalUrl.split('pulse/')[1];
    const base64Data = req.body.data.replace(/^data:image\/png;base64,/, '');
    console.log(filename);
    require('fs').writeFile('upload/pulse/' + filename + '.png', base64Data, 'base64', (err) => {
        console.log(err);
        res.send({
            filename: filename,
        });
    });
});

app.get('/upload', (req, res) => {
    fs.readdir('upload', (err, files) => {
        if (err) {
            return res.status(500).send(err);
        }
        files.forEach(file => {
            files.push(file);
        });

        res.status(200).send(files);
    });
});

app.delete('/upload/:filename', (req, res) => {
    const filename = req.originalUrl.split('upload/')[1];
    if (filename) {
        fs.unlink('upload/' + filename, (err) => {
            if (err) {
                return res.status(500).send(err);
            }
            res.status(200).send({
                success: true,
            });
        });
    } else {
        return res.status(500).send({
            message: 'Wrong path'
        });
    }
});

app.get('/api/users', (req, res) => {
    checkUser(req, res, () => {
        User.find({})
            .exec((err, data) => {
                if (!err) {
                    res.send(data);
                } else {
                    res.send(err);
                }
            });
    }, true);
});

app.put('/api/user/:id', (req, res) => {
    checkUser(req, res, () => {
        User.update({
            _id: req.params.id
        }, {
            $set: req.body
        }, (err, result) => {
            if (!err) {
                res.send(result);
            } else {
                res.send(err);
            }
        });
    }, true);
});

app.listen(8080, () => {
    console.log('Example app listening on port 8080!');
});
