(function () {
    'use strict';

    angular.module('app')
        .controller('AuthCtrl', ['$http', '$state', '$rootScope', '$scope', AuthCtrl])

    function AuthCtrl($http, $state, $rootScope) {
        let $ctrl = this;
        localStorage.removeItem('user_username');
        localStorage.removeItem('user_image');
        localStorage.removeItem('user_bio');
        localStorage.removeItem('user_email');

        this.$onInit = () => {
            $ctrl.type = 1;
            localStorage.removeItem('token');
            localStorage.removeItem('id');
        }

        function isValidPassword() {
            var valid = false;
            var str = ['ABCDEFGHIJKLMNOPQRSTUVWXYZ', '0123456789'];
            for (var i = 0; i < str[0].length; i++) {
                if ($ctrl.password.indexOf(str[0][i]) > -1) {
                    for (var j = 0; j < str[1].length; j++) {
                        if ($ctrl.password.indexOf(str[1][j]) > -1) {
                            valid = true;
                            break;
                        }
                    }
                    break;
                }
            }
            return valid;
        }

        $ctrl.changeType = function (type) {
            $ctrl.type = type;
            $ctrl.err = undefined;
        }

        $ctrl.login = function () {
            delete $ctrl.err;
            if (!$ctrl.password) {
                $ctrl.err = {
                    data: {
                        message: 'Password is required'
                    }
                };
            } else if (!$ctrl.username) {
                $ctrl.err = {
                    data: {
                        message: 'Username is require'
                    }
                };
            } else {
                $http.post(window.authUrl + 'login', {
                    username: $ctrl.username,
                    password: $ctrl.password
                }).then(function (result) {
                    if (result && result.data && result.data.success) {
                        localStorage.setItem('token', result.data.token);
                        if (!!result.data.name) {
                            localStorage.setItem('user_name', result.data.name);
                        }
                        if (!!result.data.username) {
                            localStorage.setItem('user_username', result.data.username);
                        }
                        if (!!result.data.email) {
                            localStorage.setItem('user_email', result.data.email);
                        }
                        if (!!result.data.image) {
                            localStorage.setItem('user_image', result.data.image);
                        }
                        if (!!result.data.bio) {
                            localStorage.setItem('user_bio', result.data.bio);
                        }
                        localStorage.setItem('id', result.data.id);
                        $rootScope.$broadcast('auth');
                        $state.go('dashboard');
                    }
                }, function (err) {
                    $ctrl.err = err;
                });
            }
        };

        $ctrl.register = function () {
            delete $ctrl.err;
            if (!$ctrl.password) {
                $ctrl.err = {
                    data: {
                        message: 'Password is required'
                    }
                };
            } else if (!$ctrl.retypePassword) {
                $ctrl.err = {
                    data: {
                        message: 'Please re-type password'
                    }
                };
            } else if (!$ctrl.email) {
                $ctrl.err = {
                    data: {
                        message: 'Email is require'
                    }
                };
            } else if (!$ctrl.username) {
                $ctrl.err = {
                    data: {
                        message: 'Username is require'
                    }
                };
            } else if (!isValidPassword()) {
                $ctrl.err = {
                    data: {
                        message: 'Password must contain Number and Capital letter'
                    }
                };
            } else if ($ctrl.password !== $ctrl.retypePassword) {
                $ctrl.err = {
                    data: {
                        message: 'Retype password is not matched'
                    }
                };
            } else {
                $http.post(window.authUrl + 'register', {
                    email: $ctrl.email,
                    username: $ctrl.username,
                    password: $ctrl.password,
                    company: $ctrl.company,
                }).then(function (result) {
                    try {
                        if (result.data._id) {
                            $ctrl.type = 1;
                        }
                    } catch (e) {
                        console.log(e);
                    }
                }, function (err) {
                    $ctrl.err = err;
                });
            }
        };

        $ctrl.sendEmail = function () {
            $http.post(window.authUrl + 'forgot', {
                email: $ctrl.email,
            }).then(function (result) {
                alert('Email is sent, please check');
            }, function (err) {
                alert('Email doesn\'t exist');
                $ctrl.err = err;
            });
        };
    }
})();
