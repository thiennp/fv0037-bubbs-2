/**
 * Refer to:
 * https://www.mapbox.com/mapbox-gl-js/example/mapbox-gl-draw/
 */

(function () {
    'use strict';

    angular.module('app')
        .controller('FenceCtrl', ['$http', '$state', '$scope', '$timeout', '$q', FenceCtrl]);

    /**
     * Here we inject all the injections
     * $http, $state, $scope, $timeout and $q are native angular injections
     * $http takes care of sending and getting request to/from server
     * 
     * $state is used to get the param from the current URL, all STATES are defined in config.route.js
     * for example, if user enter something like '.../geo/fence/abcdef/1',
     * the app will look in to the route list in that find
     * and it will finds out that the route will match url: '/geo/fence/:index/:pulseIndex' at line 46
     * therefore it will go to the state geo/fence defined on line 45 (all in config.route.js)
     * it will open the controller FenceCtrl (this controller)
     * and template fence.html (line 47)
     * before going to that state
     * it run the ocLazyLoad --- 'fence'
     * defined in config.lazyload.js:line 51
     * it will load all js and css to the current page
     * 
     * Back again, url '.../geo/fence/abcdef/1' match the url '/geo/fence/:index/:pulseIndex' of the state
     * therefore $state.params.index will be assigned by 'abcdef'
     * so we can use $state.params.index to get the index value from the url -- this file, line 114
     * 
     * $scope is a variable that help us to transfer data/value from this file (controller) to fence.html (template)
     * the controller and the template are combined by the $state (route.config.js)
     * that means a controller can have multiple different template or the other way around
     * so for example
     * if this file you set $scope.a = '123'
     * if you want to display a 123 text in the browser
     * in template you need to add {{ a }}
     * so it is like <div>{{ a }}</div>
     * then the browser will render as <div>123</div>
     * that's the use of the $scope
     * 
     * $timeout is nothing different than setTimeout
     * however it help angular detect the changes inside its flow
     * where setTimeout doesn't have
     * 
     * $q is an object support async method
     * for example
     * you call 2 request
     * get data from a
     * and get data from b
     * and you want to wait until both a and b finish to run the function c
     * you can write it like $q.all([a, b]).then(c)
     * something like that
     */
    function FenceCtrl($http, $state, $scope, $timeout, $q) {
        /**
         * Change state if the state changed before ans saved to localstorage
         */
        if (isNaN(Number($state.params.index)) && localStorage.getItem('activeFence') && String(localStorage.getItem('activeFence')) !== String($state.params.index)) {
            $state.go('geo/fence', {
                index: localStorage.getItem('activeFence'),
            });
            return;
        }
        /**
         * lastData is to store the last get data
         * to compare with the new get data
         */
        let lastData;
        /**
         * pulse layer is  a map's layer
         */

        mapboxgl.accessToken = window.mapboxGLToken;

        /* eslint-disable */
        /**
         * This create a map
         */
        $scope.map = new mapboxgl.Map({
            container: 'map', // container id
            style: 'mapbox://styles/mapbox/satellite-v9', //hosted style id
            center: [-91.874, 42.760], // starting position
            zoom: 12 // starting zoom
        });

        /**
         * This define the marker list
         */
        $scope.marker = [];
        $scope.added = [];
        $scope.fenceElement = {};

        /**
         * Defines whether the puslse should be shown or not
         */
        $scope.showPulse = true;

        this.$onInit = () => {
            /**
             * If token is not stored in localStorage
             * mean user hasn't logged in
             * then go to auth page
             */
            if (!localStorage.getItem('token')) {
                $state.go('auth');
                return;
            }

            /**
             * Get current active fence (stored in localStorage)
             */
            $scope.active = localStorage.getItem('fence');

            /**
             * New fence data
             */
            $scope.fence = {
                name: '',
                data: {},
            };

            /**
             * Get current index from param, to know which fence are being selected
             * if no fence is selected, then use the new fence (created above)
             */
            $scope.index = $state.params.index;
            $scope.pulseIndex = $state.params.pulseIndex;

            // Draw object
            $scope.draw = new MapboxDraw({
                displayControlsDefault: false,
                controls: {
                    polygon: true,
                    trash: true,
                }
            });

            // Nothing to explain here
            $scope.map.addControl($scope.draw);
            $scope.map.on('draw.create', updateArea);
            $scope.map.on('draw.delete', updateArea);
            $scope.map.on('draw.update', updateArea);

            console.log($state.params.index);
            $scope.$watch(() => $state.params.index, () => {
                console.log($state.params.index);
            });

            // Get data for the first time (true)
            getFenceData(true);
        };

        function showAllFence() {
            return $scope.activeIndex === 'all';
        }

        function fitBound(index) {
            $scope.fittedBound = true;
            var coordinates = [];

            try {
                /**
                 * Added fence to the bound
                 * to fit to the map
                 */
                if (showAllFence()) {
                    coordinates = $scope.list.reduce((sum, item) => {
                        try {
                            sum = sum.concat(item.data.features[0].geometry.coordinates[0]);
                        } catch (e) { }
                        return sum;
                    }, [])
                } else {
                    coordinates = $scope.list[index].data.features[0].geometry.coordinates[0];
                }
            } catch (e) { }
            var bounds = new mapboxgl.LngLatBounds();
            var boundsLen = 0;
            for (var i = 0; i < coordinates.length; i++) {
                if (!isNaN(coordinates[i][0]) && !isNaN(coordinates[i][1])) {
                    bounds.extend(new mapboxgl.LngLat(coordinates[i][0], coordinates[i][1]));
                    boundsLen++;
                }
            }

            if (boundsLen > 1) {
                $scope.map.fitBounds(bounds);
            } else if (boundsLen === 1) {
                $scope.map.setCenter(bounds[0]);
            }
        }

        function getFenceData(firstTry) {
            /**
             * Get data from http
             */
            $http.get(window.apiUrl + 'fence')
                /**
                 * Do nothing until data is returned
                 * then(callbackFunction, errorFunction) invoke the call backfunction with the data it get from the request
                 * if request failed, it invoke the second function (errorFunctino)
                 */
                .then((result) => {
                    // So if server return any result, it will be here
                    $scope.list = result.data;

                    /**
                     * If the getFenceData function is called for the first time, call the getPulseData function
                     * Otherwise, no need to do that as the getPulseData function calls itself again after 3000 ms
                     * see at the end of that function then you'll see
                     * and because the function will call itself ater 3 seconds, mean it is never stop
                     */
                    if (firstTry) {
                        $timeout(() => {
                            // Get JSON file to load phant url;
                            getPulseData();
                        }, 1000);

                        /**
                         * Check the index in the url
                         * if it is new
                         * mean there's no preselected fence
                         * otherwise choose the right fence in the list
                         * as the active one
                         * defined by the index in the url
                         * the view the fence
                         * by calling $scope.view
                         */
                        if ($state.params.index !== 'new') {
                            $timeout(() => {
                                if ($scope.list[$state.params.index]) {
                                    $scope.view($scope.list[$state.params.index], $state.params.index);
                                } else {
                                    $scope.view($scope.list[0], 0);
                                }
                            }, 1001);
                        }
                    }

                    if (!$scope.fittedBound) {
                        fitBound($state.params.index);
                    }
                }, function (err) {
                    console.log(err);
                });
        }

        function getPulseData() {
            if (!showAllFence() && $state.params.pulseIndex !== 'none') {
                $http.get(window.uploadUrl).then(function (res) {
                    /**
                     * Reduce function is a function from lodash library here
                     * it is also a basic method of ES6 or many other libraries like Rxjs...
                     * you can find document about it everywhere
                     * so here we want to get all the file from the returned api
                     * you can see the window.uploadUrl
                     * in index.html
                     * the server will returned all uploaded files
                     * but somefile maybe duplicated
                     * so this function remove duplicated file
                     * by add them one by one to an empty array
                     * after check that file is not existing in the array already
                     */
                    var data = _.reduce(res.data, function (sum, file) {
                        /**
                         * by this function
                         */
                        if (sum.indexOf(file) === -1) {
                            /**
                             * if it works, then add to the list
                             */
                            sum.push(file);
                        }
                        return sum;
                    }, []);

                    /**
                     * here we have the list, but will filter out all files which are not .json file
                     * then after all, we have a file list ($scope.fileList)
                     */
                    $scope.fileList = _.filter(data, function (file) {
                        return file.indexOf('.json') > -1;
                    });

                    // Get JSON file to load phant url;
                    $scope.data = [];

                    /**
                     * create a promise array, like [a, b, c, d]...
                     * each item in the array is a promise function
                     * they call the server
                     * and get the data back
                     * and the promise only finish when all of them finish
                     * like
                     * a ----- server -----> return result1
                     * b ----------server -----------> return result2
                     * c -- server ---> return result3
                     * d - server-----------------> return result4
                     * 
                     * so
                     * promise ----------------------> return [result1, result2, result3, result 4]
                     * 
                     */
                    var promises = _.map($scope.fileList, function (file, index) {
                        var def = $q.defer();
                        var resolved = false;

                        // Stop trying after 5 seconds
                        /**
                         * We can't use normal get request
                         * because phant url prevented request from different origin
                         * so we can only use jQuery.getJSON here
                         * but jQuery.getJSON never return an error even if request is failed
                         * so if it is not returned anything after 5 sec
                         * we assume that it is failed
                         * and stop the request
                         * by resolving an empty object
                         * so for example
                         * if 3rd request (request c) is failed
                         * promise will return
                         * [result1, result2, {}, result4]
                         * you can increase the limited time if you want
                         * I think this is why sometime you didn't see all the pulse together
                         * just because the request takes longer than 5 sec
                         * then it stop waiting
                         */
                        $timeout(function () {
                            if (!resolved) {
                                def.resolve({});
                            }
                        }, 5000);

                        $.getJSON(window.getFileURL(file), function (streamJSON) {
                            $.getJSON(streamJSON.outputUrl + '#' + Math.floor(Math.random() * 1000000), function (data) {
                                /**
                                 * here if request return data
                                 * of course before 5 sec
                                 * it resolve data
                                 * and set resolved to true
                                 * to prevent the timeout above to replace data with an empty object
                                 */
                                data[0].title = streamJSON.title;
                                def.resolve(data[0]);
                                resolved = true;
                            });
                        });
                        return def.promise;
                    });

                    /**
                     * now is the time to run the promises
                     * above we just defined them
                     * but not running them
                     * no is the time
                     */
                    $q.all(promises).then(function (res) {
                        /**
                         * if data is updated, then continue
                         * otherwise ignore
                         */
                        if (!_.isEqual(res, lastData) || $scope.activated != $scope.lastAcivated) {
                            $scope.lastAcivated = $scope.activated;
                            lastData = res;

                            reloadPulseUI()
                        }
                        $timeout(getPulseData, 3000);
                    });

                });
            } else {
                for (let i = 0; i < $scope.marker.length; i++) {
                    if ($scope.marker[i]) {
                        $scope.marker[i].remove();
                        $scope.marker[i] = undefined;
                    }
                }
                $scope.marker = [];
                $timeout(getPulseData, 3000);
            }
        }

        function reloadPulseUI() {
            if (!lastData) {
                return;
            }
            if (!$state.params.pulseIndex || $state.params.pulseIndex === 'none' ) {
                for (var i = 0; i < lastData.length; i++) {
                    addPulse(i);
                    // boundsLen++;
                    // bounds.extend(new mapboxgl.LngLat(lastData[i].lat, lastData[i].lon));
                }
            } else {
                const index = Number($state.params.pulseIndex);
                addPulse(index);
                map.setCenter([lastData[index].lat, lastData[index].lon])
            }
        }

        function updateArea() {
            /**
             * nothing special here
             * code is comming from internet
             * jsut to calculate the square size of the fence
             * for nothing
             * and of course
             * assing fence.data to save to the db
             */
            var data = $scope.draw.getAll();
            $scope.fence.data = data;
            if (!$scope.fence.data) {
                return;
            }

            var answer = document.getElementById('calculated-area');
            if (data.features.length > 0) {
                var area = turf.area(data);

                // restrict to area to 2 decimal points
                var rounded_area = Math.round(area * 100) / 100;
                $scope.fence.area = rounded_area;
            } else {
                answer.innerHTML = '';
                if (e.type !== 'draw.delete') alert("Use the draw tools to draw a polygon!");
            }
        }

        /**
         * Calculate fence center by its border points
         * https://www.geodatasource.com/developers/javascript
         * @param {} vs 
         */
        function center(vs) {
            let total = {
                x: 0,
                y: 0,
            }
            for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
                total.x += vs[i][0];
                total.y += vs[i][1];
            }
            return [total.x / vs.length, total.y / vs.length];
        }

        function distance(point, vs) {
            if (inside(point, vs)) {
                return '0 Km';
            }
            const cen = center(vs);

            var radlat1 = Math.PI * Number(point[0]) / 180;
            var radlat2 = Math.PI * cen[0] / 180;
            var theta = Number(point[1]) - cen[1]
            var radtheta = Math.PI * theta / 180
            var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            if (dist > 1) {
                dist = 1;
            }
            dist = Math.acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;
            dist = dist * 1.609344; // Km
            // dist = dist * 0.8684 // Miles
            return dist.toFixed(2) + ' Km';
        }

        function inside(point, vs) {
            // ray-casting algorithm based on
            // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
            // https://stackoverflow.com/questions/22521982/js-check-if-point-inside-a-polygon

            var x = Number(point[0]), y = Number(point[1]);

            var inside = false;
            for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
                var xi = vs[i][0], yi = vs[i][1];
                var xj = vs[j][0], yj = vs[j][1];

                var intersect = ((yi > y) != (yj > y))
                    && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
                if (intersect) inside = !inside;
            }

            return inside;
        };

        $scope.view = function (fence, index) {
            for (var i = 0; i < $scope.added.length; i++) {
                if ($scope.added[i]) {
                    $scope.map.removeLayer($scope.added[i]);
                    delete $scope.added[i];
                }
            }
            $scope.active = fence.name;
            $scope.activeIndex = index;
            localStorage.setItem('fence', fence.name);
            localStorage.setItem('activeFence', index);

            $scope.draw.deleteAll();
            $scope.fence = fence;
            reloadPulseUI();

            if (index === 'all') {
                /**
                 * If needed to show all fence
                 */
                for (var i = 0; i < $scope.list.length; i++) {
                    const fenceId = window.btoa(Math.random()).substring(0, 8);
                    $scope.added.push(fenceId);
                    $scope.map.addLayer({
                        id: fenceId,
                        type: 'fill', // "fill", "line", "symbol", "circle", "heatmap", "fill-extrusion", "raster", "hillshade", "background"
                        source: {
                            type: 'geojson',
                            data: $scope.list[i].data,
                        },
                        'paint': {
                            'fill-color': $scope.list[i].color || '#088',
                            'fill-opacity': 0.8
                        }
                    });

                    addFenceName($scope.list[i]);
                }
            } else {
                $scope.added = [window.btoa(Math.random()).substring(0, 8)];
                $scope.map.addLayer({
                    id: $scope.added[0],
                    type: 'fill', // "fill", "line", "symbol", "circle", "heatmap", "fill-extrusion", "raster", "hillshade", "background"
                    source: {
                        type: 'geojson',
                        data: fence.data,
                    },
                    'paint': {
                        'fill-color': fence.color || '#088',
                        'fill-opacity': 0.8
                    }
                });

                addFenceName($scope.fence, true);
            }
            setTimeout(() => {
                console.log('$scope.activeIndex', $scope.activeIndex);
                fitBound($scope.activeIndex);
            }, 300);
        };

        $scope.goto = function (index) {
            /**
             * Go to different page,
             * in fact, same page
             * but different index
             * mean different fence
             */
            $state.go('geo/fence', { index: index, pulseIndex: '' });
        };

        /**
         * Save data
         * then reload
         * basic code
         */
        $scope.save = function () {
            if (!$scope.fence.name) {
                $scope.error = true;
                return;
            }
            if (!$scope.fence._id || $scope.fence._id === 'new') {
                $http.post(window.apiUrl + 'fence', $scope.fence).then(() => {
                    getFenceData();
                    alert('success');
                }, function (err) {
                    console.log(err);
                });
            } else {
                $http.put(window.apiUrl + 'fence/' + $scope.fence._id, $scope.fence).then(() => {
                    alert('success');
                    let i = 0;
                    while ($scope.added[i]) {
                        $scope.map.removeLayer($scope.added[i]);
                        i++;
                    }
                    $scope.added = [];
                    getFenceData();
                }, function (err) {
                    console.log(err);
                });
            }
        };

        /**
         * Add a pulse
         * of course
         */
        function addPulse(i) {
            var lngLat = [lastData[i].lat, lastData[i].lon];

            let color = 'blue';
            /**
             * Set pulse color
             * by class
             * defined in a *.scss file
             * you can try to find '*'
             * not very hard I think
             */
            try {
                if (inside(lngLat, $scope.fence.data.features[0].geometry.coordinates[0])) {
                    color = 'red';
                } else {
                    color = 'green';
                }
            } catch (e) { }

            var el = document.createElement('div');
            el.className = `load ${color}`;
            /**
             * Add inside/outside text here
             */
            let html = `<div class="load-pulse"><div class="load-pulse-container ${color}"></div>`
                + `<div class="load-pulse-title"><p><b>${lastData[i].title}</b></p>`;

            try {
                html += `${distance(lngLat, $scope.fence.data.features[0].geometry.coordinates[0])}</div></div>`;
            } catch (e) {
                html += '</div></div>';
            }

            el.innerHTML = html;

            if ($scope.marker[i]) {
                $scope.marker[i].remove();
                delete $scope.marker[i];
            }

            if (!isNaN(lngLat[0]) && !isNaN(lngLat[1])) {
                $scope.marker[i] = new mapboxgl.Marker(el)
                    .setLngLat(lngLat)
                    .addTo($scope.map);
            }
        }

        /**
         * Add fence name
         */
        function addFenceName(fence, removeAll = false) {
            /**
             * Remove name element if it is existing to prevent duplicated name element
             */
            const fenceId = fence._id + '_' + fence.name;
            if (removeAll) {
                Object.keys($scope.fenceElement).forEach(val => {
                    const item = $scope.fenceElement[val];
                    document.getElementById(item.id).parentElement.removeChild(item);
                });
                $scope.fenceElement = {};
            } else {
                if ($scope.fenceElement[fenceId]) {
                    document.getElementById(fenceId).parentElement.removeChild($scope.fenceElement[fenceId]);
                    delete $scope.fenceElement[fenceId];
                }
            }

            try {
                // Get fence center lat an lon and put the title there
                var lngLat = center(fence.data.features[0].geometry.coordinates[0]);
            } catch (e) {
                return;
            }

            /**
             * Create an element in html
             */
            $scope.fenceElement[fenceId] = document.createElement('div');
            $scope.fenceElement[fenceId].id = fenceId;
            $scope.fenceElement[fenceId].innerHTML = `<div class="fence-title">${fence.name}</div>`;

            new mapboxgl.Marker($scope.fenceElement[fenceId])
                .setLngLat(lngLat)
                .addTo($scope.map);
        }
    }
})(); 
