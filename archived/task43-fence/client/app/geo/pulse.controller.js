(function () {
    'use strict';

    angular.module('app')
        .controller('PulseCtrl', ['$http', '$q', '$state', '$scope', '$timeout', '$mdDialog', 'FileUploader', PulseCtrl])

    function PulseCtrl($http, $q, $state, $scope, $timeout, $mdDialog, FileUploader) {
        let streamJSON;
        $scope.apiUrl = window.apiUrl;
        let recurTime = 10000;

        this.$onInit = () => {
            if (!localStorage.getItem('token')) {
                $state.go('auth');
                return;
            }

            $scope.active = localStorage.getItem('fence');
            $scope.getData();
            $scope.deleting = [];
            $scope.error = [];
            $scope.loading = [];
            $scope.fileList = [];
            $scope.data = [];
            $scope.chartData = [];
            $scope.title = [];
            $scope.fence = [];
            $scope.hash = [];

            /**
             * Get fence list data
             */
            $http.get(window.apiUrl + 'fence')
                /**
                 * Do nothing until data is returned
                 * then(callbackFunction, errorFunction) invoke the call backfunction with the data it get from the request
                 * if request failed, it invoke the second function (errorFunctino)
                 */
                .then((result) => {
                    // So if server return any result, it will be here
                    try {
                        $scope.fence = result.data[localStorage.getItem('activeFence')].data.features[0].geometry.coordinates[0];
                    } catch {
                        $scope.fence = [];
                    }
                });
        };


        $scope.getData = function () {
            $scope.loadingAll = true;
            recuringGetData();
        };

        function randomHash() {
            return Math.floor(Math.random() * 1000000);
        }

        function recuringGetData() {
            console.log('recuringGetData');
            $http.get(window.uploadUrl).then(function (res) {
                $scope.loadingAll = false;
                var data = _.reduce(res.data, function (sum, file) {
                    if (sum.indexOf(file) === -1) {
                        sum.push(file);
                    }
                    return sum;
                }, []);

                $scope.fileList = _.filter(data, function (file) {
                    return file.indexOf('.json') > -1;
                });

                $scope.loading = _.map($scope.fileList, function (_item, index) {
                    return true;
                });

                $scope.hash = _.map($scope.fileList, function () {
                    return randomHash();
                });

                // Get JSON file to load phant url;
                if (!$scope.data) {
                    $scope.data = [];
                }

                $q.all(_.map($scope.fileList, function (file, index) {
                    var defer = $q.defer();
                    // Stop trying after 5 seconds
                    $timeout(function () {
                        $scope.loading[index] = false;
                        if (!$scope.data[index]) {
                            $scope.error[index] = true;
                        } else {
                            $scope.error[index] = false;
                            defer.reject();
                        }
                    }, 5000);


                    $.getJSON(window.getFileURL(file), function (streamJSON) {
                        getStreamData(streamJSON.outputUrl, index, defer);
                        $scope.title[index] = streamJSON.title;
                    });

                    return defer.promise;
                })).then(() => {
                    $timeout(recuringGetData, 3000);
                });
            });
        }


        // Get data from data which is get from json file
        function getStreamData(url, index, defer) {
            // Get data from phant
            $.getJSON(url + '#' + randomHash(), function (data) {
                $scope.data[index] = (data[0]);
                $scope.loading[index] = false;
                $scope.data[index].distance = distance([data[0].lat, data[0].lon], $scope.fence);
                $scope.data[index].location = inside([data[0].lat, data[0].lon], $scope.fence) ? 'Inside' : 'Outside';
                $scope.labels = [];
                $scope.chartData[index] = data.map(function (item) {
                    $scope.labels.push('');
                    return item && Number(item.value_2) || 0;
                });
                $timeout(function () {
                    $scope.loading[index] = false;
                    if (!$scope.data[index]) {
                        $scope.error[index] = true;
                    }
                    defer.resolve(data);
                }, recurTime);
            });
        }

        // Deprecated
        $scope.newPulse = () => {
            $mdDialog.show({
                templateUrl: 'app/geo/add-pulse.html',
                controller: ['$scope', function ($scope) {
                    $scope.data = {};

                    $scope.submit = function () {
                        const url = streamJSON.inputUrl
                            + '?private_key=' + streamJSON.privateKey
                            + '&lat=' + $scope.data.lat
                            + '&lon=' + $scope.data.lon
                            + '&value_1=' + $scope.data.value_1
                            + '&value_2=' + $scope.data.value_2;
                        $http.post(url, {}, function (data) {
                            console.log(data);
                        });
                    };
                }],
            });
        }

        $scope.delete = (index) => {
            $scope.deleting[index] = true;
            $http.delete(window.uploadUrl + '/' + $scope.fileList[index])
                .then(function () {
                    $scope.deleting.splice(index, 1);
                    $scope.error.splice(index, 1);
                    $scope.loading.splice(index, 1);
                    $scope.fileList.splice(index, 1);
                    $scope.data.splice(index, 1);
                })
                .catch(function () {
                    $scope.deleting[index] = false;
                });
        };

        function inside(point, vs) {
            // ray-casting algorithm based on
            // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
            // https://stackoverflow.com/questions/22521982/js-check-if-point-inside-a-polygon

            var x = Number(point[0]), y = Number(point[1]);

            var inside = false;
            for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
                var xi = vs[i][0], yi = vs[i][1];
                var xj = vs[j][0], yj = vs[j][1];

                var intersect = ((yi > y) != (yj > y))
                    && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
                if (intersect) inside = !inside;
            }

            return inside;
        };

        /**
         * Calculate fence center by its border points
         * https://www.geodatasource.com/developers/javascript
         * @param {} vs 
         */
        function center(vs) {
            let total = {
                x: 0,
                y: 0,
            }
            for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
                total.x += vs[i][0];
                total.y += vs[i][1];
            }
            return [total.x / vs.length, total.y / vs.length];
        }

        function distance(point, vs) {
            if (inside(point, vs)) {
                return '0 Km';
            }
            const cen = center(vs);

            var radlat1 = Math.PI * Number(point[0]) / 180;
            var radlat2 = Math.PI * cen[0] / 180;
            var theta = Number(point[1]) - cen[1]
            var radtheta = Math.PI * theta / 180
            var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            if (dist > 1) {
                dist = 1;
            }
            dist = Math.acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;
            dist = dist * 1.609344; // Km
            // dist = dist * 0.8684 // Miles
            return dist + ' Km';
        }

        var uploader = $scope.uploader = new FileUploader({
            url: window.uploadUrl
        });

        // FILTERS

        // a sync filter
        uploader.filters.push({
            name: 'syncFilter',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                console.log('syncFilter');
                return this.queue.length < 10;
            }
        });

        // an async filter
        uploader.filters.push({
            name: 'asyncFilter',
            fn: function (item /*{File|FileLikeObject}*/, options, deferred) {
                console.log('asyncFilter');
                setTimeout(deferred.resolve, 1e3);
            }
        });

        // CALLBACKS
        uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function (fileItem) {
            $scope.uploader.uploadAll();
            console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function (addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function (item) {
            console.info('onBeforeUploadItem', item);
        };
        uploader.onProgressItem = function (fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function (progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function (fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
        };
        uploader.onErrorItem = function (fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function (fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function (fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function () {
            console.info('onCompleteAll');
            $scope.getData();
        };

        console.info('uploader', uploader);


        // Uploader life circle
        // ==============================================================================================================
        $scope.upload = function (index) {
            $mdDialog.show({
                templateUrl: 'app/geo/upload.html',
                controller: 'UploadController',
                controllerAs: 'vm'
            }).then(function (data) {
                if (data) {
                    const url = window.apiUrl + 'image/pulse/' + $scope.fileList[index].split('.json').join('');
                    $http.post(url, { data }, function () {
                        $scope.hash[index] = randomHash();
                    });
                }
            });
        }

        $scope.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: false,
                        position: 'left',
                    },
                ],
            },
        };
    }
})(); 
