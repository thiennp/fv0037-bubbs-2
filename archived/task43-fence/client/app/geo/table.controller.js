(function () {
    'use strict';

    angular.module('app')
        .controller('GeoTableCtrl', ['$scope', '$state', '$http', GeoTableCtrl])

    function GeoTableCtrl($scope, $state, $http) {
        this.$onInit = function () {
            $scope.showValue3 = $state.params.id === 3;
            $scope.active = localStorage.getItem('fence');
            getData();
        };

        // Get data from data which is get from json file
        function getData() {
            $http.get(window.apiUrl + 'fence').then((result) => {
                $scope.data = result.data;
                setTimeout(getData, 5000);
            }, function (err) {
                console.log(err);
            });
        }

        $scope.showGeo = function (index) {
            $state.go('geo/fence', { index: index, pulseIndex: 'none' });
        };

        /**
         * Remove fence by index of it in the data list, no magic here
         */
        $scope.remove = function (index) {
            confirm = window.confirm('Are you sure?');
            if (confirm) {
                $http.delete(window.apiUrl + 'fence/' + $scope.data[index]._id).then(() => {
                    $scope.data = _.filter($scope.data, item => item._id !== $scope.$scope.data[index]._id);
                    alert('success');
                }, function (err) {
                    console.log(err);
                });
            }
        };
    }
})();
