(function () {
    'use strict';

    angular.module('app')
        .controller('PulseTableCtrl', ['$state', '$timeout', PulseTableCtrl])

    function PulseTableCtrl($state, $timeout) {
        var vm = this;

        this.$onInit = function () {
            vm.columns = [];
            $.getJSON(window.getFileURL($state.params.id), function (data) {
                vm.streamJSON = data;
                getData();
            });
        };

        // Get data from data which is get from json file
        function getData() {
            // Get data from phant
            $.getJSON(vm.streamJSON.outputUrl + '#' + Math.floor(Math.random() * 1000000), function (data) {
                if (!_.isEqual(data, vm.data)) {
                    vm.data = data;
                    vm.columns = _.reduce(vm.data, (sum, val) => {
                        _.each(val, (_item, key) => {
                            if (sum.indexOf(key) === -1) {
                                sum.push(key);
                            }
                        });
                        return sum;
                    }, []);
                }
                $timeout(getData, 1000);
            });
        }
    }
})();
