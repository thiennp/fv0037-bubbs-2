(function () {
    'use strict';

    angular.module('app')
        .controller('ProfileCtrl', ['$http', '$scope', 'FileUploader', ProfileCtrl])

    function ProfileCtrl($http, $scope, FileUploader) {
        this.$onInit = () => {
            if (!localStorage.getItem('token')) {
                $state.go('auth');
                return;
            }

            $scope.profile = {};
            $scope.getData();

            $scope.defaultAvatars = [
                'https://www.w3schools.com/w3images/avatar1.png',
                'https://www.w3schools.com/w3images/avatar2.png',
                'https://www.w3schools.com/w3images/avatar3.png',
                'https://www.w3schools.com/w3images/avatar4.png',
                'https://www.w3schools.com/w3images/avatar5.png',
                'https://www.w3schools.com/w3images/avatar6.png',
            ];
        };

        $scope.edit = {
            bio: false,
        };

        $scope.getData = function () {
            $scope.profile.bio = localStorage.getItem('user_bio');
            $scope.profile.email = localStorage.getItem('user_email');
            $scope.profile.image = localStorage.getItem('user_image');
            $scope.profile.username = localStorage.getItem('user_username');
        };

        $scope.save = function () {
            $http.put(window.apiUrl + 'profile', $scope.profile).then(function (result) {
                alert('success');
                $scope.edit.bio = false;
                if (!!result.data.name) {
                    localStorage.setItem('user_name', result.data.name);
                }
                if (!!result.data.username) {
                    localStorage.setItem('user_username', result.data.username);
                }
                if (!!result.data.email) {
                    localStorage.setItem('user_email', result.data.email);
                }
                if (!!result.data.image) {
                    localStorage.setItem('user_image', result.data.image);
                }
                if (!!result.data.bio) {
                    localStorage.setItem('user_bio', result.data.bio);
                }
            }, function (err) {
                $scope.err = err;
            });
        };

        // Uploader life circle
        // ==============================================================================================================
        var uploader = $scope.uploader = new FileUploader({
            url: window.uploadUrl
        });

        // FILTERS

        // a sync filter
        uploader.filters.push({
            name: 'syncFilter',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                console.log('syncFilter');
                return this.queue.length < 10;
            }
        });

        // an async filter
        uploader.filters.push({
            name: 'asyncFilter',
            fn: function (item /*{File|FileLikeObject}*/, options, deferred) {
                console.log('asyncFilter');
                setTimeout(deferred.resolve, 1e3);
            }
        });

        // CALLBACKS
        uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function (fileItem) {
            $scope.uploader.uploadAll();
            $scope.profile.image = fileItem.file.name;
            $scope.edit.active = false;
            console.info('onAfterAddingFile', fileItem.file.name);
        };
        uploader.onAfterAddingAll = function (addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function (item) {
            console.info('onBeforeUploadItem', item);
        };
        uploader.onProgressItem = function (fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function (progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function (fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
        };
        uploader.onErrorItem = function (fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function (fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function (fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function () {
            console.info('onCompleteAll');
        };

        console.info('uploader', uploader);

    }
})(); 
