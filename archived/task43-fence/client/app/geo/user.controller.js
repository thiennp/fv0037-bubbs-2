(function () {
    'use strict';

    angular.module('app')
        .controller('UserCtrl', ['$http', '$state', '$scope', UserCtrl]);

    function UserCtrl($http, $state, $scope) {
        this.$onInit = () => {
            /**
             * If token is not stored in localStorage
             * mean user hasn't logged in
             * then go to auth page
             */
            if (!localStorage.getItem('token')) {
                $state.go('auth');
                return;
            }

            $scope.getUsers();
        };

        $scope.getUsers = function () {
            /**
             * Get data from http
             */
            $http.get(window.apiUrl + 'users')
                .then((result) => {
                    $scope.list = result.data;
                }, function (err) {
                    $state.go('auth');
                    console.log(err);
                });
        }

        $scope.updateUser = function (user, param, value) {
            user[param] = value;
            $http.put(window.apiUrl + 'user/' + user._id, user).then(function () {
                $scope.getUsers();
            });
        }
    }
})();
