(function () {
    'use strict';

    angular.module('app')
        .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$ocLazyLoadProvider',
            function ($stateProvider, $urlRouterProvider, $httpProvider, $ocLazyLoadProvider) {
                var routes, setRoutes;

                routes = [
                    'ui/cards', 'ui/typography', 'ui/buttons', 'ui/icons', 'ui/grids', 'ui/widgets', 'ui/components', 'ui/timeline', 'ui/lists', 'ui/pricing-tables',
                    'table/static', 'table/responsive', 'table/data',
                    'form/elements', 'form/layouts', 'form/validation',
                    'chart/echarts', 'chart/echarts-line', 'chart/echarts-bar', 'chart/echarts-pie', 'chart/echarts-scatter', 'chart/echarts-more',
                    'page/404', 'page/500', 'page/blank', 'page/forgot-password', 'page/invoice', 'page/lock-screen', 'page/profile', 'page/signin', 'page/signup', 'page/login',
                    'app/calendar', 'page/geo_fence_main'
                ];

                setRoutes = function (route) {
                    var config, url;
                    url = '/' + route;
                    config = {
                        url: url,
                        templateUrl: 'app/' + route + '.html'
                    };
                    $stateProvider.state(route, config);
                    return $stateProvider;
                };

                routes.forEach(function (route) {
                    return setRoutes(route);
                });

                $stateProvider
                    .state('dashboard', {
                        url: '/dashboard',
                        templateUrl: 'app/dashboard/dashboard.html',
                        controller: 'DashboardCtrl',
                    })
                    .state('auth', {
                        url: '/auth',
                        templateUrl: 'app/auth/auth.html',
                        controller: 'AuthCtrl',
                        controllerAs: '$ctrl',
                    })
                    .state('geo/fence', {
                        url: '/geo/fence/:index/:pulseIndex',
                        templateUrl: 'app/geo/fence.html',
                        controller: 'FenceCtrl',
                        resolve: {
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'fence',
                                ]);
                            }]
                        }
                    })
                    .state('users', {
                        url: '/users',
                        templateUrl: 'app/geo/user.html',
                        controller: 'UserCtrl',
                        controllerAs: 'vm',
                    })
                    .state('geo/pulse', {
                        url: '/geo/pulse',
                        templateUrl: 'app/geo/pulse.html',
                        controller: 'PulseCtrl',
                        resolve: {
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'geo',
                                ]);
                            }]
                        }
                    })
                    .state('geo/table', {
                        url: '/geo/table',
                        templateUrl: 'app/geo/table.html',
                        controller: 'GeoTableCtrl',
                        controllerAs: 'vm',
                        resolve: {
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'geo',
                                ]);
                            }]
                        }
                    })
                    .state('geo/pulsetable', {
                        url: '/geo/pulsetable/:id',
                        templateUrl: 'app/geo/pulsetable.html',
                        controller: 'PulseTableCtrl',
                        controllerAs: 'vm',
                        resolve: {
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'geo',
                                ]);
                            }]
                        }
                    })
                    .state('profile', {
                        url: '/profile',
                        templateUrl: 'app/geo/profile.html',
                        controller: 'ProfileCtrl',
                        controllerAs: 'vm'
                    })
                    .state('form/editor', {
                        url: '/form/editor',
                        templateUrl: 'app/form/editor.html',
                        resolve: {
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'textAngular'
                                ]);
                            }]
                        }
                    })
                    .state('form/wizard', {
                        url: '/form/wizard',
                        templateUrl: 'app/form/wizard.html',
                        resolve: {
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'angular-wizard'
                                ]);
                            }]
                        }
                    })
                    .state('map/maps', {
                        url: '/map/maps',
                        templateUrl: 'app/map/maps.html',
                        resolve: {
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    'googlemap',
                                ]);
                            }]
                        }
                    });

                $urlRouterProvider
                    .when('/', '/dashboard')
                    .otherwise('/auth');

                $httpProvider.interceptors.push(function ($q) {
                    return {
                        'request': function (config) {
                            config.headers['x-access-token'] = localStorage.getItem('token');
                            config.headers['x-access-id'] = localStorage.getItem('id');
                            return config;
                        }
                    };
                });
            }
        ])
        .run(['$rootScope', function ($rootScope) {
            $rootScope.getProfileImage = function (image) {
                if (!image) {
                    image = localStorage.getItem('user_image') || 'https://www.w3schools.com/w3images/avatar1.png';
                }
                if (image.indexOf('http') !== 0) {
                    return window.getFileURL(image);
                } else {
                    return image;
                }
            };

            $rootScope.getUsername = function () {
                return localStorage.getItem('user_username') || '';
            };
        }]);

})();
