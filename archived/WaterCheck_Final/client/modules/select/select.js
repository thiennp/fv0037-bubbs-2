/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App')
        .controller('SelectController', SelectController);

    // Select apps
    function SelectController($http, $rootScope, $state) {
        var $ctrl = this;
        $ctrl.app = '';

        function init() {
            $http.get(window.authUrl + 'app').then(function (result) {
                $ctrl.list = result.data;
                if ($state.params.next !== 'register' || !$ctrl.list.length) {
                    $ctrl.showMaster = true;
                }
            });
        }

        $ctrl.onSelect = function () {
            const appId = $ctrl.appId || ''
            window.clientAuthUrl = window.authUrl + appId + '/';
            switch ($state.params.next) {
                case 'main':
                    $state.go('app.main', { appId: appId });
                    break;
                case 'register':
                    $state.go('app.register', { appId: appId });
                    break;
            }
        };

        init();
    }
})();
