/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App')
        .controller('AdminController', AdminController);

    // Manage apps
    function AdminController($http, $rootScope, $state, $timeout, $window) {
        var $ctrl = this;
        $ctrl.app = '';
        $ctrl.edit = {};

        function init() {
            delete $ctrl.edit.item;
            $http.get(window.authUrl + 'app').then(function (result) {
                $ctrl.list = result.data;
            });
        }

        $ctrl.onEdit = function (item, index) {
            $ctrl.edit.item = item;
            $timeout(function () {
                const input = $window.document.getElementById('editing-item');
                if (input) {
                    input.focus();
                }
            }, 100);
        };

        $ctrl.onDelete = function (item) {
            if (window.confirm('Are you sure you want to delete? ' + item.name)) {
                $http.delete(window.apiUrl + 'app/' + item._id)
                    .then(init);
            }
        };

        $ctrl.onCreate = function () {
            var newItem = {
                name: ''
            };
            const index = $ctrl.list.length;
            $ctrl.list.push(newItem);
            $ctrl.onEdit(newItem, index);
        };

        $ctrl.onSave = function (item) {
            if (item._id) {
                $http.put(window.apiUrl + 'app/' + item._id, item)
                    .then(function (result) {
                        if (result.data.ok) {
                            $ctrl.edit.success = item._id;
                            init();
                        } else {
                            delete $ctrl.edit.success;
                            $ctrl.edit.error = result.data.errmsg;
                        }
                    });
            } else {
                $http.post(window.apiUrl + 'app', item)
                    .then(function (result) {
                        if (result.data._id) {
                            $ctrl.edit.success = result._id;
                            init();
                        } else {
                            delete $ctrl.edit.success;
                            $ctrl.edit.error = result.data.errmsg;
                        }
                    });
            }
        };

        init();
    }
})();

