/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App')
        .controller('MarkersController', MarkersController);

    // For global map page
    function MarkersController($http, $rootScope, $state, $uibModal) {
        var vm = this;

        vm.statuses = [
            '',
            'good',
            'bad',
            'ugly',
            'planned',
            'route'
        ];

        vm.filters = {
            'good': true,
            'bad': true,
            'ugly': true,
            'planned': true,
            'route': true
        };

        if (localStorage.getItem('filters')) {
            try {
                vm.filters = JSON.parse(localStorage.getItem('filters'));
            } catch (e) {}
        }

        $rootScope.$on('filter markers', function (evt, filters) {
            vm.filters = filters;
            localStorage.setItem('filters', JSON.stringify(vm.filters));
        });

        vm.filterMarker = function () {
            $rootScope.$broadcast('filter markers', vm.filters);
        };

        getData();

        function getData() {
            $http.get($rootScope.clientApiUrl('marker')).then(function (result) {
                vm.list = result.data;
                $rootScope.$broadcast('map refresh');
            }, function () {
                localStorage.removeItem('token');
                $state.go('login');
            });
        }

        vm.add = function () {
            vm.list.push({
                'lat': -30.5094156,
                'lng': 80.0793512,
                'description': '',
                'markerId': '',
                'status': 1,
                'route': 0,
                'routeName': '',
                'image': '',
                'history': []
            });
            vm.edit(vm.list.length - 1);
        };

        vm.delete = function (index, historyIndex) {
            if (confirm('do you want to delete?')) {
                if (!historyIndex) {
                    $http.delete($rootScope.clientApiUrl('marker/' + vm.list[index]._id)).then(function (result) {
                        vm.list.splice(index, 1);
                        $rootScope.$broadcast('map refresh');
                    });
                } else {
                    vm.list[index].history.splice(historyIndex - 1, 1);
                    vm.edit(index);
                    vm.save(true);
                }
            }
        };

        vm.addHistory = function (index) {
            if (!vm.list[index].history.length) {
                vm.list[index].history = [{
                    'markerId': vm.list[index]._id,
                    'description': vm.list[index].description,
                    'status': vm.list[index].status,
                    'route': vm.list[index].route,
                    'routeName': vm.list[index].routeName,
                    'image': vm.list[index].image
                }];
            }
            vm.list[index].history.unshift({
                'markerId': vm.list[index]._id,
                'description': '',
                'status': 1,
                'route': 1,
                'routeName': '',
                'image': ''
            });
            vm.edit(index, 1);
        };

        vm.edit = function (index, historyIndex) {
            vm.editingIndex = index + 1;
            vm.historyIndex = historyIndex;
            vm.editingItem = {
                'markerId': vm.list[index].markerId,
                'description': vm.list[index].description,
                'status': String(vm.list[index].status),
                'route': 1,
                'routeName': vm.list[index].routeName,
                'lat': vm.list[index].lat,
                'lng': vm.list[index].lng,
                'history': vm.list[index].history,
                'image': vm.list[index].image,
            };
            if (vm.historyIndex) {
                vm.editingHistoryItem = {
                    'description': vm.editingItem.history[vm.historyIndex - 1].description,
                    'status': String(vm.editingItem.history[vm.historyIndex - 1].status),
                    'route': String(vm.editingItem.history[vm.historyIndex - 1].route),
                    'routeName': vm.editingItem.history[vm.historyIndex - 1].routeName,
                    'image': vm.editingItem.history[vm.historyIndex - 1].image,
                    'create': vm.editingItem.history[vm.historyIndex - 1].create,
                    'edit': vm.editingItem.history[vm.historyIndex - 1].edit
                };
            }
        };

        vm.save = function (noAlert) {
            if (noAlert || confirm('do you want to save changes?')) {
                if (!vm.historyIndex) {
                    vm.list[vm.editingIndex - 1].markerId = vm.editingItem.markerId;
                    vm.list[vm.editingIndex - 1].description = vm.editingItem.description;
                    vm.list[vm.editingIndex - 1].status = Number(vm.editingItem.status);
                    vm.list[vm.editingIndex - 1].route = Number(vm.editingItem.route);
                    vm.list[vm.editingIndex - 1].routeName = vm.editingItem.routeName;
                    vm.list[vm.editingIndex - 1].lat = vm.editingItem.lat;
                    vm.list[vm.editingIndex - 1].lng = vm.editingItem.lng;
                    if (vm.editingItem.uploadImage) {
                        vm.editingItem.image = vm.editingItem.uploadImage;
                        vm.list[vm.editingIndex - 1].uploadImage = vm.editingItem.uploadImage;
                        vm.list[vm.editingIndex - 1].image = vm.editingItem.uploadImage;
                    }
                } else {
                    vm.list[vm.editingIndex - 1].history[vm.historyIndex - 1].description = vm.editingHistoryItem.description;
                    vm.list[vm.editingIndex - 1].history[vm.historyIndex - 1].status = Number(vm.editingHistoryItem.status);
                    vm.list[vm.editingIndex - 1].history[vm.historyIndex - 1].route = Number(vm.editingHistoryItem.route);
                    vm.list[vm.editingIndex - 1].history[vm.historyIndex - 1].routeName = vm.editingHistoryItem.routeName;
                    if (vm.editingHistoryItem.uploadImage) {
                        vm.editingHistoryItem.image = vm.editingHistoryItem.uploadImage;
                        vm.list[vm.editingIndex - 1].history[vm.historyIndex - 1].uploadImage = vm.editingHistoryItem.uploadImage;
                        vm.list[vm.editingIndex - 1].history[vm.historyIndex - 1].image = vm.editingHistoryItem.uploadImage;
                    }
                }

                let def;
                if (vm.list[vm.editingIndex - 1]._id) {
                    def = $http.put($rootScope.clientApiUrl('marker/' + vm.list[vm.editingIndex - 1]._id), vm.list[vm.editingIndex - 1]);
                } else {
                    def = $http.post($rootScope.clientApiUrl('marker'), vm.list[vm.editingIndex - 1]);
                }
                def.then(function (result) {
                    vm.cancel();
                    getData();
                    $rootScope.$broadcast('map refresh');
                });
            }
        };

        vm.cancel = function () {
            if (!vm.list[vm.editingIndex - 1] || !vm.list[vm.editingIndex - 1]._id) {
                vm.list.splice(vm.editingIndex - 1, 1);
            } else {
                delete vm.list[vm.editingIndex - 1].uploadImage;
                if (vm.list[vm.editingIndex - 1].history) {
                    angular.forEach(vm.list[vm.editingIndex - 1].history, function (item, index) {
                        delete vm.list[vm.editingIndex - 1].history[index].uploadImage;
                    });
                }
            }
            delete vm.editingIndex;
            delete vm.historyIndex;
            delete vm.editingItem;
            delete vm.editingHistoryItem;
        };

        vm.upload = function (index, historyIndex) {
            var modalInstance = $uibModal.open({
                templateUrl: './modules/upload/upload.html',
                controller: 'UploadController',
                controllerAs: 'vm'
            });

            modalInstance.result.then(function (data) {
                if (data) {
                    if (historyIndex) {
                        vm.list[index].history[historyIndex - 1].uploadImage = data;
                    } else {
                        vm.list[index].uploadImage = data;
                        vm.editingItem.uploadImage = data;
                    }
                }
            });
        }
    }
})();
