App
    .controller('HeaderController', [
        '$http',
        '$rootScope',
        '$scope',
        function ($http, $rootScope, $scope) {
            $scope.clearData = function (callback) {
                var url = $rootScope.server.inputUrl + '/clear?private_key=' + $rootScope.server.privateKey;
                $http.get(url, function () {
                    removeMarkers();
                });
            };

            $scope.toggleHistory = function () {
                $rootScope.hideHistory = !$rootScope.hideHistory;
            };

            $scope.toggleOnAddingNewMarker = function () {
                $scope.onAddingNewMarker = !$scope.onAddingNewMarker;
                window.onAddingNewMarker = $scope.onAddingNewMarker;
            };
        }
    ]);