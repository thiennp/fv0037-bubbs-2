const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const md5 = require('md5');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const nodemailer = require('nodemailer');

app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ limit: '5mb' }));

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/dist'));
app.use(express.static(__dirname + '/upload'));

mongoose.connect('mongodb://localhost/fv0037-42');

var Schema = mongoose.Schema;

var UserModel = new Schema({
    email: {
        unique: true,
        type: String
    },
    username: {
        unique: true,
        type: String
    },
    password: String,
    company: String,
    create: Date,
    edit: Date,
    image: String,
    name: String,
    bio: String,
    verificationCode: String,
    passwordCode: String,
});

var User = mongoose.model('UserModel', UserModel);

function log(title, text) {
    console.log('');
    console.log('');
    console.log('');
    console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
    console.log(title);
    if (text) {
        console.log(text);
    }
    console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
    console.log('');
    console.log('');
}

function sendMail(email, text) {
    // TODO: Update email account here
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'shoresafety111@gmail.com',
            pass: 'shoresafety111ForTesting', // Update your email account here
        }
    });

    var mailOptions = {
        from: 'test@gmail.com',
        to: email,
        subject: 'Registration',
        text: text,
    };

    log(`To ${email}:`, text);

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}

var apiRoutes = express.Router();
// route to authenticate a user (POST http://localhost:8080/api/authenticate)

// route middleware to verify a token
apiRoutes.use(function (req, res, next) {
    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    // decode token
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, 'secret', function (err, decoded) {
            if (err) {
                return res.send(401, { success: false, message: 'Failed to authenticate token.' });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });
    } else {
        // if there is no token
        // return an error
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    }
});

app.use('/api', apiRoutes);

app.get('/', function (req, res) {
    res.sendFile('client/index.html', {
        root: __dirname
    });
});

app.post('/auth/register', function (req, res) {
    var User = mongoose.model('UserModel', UserModel);
    User.findOne({
        email: req.body.email,
    }, function (err, user) {
        if (err) throw err;
        if (!user) {
            User.findOne({
                username: req.body.username,
            }, function (err, user) {
                if (!user) {
                    req.body.create = new Date();
                    req.body.password = md5(req.body.password);
                    req.body.company = req.body.company;
                    req.body.verificationCode = md5(Math.random());
                    var user = new User(req.body);
                    log('Registering', user);
                    user.save(function (err, result) {
                        if (!err) {
                            log('Registered', result);
                            const mailContent = req.headers.host + '/#!/verify/' + req.body.verificationCode;
                            sendMail(user.email, mailContent)
                            res.send(result);
                        } else {
                            res.send(err);
                        }
                    });
                } else {
                    res.send(401, { success: false, message: 'Username is already exist' });
                }
            });
        } else {
            res.send(401, { success: false, message: 'Email is already exist' });
        }
    });
});

app.post('/auth/password', function (req, res) {
    var User = mongoose.model('UserModel', UserModel);
    log('Password code', req.body.passwordCode);
    User.findOne({
        passwordCode: req.body.passwordCode,
    }, function (err, user) {
        if (err) throw err;
        if (user) {
            log('user', user);
            user.password = md5(req.body.password);
            user.save(function (err, result) {
                if (!err) {
                    log(`Password updated to: ${user.email}`);
                    res.send(result);
                } else {
                    res.send(err);
                }
            });
        } else {
            res.send(401, { success: false, message: 'passwordCode us not correct' });
        }
    });
});

app.post('/auth/forgot', function (req, res) {
    var User = mongoose.model('UserModel', UserModel);
    User.findOne({
        email: req.body.email,
    }, function (err, user) {
        if (err) throw err;
        if (user) {
            user.passwordCode = md5(Math.random());
            user.save(function (err, result) {
                if (!err) {
                    const mailContent = req.headers.host + '/#!/password/' + user.passwordCode;
                    sendMail(user.email, mailContent)
                    res.send(result);
                } else {
                    res.send(err);
                }
            });
        } else {
            res.send(401, { success: false, message: 'Email doens\'t exist' });
        }
    });
});

app.get('/auth/verification/:verificationCode', function (req, res) {
    var User = mongoose.model('UserModel', UserModel);
    User.findOne({
        verificationCode: req.params.verificationCode,
    }, function (err, user) {
        if (err) throw err;
        if (user) {
            user.verificationCode = null;
            user.save(function (err, result) {
                if (!err) {
                    res.send(result);
                } else {
                    res.send(err);
                }
            });
        } else {
            res.send(401, { success: false, message: 'Can not find user' });
        }
    });
});

app.post('/auth/login', function (req, res) {
    User.findOne({
        username: req.body.username,
        password: md5(req.body.password),
    }, function (err, user) {
        if (err) throw err;

        if (!user) {
            res.send(401, { success: false, message: 'Authentication failed. Please check your email or password.' });
        } else if (user) {
            const payload = {};

            var token = jwt.sign({}, 'secret');

            // return the information including token as JSON
            res.send({
                success: true,
                token: token,
                id: user._id,
                email: user.email,
                name: user.name,
                image: user.image,
                username: user.username,
            });
        }
    });
});

app.get('/api/profile', function (req, res) {
    const userId = req.headers['x-access-id'];
    User.findOne(
        {
            _id: userId,
        }, {
            'email': true,
            'image': true,
            'username': true,
            'name': true,
            'bio': true
        }, function (err, user) {
            if (!err) {
                res.send(user);
            } else {
                res.send(err);
            }
        });
});

app.put('/api/profile', function (req, res) {
    const userId = req.headers['x-access-id'];
    if (req.body.uploadImage) {
        var base64Data = req.body.uploadImage.replace(/^data:image\/png;base64,/, '');
        var filename = 'upload_' + Math.floor(Math.random() * 1000000000) + '.png';
        require('fs').writeFile('upload/' + filename, base64Data, 'base64', function (err) {
            console.log(err);
        });
        req.body.image = filename;
        delete req.body.uploadImage;
    }
    req.body.edit = new Date();

    User
        .update({
            _id: userId
        }, {
                $set: req.body
            }, function (err, result) {
                if (!err) {
                    User.findOne(
                        {
                            _id: userId
                        }, {
                            'email': true,
                            'image': true,
                            'name': true,
                            'bio': true
                        }, function (err, user) {
                            if (!err) {
                                res.send(user);
                            } else {
                                res.send(err);
                            }
                        });
                } else {
                    res.send(err);
                }
            });
});

app.listen(8080, function () {
    console.log('Example app listening on port 8080!')
});
