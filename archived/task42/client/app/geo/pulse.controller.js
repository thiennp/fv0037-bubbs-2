(function () {
    'use strict';

    angular.module('app')
        .controller('PulseCtrl', ['$scope', '$state', '$timeout', PulseCtrl])

    function PulseCtrl($scope, $state, $timeout) {
        // Init the map
        if (!localStorage.getItem('token')) {
            $state.go('auth');
            return;
        }

        L.mapbox.accessToken = window.mapboxGLToken;
        var map = L.mapbox.map('map', 'mapbox.light');
        var myLayer = L.mapbox.featureLayer().addTo(map);
        var geojson = [];
        var streamJSON;
        var lastData;

        /**
         * streamId will store the id of the stream. So in any case you need to definde which stream is this, use it
         * like (if streamId === 1) { -- do anything you want with stream 1 --- }
         */
        var streamId = Number($state.params.id);

        /**
         * So if streamId is 3, this is the face marker stream
         */
        var isFaceMarker = streamId === 3;

        this.$onInit = function() {
            window.setView = false;
            $scope.limit = 100;
            $scope.lastLimit = 100;
            window.scopeId = Math.random();

            /**
             * Get JSON file to load phant url;
             * Now we use streamId to load the right stream by its id
             */
            $.getJSON('../dataStreams/live_data/data_stream_' + streamId + '.json', function (data) {
                streamJSON = data;
                getData(window.scopeId);
            });

            myLayer.on('layeradd', function (e) {
                var marker = e.layer,
                    feature = marker.feature;
                if (marker.setIcon) {
                    marker.setIcon(L.divIcon(feature.properties.icon));
                }
            });

            myLayer.setGeoJSON(geojson);

            map.scrollWheelZoom.disable();
        };

        function mainMarkerHTML(res, i) {
            var loadHtml = '';
            /**
             * Here say !isFaceMarker, or if it's not a face marker
             */
            if (!isFaceMarker) {
                /**
                 * The speed class will be based on the value of the speed
                 * The speedClass will define the color of the pulse
                 * the relationships of the color and the class are defined in the style css file
                 */
                var speed = res.speed;
                var speedClass = '';
                if (speed < 1) {
                    speedClass = ' white';
                } else if (speed < 16) {
                    speedClass = ' green';
                } else if (speed < 31) {
                    speedClass = ' dark-green';
                } else if (speed < 46) {
                    speedClass = ' blue';
                } else if (speed < 61) {
                    speedClass = ' dark-blue';
                } else if (speed < 71) {
                    speedClass = ' yellow';
                } else if (speed < 81) {
                    speedClass = ' dark-yellow';
                } else if (speed < 86) {
                    speedClass = ' purple';
                } else if (speed < 91) {
                    speedClass = ' dark-purple';
                } else if (speed < 100) {
                    speedClass = ' red';
                } else {
                    speedClass = ' dark-red';
                }
                /**
                 * if !i, or if i === 0, or if this is the first item in the list
                 * THIS IS MAIN MARKER
                 */
                if (!i) {
                    /**
                     * Here you can check the streamId
                     */
                    var value1, value2;
                    if (streamId === 1) {
                        value1 = res.value_1;
                        value2 = res.value_2;
                    }
                    if (streamId === 2) {
                        value1 = res.value_1;
                        value2 = res.value_2;
                    }
                    loadHtml = '<div class="load' + speedClass + '">'
                        + '     <div class="load-tooltip">'
                        + '         <div><strong>Value 1</strong>: ' + value1 + '</div>'
                        + '         <div><strong>Value 2</strong>: ' + value2 + '</div>'
                        + '         <div><strong>Time stamp</strong>: ' + moment(res.timestamp).format('LL LT') + '</div>'
                        + '     </div>'
                        + ' <div class="load-pulse"><div class="load-pulse-container' + speedClass + '"></div></div></div>';

                /**
                 * Here, is for i > 0, mean all the other items of the list, not other stream
                 * THESE ARE HISTORY MARKERS
                 */
                } else {
                    /**
                     * Here you can check the streamId
                     */
                    var value1, value2;
                    if (streamId === 1) {
                        value1 = res.value_1;
                        value2 = res.value_2;
                    }
                    if (streamId === 2) {
                        value1 = res.value_1;
                        value2 = res.value_2;
                    }
                    loadHtml = '<div class="load' + speedClass + '">'
                        + '     <div class="load-tooltip">'
                        + '         <div><strong>Value 1</strong>: ' + value1 + '</div>'
                        + '         <div><strong>Value 2</strong>: ' + value2 + '</div>'
                        + '         <div><strong>Time stamp</strong>: ' + moment(res.timestamp).format('LL LT') + '</div>'
                        + '     </div>'
                        + ' </div>';
                }
            /**
             * So below is the face marker
             */
            } else {
                var status = String(res.value_1);
                var hideTooltip = false;
                var statusClass;
                switch (status) {
                    case 'HRAA':
                        statusClass = ' good';
                        break;
                    case 'AcE=':
                        statusClass = ' bad';
                        break;
                    default:
                        statusClass = ' ugly';
                        /**
                         * The hideTooltip is used to define if status is not HRAA or AcE=
                         */
                        hideTooltip = true;
                        break;
                }
                /**
                 * Only show if hideTooltip is false, meaning status MUST BE 'HRAA" or 'AcE=' to be shown
                 */
                if (!hideTooltip) {
                    loadHtml = '<div class="load status' + statusClass + '">'
                        + '         <div class="load-tooltip">'
                        + '             <div><strong>Value 1</strong>: ' + res.value_1 + '</div>'
                        + '             <div><strong>Value 2</strong>: ' + res.value_2 + '</div>'
                        + '             <div><strong>Time stamp</strong>: ' + moment(res.timestamp).format('LL LT') + '</div>'
                        + '         </div>'
                        + '     </div>';
                }
            }
            return loadHtml;
        }

        // Get data from data which is get from json file
        function getData(id) {
            if (id === window.scopeId) {
                // Get data from phant
                $.getJSON(streamJSON.outputUrl + '#' + Math.floor(Math.random() * 1000000), function (data) {
                    data = _.map(data, function (item) {
                        /**
                         * This check, if item.lat is undefined, it try to get the value from item.latitude
                         * For example, item.lat = undefined, item.latitude = 1
                         * this will be item.lat = undefined || 1
                         * => item.lat = 1
                         * 
                         * In the other case, if item.lat = 1, item.latitude = undefined
                         * so item.lat = 1 || undefined
                         * => item.lat = 1;
                         * so in both case it will be 1
                         * 
                         * Same for lon and longitude
                         */
                        item.lat = item.lat || item.latitude;
                        item.lon = item.lon || item.longitude;
                        return item;
                    });

                    /**
                     * Check if data is different than lastData, or limit is different than lastLimit
                     * if no changed, then ignore
                     * if yes, then update the map
                     */
                    if (!_.isEqual(data, lastData) || $scope.limit !== $scope.lastLimit) {
                        geojson = [];
                        lastData = data;
                        $scope.lastLimit = $scope.limit;

                        /**
                         * If no item in the list, mean lastData[0] is not existing, use default value 57.709... and 11.952...
                         */
                        var lat = (lastData[0] && (lastData[0].lat || lastData[0].latitude)) || 57.709615;
                        var lon = (lastData[0] && (lastData[0].lon || lastData[0].longitude)) || 11.952478;

                        /**
                         * Show all pulses
                         * but only if the pulse index is smaller than limit
                         * so you see i < lastData.length
                         * and i < $scope.limit
                         */
                        for (var i = 0; i < lastData.length && i < $scope.limit; i++) {
                            /**
                             * This condition is to check
                             * if !isFaceMarker => not face marker, then continue
                             * Otherwise, if i === 0, or if this is the first item - and for face marker, then continue
                             * If not, mean this is face stream, and not the first item, then ignore
                             */
                            if (!isFaceMarker || i === 0) {
                                /**
                                 * Get html from mainMarkerHTML function
                                 */
                                var html = mainMarkerHTML(lastData[i], i);
                                var lastDataLat = (lastData[i].lat || lastData[i].latitude);
                                var lastDataLon = (lastData[i].lon || lastData[i].longitude);
                                if (html) {
                                    // Add data to geojson
                                    geojson.push({
                                        type: 'Feature',
                                        geometry: {
                                            type: 'Point',
                                            coordinates: [lastDataLon, lastDataLat]
                                        },
                                        properties: {
                                            icon: {
                                                html: html // add content inside the marker
                                            }
                                        }
                                    });
                                }
                            }
                        }

                        // Apply marker data to the map
                        myLayer.setGeoJSON(geojson);

                        /**
                         * This the view is never set before
                         * then set the view
                         * otherwise, ignore
                         * This condition is to ensure that the view is not reloaded
                         */
                        if (!window.setView) {
                            window.setView = true;
                            map.setView([lat, lon], 15);
                        }
                    }

                    /**
                     * After finish loading data and apply markers to the map
                     * wait for 3 seconds (3000 ms)
                     * then call the function to get and check the data again
                     * So the map will be updated if data is changed
                     * after 3 seconds
                     */
                    $timeout(function () {
                        getData(id);
                    }, 3000);
                });
            }
        }
    }
})();
