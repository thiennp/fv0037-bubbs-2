(function () {
    'use strict';

    angular.module('app')
        .controller('GeoTableCtrl', ['$state', '$timeout', GeoTableCtrl])

    function GeoTableCtrl($state, $timeout) {
        var vm = this;
        this.$onInit = function() {
            vm.showValue3 = $state.params.id === 3;

            // Get JSON file to load phant url;
            $.getJSON('../dataStreams/live_data/data_stream_' + $state.params.id + '.json', function (data) {
                vm.streamJSON = data;
                getData();
            });
        };

        // Get data from data which is get from json file
        function getData() {
            // Get data from phant
            $.getJSON(vm.streamJSON.outputUrl + '#' + Math.floor(Math.random() * 1000000), function (data) {
                if (!_.isEqual(data, vm.data)) {
                    vm.data = data;
                }
                $timeout(getData, 1000);
            });
        }
    }
})();
