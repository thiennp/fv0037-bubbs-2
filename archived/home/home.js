App
    .config(function ($stateProvider) {
        $stateProvider
            .state('home', {
                url: '/home',
                template: '<div ui-view></div>'
            })
            .state('home.key', {
                url: '/:key',
                templateUrl: './home/home.html',
                controller: 'HomeController'
            })
    })

    .controller('HomeController', [
        '$http',
        '$interval',
        '$q',
        '$rootScope',
        '$scope',
        '$state',
        function ($http, $interval, $q, $rootScope, $scope, $state) {
            window.homeState = true;
            $http.get('./data/device_' + $state.params.key + '.json').then(function (res) {
                $rootScope.server = res.data;
                localStorage.setItem('server', $rootScope.server.inputUrl);
                localStorage.setItem('privateKey', $rootScope.server.privateKey);
            });

            function getData(callback) {
                var url = $rootScope.server.outputUrl + '#' + Math.floor(Math.random() * 1000000);
                if (!$scope.loading) {
                    $scope.loading = true;
                    $.ajax({
                        url: url,
                        data: { page: 1 },
                        dataType: 'jsonp'
                    }).done(function (results) {
                        if (results.length) {
                            var valid = false;
                            if (newAddedMarker.lat) {
                                var i = 0;
                                while (results[i] && !valid) {
                                    if (Number(results[i].lat) === Number(newAddedMarker.lat) && Number(results[i].lng) === Number(newAddedMarker.lng)) {
                                        valid = true;
                                    }
                                    i++;
                                }
                            } else {
                                valid = true;
                            }
                            if (valid) {
                                if (lastLat !== results[0].lat || lastLng !== results[0].lng || lastHum !== results[0].hum || lastTemp !== results[0].temp) {
                                    lastLat = results[0].lat;
                                    lastLng = results[0].lng;
                                    lastHum = results[0].hum;
                                    lastTemp = results[0].temp;
                                    geoHistory = results.reverse();
                                    if (geoHistory.length) {
                                        lat = Number(lastLat);
                                        lng = Number(lastLng);
                                        var humTempHtml = '<div class="description-title col-xs-6">Hum</div>';
                                        humTempHtml += '<div class="description-value col-xs-6">';
                                        humTempHtml += lastHum;
                                        humTempHtml += '</div>';
                                        humTempHtml += '<div class="description-title col-xs-6">Temp</div>';
                                        humTempHtml += '<div class="description-value col-xs-6">';
                                        humTempHtml += lastTemp;
                                        humTempHtml += '</div>';
                                        $('#sidebar-1 .hum-temp').html(humTempHtml);

                                        if (callback) {
                                            callback();
                                        }
                                    }
                                }
                            }
                        } else {
                            if (newAddedMarker && newAddedMarker.lat) {
                                lat = newAddedMarker.lat;
                                lng = newAddedMarker.lng;
                            }
                            geoHistory = [{
                                lat: lat,
                                lng: lng
                            }];
                            if (callback) {
                                callback();
                            }
                        }
                        $scope.loading = false;
                    }).error(function () {
                        $scope.loading = false;
                    });
                }
            }

            $q.all([$http.get('./data/lnglat.json'), $http.get('./data/location_' + $state.params.key + '.json'), $http.get('./data/leftside.json')]).then(function (response) {
                getData(function () {
                    zoom = response[0].data.zoom;
                    locationData = response[1].data;
                    leftSidebarData = response[2].data;

                    // For google map
                    initLMap($state);
                    createMarkers();
                });
            });

            $interval(function () {
                getData(updateLMapMarker);
            }, 1000);

            $scope.closeSidebar = function (index) {
                $('#sidebar-' + index).removeClass('active');
            };
        }]);