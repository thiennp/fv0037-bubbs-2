const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const md5 = require('md5');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ limit: '5mb' }));


app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/client'));
app.use(express.static(__dirname + '/upload'));

mongoose.connect('mongodb://localhost/fv0037');

var Schema = mongoose.Schema;

var MarkerModel = new Schema({
    'lat': Number,
    'lng': Number,
    'description': String,
    'markerId': String,
    'status': Number,
    'routeName': String,
    'route': Number,
    'image': String,
    'create': Date,
    'edit': Date,
    'history': [{
        'description': String,
        'status': Number,
        'routeName': String,
        'route': Number,
        'image': String,
        'create': Date,
        'edit': Date
    }],
    'userId': String
});

var UserModel = new Schema({
    'email': {
        unique: true,
        type: String
    },
    'username': {
        unique: true,
        type: String
    },
    'password': String,
    'create': Date,
    'edit': Date,
    'image': String,
    'name': String,
    'bio': String
});


var Marker = mongoose.model('MarkerModel', MarkerModel);
var User = mongoose.model('UserModel', UserModel);
function applyUser(marker, user) {
    return {
        _id: marker._id,
        lat: marker.lat,
        lng: marker.lng,
        description: marker.description,
        markerId: marker.markerId,
        status: marker.status,
        route: marker.route,
        routeName: marker.routeName,
        image: marker.image,
        create: marker.create,
        edit: marker.edit,
        history: marker.history,
        userId: marker.userId,
        user: user ? {
            name: user.name,
            username: user.username,
            image: user.image,
            email: user.email
        } : {}
    };
}
function applyAllUsers(res, markers, users) {
    var sendMarkers = [];
    var usersById = {};
    var i;
    for (i = 0; i < users.length; i++) {
        usersById[users[i]._id] = users[i];
    }
    for (i = 0; i < markers.length; i++) {
        sendMarkers[i] = applyUser(markers[i], usersById[markers[i].userId]);
    }
    res.send(sendMarkers);
}

var apiRoutes = express.Router();

// route to authenticate a user (POST http://localhost:8080/api/authenticate)

// route middleware to verify a token
apiRoutes.use(function (req, res, next) {

    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    // decode token
    if (token) {

        // verifies secret and checks exp
        jwt.verify(token, 'secret', function (err, decoded) {
            if (err) {
                return res.send(401, { success: false, message: 'Failed to authenticate token.' });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });

    } else {

        // if there is no token
        // return an error
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });

    }
});

app.use('/api', apiRoutes);


app.get('/', function (req, res) {
    res.sendFile('client/index.html', {
        root: __dirname
    });
});

app.get('/api/all', function (req, res) {
    const userId = req.headers['x-access-id'];
    Marker.find({})
        .sort('_id')
        .exec(function (err, markers) {
            if (!err) {
                User.find({}, function (err, users) {
                    if (!err) {
                        applyAllUsers(res, markers, users);
                    } else {
                        res.send(err);
                    }
                });
            } else {
                res.send(err);
            }
        });
});

app.get('/api/marker', function (req, res) {
    const userId = req.headers['x-access-id'];
    Marker.find({
        status: {
            $lte: 4
        }
    })
        .sort('_id')
        .exec(function (err, markers) {
            if (!err) {
                User.find({}, function (err, users) {
                    if (!err) {
                        applyAllUsers(res, markers, users);
                    } else {
                        res.send(err);
                    }
                });
            } else {
                res.send(err);
            }
        });
});

app.get('/api/route', function (req, res) {
    const userId = req.headers['x-access-id'];
    Marker.find({
        status: {
            $gt: 4
        }
    })
        .sort('_id')
        .exec(function (err, markers) {
            if (!err) {
                User.find({}, function (err, users) {
                    if (!err) {
                        applyAllUsers(res, markers, users);
                    } else {
                        res.send(err);
                    }
                });
            } else {
                res.send(err);
            }
        });
});

app.get('/api/marker/:id', function (req, res) {
    const userId = req.headers['x-access-id'];
    Marker.findOne({
        _id: req.params.id
    }, function (err, marker) {
        if (!err) {
            if (marker) {
                User.findOne({
                    _id: marker.userId
                }, function (err, user) {
                    if (!err) {
                        const returnMarker = applyUser(marker, user);
                        res.send(returnMarker);
                    } else {
                        res.send(err);
                    }
                });
            }
        } else {
            res.send(err);
        }
    });
});

app.post('/api/marker', function (req, res) {
    const userId = req.headers['x-access-id'];
    if (req.body.image) {
        var base64Data = req.body.image.replace(/^data:image\/png;base64,/, '');
        var filename = 'upload_' + Math.floor(Math.random() * 1000000000) + '.png';
        require('fs').writeFile('upload/' + filename, base64Data, 'base64', function (err) {
            console.log(err);
        });
        req.body.image = filename;
    }
    req.body.create = new Date();
    req.body.userId = userId;
    if (req.body.history) {
        req.body.history.forEach(function (item) {
            if (item.uploadImage) {
                var base64Data = item.uploadImage.replace(/^data:image\/png;base64,/, '');
                var filename = 'upload_' + Math.floor(Math.random() * 1000000000) + '.png';
                require('fs').writeFile('upload/' + filename, base64Data, 'base64', function (err) {
                    console.log(err);
                });
                item.image = filename;
            }
            if (!item._id) {
                item.create = new Date();
            } else {
                item.edit = new Date();
            }
        });
    }

    var marker = new Marker(req.body);
    marker.save(function (err, result) {
        if (!err) {
            res.send(result);
        } else {
            res.send(err);
        }
    });
});

app.put('/api/marker/:id', function (req, res) {
    const userId = req.headers['x-access-id'];
    if (req.body.uploadImage) {
        var base64Data = req.body.uploadImage.replace(/^data:image\/png;base64,/, '');
        var filename = 'upload_' + Math.floor(Math.random() * 1000000000) + '.png';
        require('fs').writeFile('upload/' + filename, base64Data, 'base64', function (err) {
            console.log(err);
        });
        req.body.image = filename;
    }
    req.body.update = new Date();
    req.body.userId = userId;
    req.body.history.forEach(function (item) {
        if (item.uploadImage) {
            var base64Data = item.uploadImage.replace(/^data:image\/png;base64,/, '');
            var filename = 'upload_' + Math.floor(Math.random() * 1000000000) + '.png';
            require('fs').writeFile('upload/' + filename, base64Data, 'base64', function (err) {
                console.log(err);
            });
            item.image = filename;
        }
        if (!item._id) {
            item.create = new Date();
        } else {
            item.edit = new Date();
        }
    });
    Marker
        .update({
            _id: req.params.id
        }, {
            $set: req.body
        }, function (err, result) {
            if (!err) {
                res.send(result);
            } else {
                res.send(err);
            }
        });
});

app.delete('/api/marker', function (req, res) {
    const userId = req.headers['x-access-id'];
    var Marker = mongoose.model('MarkerModel', MarkerModel);
    Marker.remove({ '_id': req.query.id },
        function (err) {
            if (!err) {
                res.send(req.body);
            } else {
                res.send(err);
            }
        });
});

app.post('/auth/register', function (req, res) {
    var User = mongoose.model('UserModel', UserModel);
    User.findOne({
        email: req.body.email
    }, function (err, user) {
        if (err) throw err;
        if (!user) {
            User.findOne({
                username: req.body.username
            }, function (err, user) {
                if (!user) {
                    req.body.create = new Date();
                    req.body.password = md5(req.body.password);
                    var user = new User(req.body);
                    user.save(function (err, result) {
                        if (!err) {
                            res.send(result);
                        } else {
                            res.send(err);
                        }
                    });
                } else {
                    res.send(401, { success: false, message: 'Username is already exist' });
                }
            });
        } else {
            res.send(401, { success: false, message: 'Email is already exist' });
        }
    });
});

app.post('/auth/login', function (req, res) {
    User.findOne({
        username: req.body.username,
        password: md5(req.body.password)
    }, function (err, user) {
        if (err) throw err;

        if (!user) {
            res.send(401, { success: false, message: 'Authentication failed. Please check your email or password.' });
        } else if (user) {
            const payload = {};

            var token = jwt.sign({}, 'secret');

            // return the information including token as JSON
            res.send({
                success: true,
                token: token,
                id: user._id,
                email: user.email,
                name: user.name,
                image: user.image,
                username: user.username
            });
        }
    });
});

app.get('/api/profile', function (req, res) {
    const userId = req.headers['x-access-id'];
    User.findOne(
        {
            _id: userId
        }, {
            'email': true,
            'image': true,
            'username': true,
            'name': true,
            'bio': true
        }, function (err, user) {
            if (!err) {
                res.send(user);
            } else {
                res.send(err);
            }
        });
});

app.put('/api/profile', function (req, res) {
    const userId = req.headers['x-access-id'];
    if (req.body.uploadImage) {
        var base64Data = req.body.uploadImage.replace(/^data:image\/png;base64,/, '');
        var filename = 'upload_' + Math.floor(Math.random() * 1000000000) + '.png';
        require('fs').writeFile('upload/' + filename, base64Data, 'base64', function (err) {
            console.log(err);
        });
        req.body.image = filename;
        delete req.body.uploadImage;
    }
    req.body.edit = new Date();

    User
        .update({
            _id: userId
        }, {
            $set: req.body
        }, function (err, result) {
            if (!err) {
                User.findOne(
                    {
                        _id: userId
                    }, {
                        'email': true,
                        'image': true,
                        'name': true,
                        'bio': true
                    }, function (err, user) {
                        if (!err) {
                            res.send(user);
                        } else {
                            res.send(err);
                        }
                    });
            } else {
                res.send(err);
            }
        });
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
});