/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App')
        .controller('ReportController', ReportController);

    // For global map page
    function ReportController($http, $state, $timeout) {
        var vm = this;

        vm.labels = ['', 'Good', 'Bad', 'Ugly', 'Planned', 'Route'];
        vm.selections = [];

        getData();

        function getData() {
            if ($state.params.id && $state.params.id.split(',').length < 2) {
                $http.get(window.apiUrl + 'marker/' + $state.params.id).then(function (result) {
                    vm.data = result.data;
                }, function () {
                    localStorage.removeItem('token');
                    $state.go('auth');
                });
            } else {
                $http.get(window.apiUrl + 'all').then(function (result) {
                    vm.list = result.data;
                    if ($state.params.id) {
                        vm.allIds = $state.params.id.split(',');
                        vm.shownList = [];
                        _.each(vm.list, function (item) {
                            if (vm.allIds.indexOf(item._id) > -1) {
                                vm.shownList.push(item);
                            }
                        });
                    } else {
                        vm.shownList = vm.list;
                    }
                }, function () {
                    localStorage.removeItem('token');
                    $state.go('auth');
                });
            }
        }

        vm.export = function () {
            vm.printing = true;
            $timeout(function () {
                window.print();
                vm.printing = false;
            }, 10);
        };

        vm.toggleSelection = function (index) {
            $timeout(function () {
                if (index === undefined) {
                    if (!vm.preventAll) {
                        if (!vm.allSelected) {
                            _.each(vm.list, function (item, index) {
                                vm.selections[index] = false;
                            });
                        } else {
                            _.each(vm.list, function (item, index) {
                                vm.selections[index] = true;
                            });
                        }
                    } else {
                        vm.preventAll = false;
                    }
                } else {
                    vm.preventAll = true;

                    if (!vm.selections[index] && vm.allSelected) {
                        vm.allSelected = false;
                    }

                    $timeout(function () {
                        vm.preventAll = false;
                    }, 100);
                }
            }, 10);
        };

        vm.selectAll = function (status) {
            var ids = [];

            _.each(vm.list, function (item) {
                if (item.status === status) {
                    ids.push(item._id);
                }
            });

            $state.go('report', {
                id: ids.join(',')
            });
        };

        vm.selections = function () {
            var ids = [];
            _.each(vm.list, function (item, index) {
                if (vm.selections[index]) {
                    ids.push(item._id);
                }
            });
            return ids.join(',');
        };
    }
})();