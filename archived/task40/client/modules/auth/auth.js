/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App')
        .controller('AuthController', AuthController);

    // For global map page
    function AuthController($http, $state, $rootScope) {
        var vm = this;
        vm.isRegisterPage = false;

        localStorage.removeItem('token');
        localStorage.removeItem('id');

        vm.login = function () {
            delete vm.err;
            if (!vm.password) {
                vm.err = {
                    data: {
                        message: 'Password is required'
                    }
                };
            } else if (!vm.username) {
                vm.err = {
                    data: {
                        message: 'Username is require'
                    }
                };
            } else {
                $http.post(window.authUrl + 'login', {
                    username: vm.username,
                    password: vm.password
                }).then(function (result) {
                    if (result && result.data && result.data.success) {
                        localStorage.setItem('token', result.data.token);
                        if (!!result.data.name) {
                            localStorage.setItem('user_name', result.data.name);
                        }
                        if (!!result.data.username) {
                            localStorage.setItem('user_username', result.data.username);
                        }
                        if (!!result.data.email) {
                            localStorage.setItem('user_email', result.data.email);
                        }
                        if (!!result.data.image) {
                            localStorage.setItem('user_image', result.data.image);
                        }
                        localStorage.setItem('id', result.data.id);
                        $rootScope.$broadcast('auth');
                        $state.go('main');
                    }
                }, function (err) {
                    vm.err = err;
                });
            }
        };

        vm.register = function () {
            delete vm.err;
            if (!vm.password) {
                vm.err = {
                    data: {
                        message: 'Password is required'
                    }
                };
            } else if (!vm.retypePassword) {
                vm.err = {
                    data: {
                        message: 'Please re-type password'
                    }
                };
            } else if (!vm.email) {
                vm.err = {
                    data: {
                        message: 'Email is require'
                    }
                };
            } else if (!vm.username) {
                vm.err = {
                    data: {
                        message: 'Username is require'
                    }
                };
            } else if (vm.password !== vm.retypePassword) {
                vm.err = {
                    data: {
                        message: 'Retype password is not matched'
                    }
                };
            } else {
                $http.post(window.authUrl + 'register', {
                    email: vm.email,
                    username: vm.username,
                    password: vm.password
                }).then(function (result) {
                    try {
                        if (result.data._id) {
                            vm.isRegisterPage = false;
                        }
                    } catch (e) {
                        console.log(e);
                    }
                }, function (err) {
                    vm.err = err;
                });
            }
        };
    }
})();