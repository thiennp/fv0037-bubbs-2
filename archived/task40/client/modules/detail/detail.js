/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App')
        .controller('DetailController', DetailController);

    // For global map page
    function DetailController($http, $state) {
        var vm = this;

        if (!$state.params || !$state.params.id) {
            $state.go('main');
        } else {
            $http.get(window.apiUrl + 'marker/' + $state.params.id).then(function (result) {
                vm.marker = result.data;
            }, function (err) {
                console.log(err);
                localStorage.removeItem('token');
                $state.go('auth');
            });
        }
    }
})();