/**
 * Application: App
 * --------------------------------------------------------------------------------------------------------------------
 * Global Modules
 * --------------------------------------------------------------------------------------------------------------------
 */

(function () {
    'use strict';

    angular
        .module('App', [
            'ngSanitize',
            'ngRoute',
            'ui.router',
            'ui.bootstrap',
            'chart.js',
            'angularMoment'
        ])
        .config(routerConfig)
        .run(run);

    /** @ngInject */
    function routerConfig($httpProvider, $stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('main', {
                url: '/main',
                templateUrl: 'modules/main/main.html',
                controller: 'MainController',
                controllerAs: 'mainCtrl'
            })
            .state('detail', {
                url: '/detail/:id',
                templateUrl: 'modules/detail/detail.html',
                controller: 'DetailController',
                controllerAs: 'detailCtrl'
            })
            .state('chart', {
                url: '/chart',
                templateUrl: 'modules/chart/chart.html',
                controller: 'ChartController',
                controllerAs: 'chartCtrl'
            })
            .state('markers', {
                url: '/markers',
                templateUrl: 'modules/markers/markers.html',
                controller: 'MarkersController',
                controllerAs: 'vm'
            })
            .state('reports', {
                url: '/report',
                templateUrl: 'modules/report/report.html',
                controller: 'ReportController',
                controllerAs: 'vm'
            })
            .state('report', {
                url: '/report/:id',
                templateUrl: 'modules/report/report.html',
                controller: 'ReportController',
                controllerAs: 'vm'
            })
            .state('route-markers', {
                url: '/route-markers',
                templateUrl: 'modules/route-markers/route-markers.html',
                controller: 'RouteMarkersController',
                controllerAs: 'vm'
            })
            .state('auth', {
                url: '/auth',
                templateUrl: 'modules/auth/auth.html',
                controller: 'AuthController',
                controllerAs: 'vm'
            })
            .state('profile', {
                url: '/profile',
                templateUrl: 'modules/profile/profile.html',
                controller: 'ProfileController',
                controllerAs: 'vm'
            });

        $urlRouterProvider.otherwise('/main');
        $httpProvider.interceptors.push(function ($q) {
            return {
                'request': function (config) {
                    config.headers['x-access-token'] = localStorage.getItem('token');
                    config.headers['x-access-id'] = localStorage.getItem('id');
                    return config;
                }
            };
        });
    }

    function run() {
    }
})();